package id.halloarif.app.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ListSettingModel implements Parcelable {

    private String title;
    private int icon;
    private String titleDesc;

    public ListSettingModel(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    /**/
    public ListSettingModel(Parcel in) {
        title = in.readString();
        icon = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(icon);
        dest.writeString(titleDesc);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ListSettingModel> CREATOR = new Creator<ListSettingModel>() {
        @Override
        public ListSettingModel createFromParcel(Parcel in) {
            return new ListSettingModel(in);
        }

        @Override
        public ListSettingModel[] newArray(int size) {
            return new ListSettingModel[size];
        }
    };
    /**/

    @Override
    public String toString() {
        return "ListSettingModel{" +
                "title = '" + title + '\'' +
                ", icon = " + icon +
                ", titleDesc = '" + titleDesc + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitleDesc() {
        return titleDesc;
    }

    public void setTitleDesc(String titleDesc) {
        this.titleDesc = titleDesc;
    }
}
