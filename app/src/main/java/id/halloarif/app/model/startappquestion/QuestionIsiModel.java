package id.halloarif.app.model.startappquestion;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestionIsiModel implements Parcelable {

    public String jawaban;

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    protected QuestionIsiModel(Parcel in) {
        jawaban = in.readString();
    }

    public static final Creator<QuestionIsiModel> CREATOR = new Creator<QuestionIsiModel>() {
        @Override
        public QuestionIsiModel createFromParcel(Parcel in) {
            return new QuestionIsiModel(in);
        }

        @Override
        public QuestionIsiModel[] newArray(int size) {
            return new QuestionIsiModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(jawaban);
    }
}