package id.halloarif.app.model.startappquestion;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class QuestionMainModel implements Parcelable {

    public String pertanyaan;
    public String keterangan;
    public String tipe;
    public List<QuestionIsiModel> isi = new ArrayList<>();
    public Integer lainnya;

    protected QuestionMainModel(Parcel in) {
        pertanyaan = in.readString();
        keterangan = in.readString();
        tipe = in.readString();
        if (in.readByte() == 0) {
            lainnya = null;
        } else {
            lainnya = in.readInt();
        }
    }

    public static final Creator<QuestionMainModel> CREATOR = new Creator<QuestionMainModel>() {
        @Override
        public QuestionMainModel createFromParcel(Parcel in) {
            return new QuestionMainModel(in);
        }

        @Override
        public QuestionMainModel[] newArray(int size) {
            return new QuestionMainModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(pertanyaan);
        parcel.writeString(keterangan);
        parcel.writeString(tipe);
        if (lainnya == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(lainnya);
        }
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public List<QuestionIsiModel> getIsi() {
        return isi;
    }

    public void setIsi(List<QuestionIsiModel> isi) {
        this.isi = isi;
    }

    public Integer getLainnya() {
        return lainnya;
    }

    public void setLainnya(Integer lainnya) {
        this.lainnya = lainnya;
    }
}