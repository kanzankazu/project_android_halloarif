package id.halloarif.app.API;

import id.halloarif.app.IConfig;

public class API {

    private static ApiServices getAPIService() {
        return RetrofitClient.getClient(IConfig.API_BASE_URL).create(ApiServices.class);
    }
}
