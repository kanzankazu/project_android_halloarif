package id.halloarif.app.view.activity.Role.Client;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.List;

import id.halloarif.app.R;
import id.halloarif.app.util.PictureUtil;

public class ClientHomeArticleQuotesAdapter extends RecyclerView.Adapter<ClientHomeArticleQuotesAdapter.ViewHolderQuotes> {
    private static final int ITEM_TYPE_QUOTES = 0;
    private static final int ITEM_TYPE_ARTICLE = 1;
    private final List<ArticleQuotesModel> models;
    private final Activity activity;

    public ClientHomeArticleQuotesAdapter(Activity activity, List<ArticleQuotesModel> models) {
        this.activity = activity;
        this.models = models;
    }

    @Override
    public int getItemViewType(int position) {
        String type = models.get(position).getType();
        if (type.equalsIgnoreCase("quotes")) {
            return ITEM_TYPE_QUOTES;
        } else if (type.equalsIgnoreCase("article")) {
            return ITEM_TYPE_ARTICLE;
        } else {
            return ITEM_TYPE_QUOTES;
        }
    }

    @NonNull
    @Override
    public ViewHolderQuotes onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        int viewType = getItemViewType(i);
        if (viewType == ITEM_TYPE_QUOTES) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_client_home_quotes, parent, false);
            return new ViewHolderQuotes(view);
        } else if (viewType == ITEM_TYPE_ARTICLE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_client_home_article, parent, false);
            return new ViewHolderArticle(view);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderQuotes viewHolder, int position) {
        ArticleQuotesModel model = models.get(position);

        switch (viewHolder.getItemViewType()) {
            case ITEM_TYPE_QUOTES:
                ViewHolderQuotes holder = (ViewHolderQuotes) viewHolder;
                //holder.ivListItemClientHomeQuotesfvbi.setImageResource(model.getImage());
                Glide.with(activity)
                        .load(model.getImage())
                        .asBitmap()
                        .placeholder(R.drawable.gallery_cooming_soon)
                        .error(R.drawable.gallery_cooming_soon)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                Bitmap bitmap = PictureUtil.resizeImageBitmap(resource, 720);
                                holder.ivListItemClientHomeQuotesfvbi.setImageBitmap(bitmap);
                            }
                        });
                break;
            case ITEM_TYPE_ARTICLE:
                ViewHolderArticle holder1 = (ViewHolderArticle) viewHolder;
                //holder1.ivListItemClientHomeArticlefvbi.setImageResource(model.getImage());
                Glide.with(activity)
                        .load(model.getImage())
                        .asBitmap()
                        .placeholder(R.drawable.gallery_cooming_soon)
                        .error(R.drawable.gallery_cooming_soon)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                Bitmap bitmap = PictureUtil.resizeImageBitmap(resource, 720);
                                holder1.ivListItemClientHomeArticlefvbi.setImageBitmap(bitmap);
                            }
                        });
                holder1.tvListItemClientHomeArticlefvbi.setText(model.getDesc());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolderQuotes extends RecyclerView.ViewHolder {
        private final ImageView ivListItemClientHomeQuotesfvbi;

        public ViewHolderQuotes(@NonNull View itemView) {
            super(itemView);
            ivListItemClientHomeQuotesfvbi = (ImageView) itemView.findViewById(R.id.ivListItemClientHomeQuotes);
        }
    }

    public class ViewHolderArticle extends ViewHolderQuotes {
        private final ImageView ivListItemClientHomeArticlefvbi;
        private final TextView tvListItemClientHomeArticlefvbi;

        ViewHolderArticle(View itemView) {
            super(itemView);
            ivListItemClientHomeArticlefvbi = (ImageView) itemView.findViewById(R.id.ivListItemClientHomeArticle);
            tvListItemClientHomeArticlefvbi = (TextView) itemView.findViewById(R.id.tvListItemClientHomeArticle);
        }
    }
}
