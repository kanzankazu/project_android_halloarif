package id.halloarif.app.view.activity.App;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.util.SystemUtil;

/**
 * add return value
 */
public class BoxUnegUnegDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvBoxDetailClientNamefvbi;
    private TextView tvBoxDetailClientNameAliasfvbi;
    private TextView tvBoxDetailKategorifvbi;
    private TextView tvBoxDetailLevelProblemfvbi;
    private EditText etBoxDetailPesanClientfvbi;
    private TextView tvBoxDetailPesanClientfvbi;
    private Spinner spBoxDetailMotivatorChoosefvbi;
    private EditText etBoxDetailPesanMotivatorfvbi;
    private TextView tvBoxDetailPesanMotivatorfvbi;
    private CardView cvBoxDetailSavefvbi;
    private CardView cvBoxDetailSendfvbi;
    private CardView cvBoxDetailBackfvbi;
    private CardView cvBoxDetailChatMotivatorfvbi;
    private CardView cvBoxDetailCallMotivatorfvbi;

    private BoxUnegUnegModel model;
    private int roleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_uneg_uneg_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        tvBoxDetailClientNamefvbi = (TextView) findViewById(R.id.tvBoxDetailClientName);
        tvBoxDetailClientNameAliasfvbi = (TextView) findViewById(R.id.tvBoxDetailClientNameAlias);
        tvBoxDetailKategorifvbi = (TextView) findViewById(R.id.tvBoxDetailKategori);
        tvBoxDetailLevelProblemfvbi = (TextView) findViewById(R.id.tvBoxDetailLevelProblem);
        etBoxDetailPesanClientfvbi = (EditText) findViewById(R.id.etBoxDetailPesanClient);
        tvBoxDetailPesanClientfvbi = (TextView) findViewById(R.id.tvBoxDetailPesanClient);
        spBoxDetailMotivatorChoosefvbi = (Spinner) findViewById(R.id.spBoxDetailMotivatorChoose);
        etBoxDetailPesanMotivatorfvbi = (EditText) findViewById(R.id.etBoxDetailPesanMotivator);
        tvBoxDetailPesanMotivatorfvbi = (TextView) findViewById(R.id.tvBoxDetailPesanMotivator);
        cvBoxDetailSavefvbi = (CardView) findViewById(R.id.cvBoxDetailSave);
        cvBoxDetailSendfvbi = (CardView) findViewById(R.id.cvBoxDetailSend);
        cvBoxDetailBackfvbi = (CardView) findViewById(R.id.cvBoxDetailBack);
        cvBoxDetailChatMotivatorfvbi = (CardView) findViewById(R.id.cvBoxDetailChatMotivator);
        cvBoxDetailCallMotivatorfvbi = (CardView) findViewById(R.id.cvBoxDetailCallMotivator);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.PARAM)) {
            model = (BoxUnegUnegModel) bundle.getParcelableExtra(ISeasonConfig.PARAM);
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
            roleId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
        }
    }

    private void initContent() {
        Toast.makeText(getApplicationContext(), model.toString(), Toast.LENGTH_SHORT).show();

        initSpinnerMotivatorSelectBox();

        updateUI(roleId);
    }

    private void initSpinnerMotivatorSelectBox() {
        //Spinner
        List<AppModel> models = new ArrayList<>();
        models.add(new AppModel(0, "Pilih Motivator", "Admin"));
        models.add(new AppModel(1, "GEMILANG001", "Admin"));
        models.add(new AppModel(2, "GEMILANG002", "Eva Kustina"));
        models.add(new AppModel(3, "GEMILANG003", "Topan"));
        models.add(new AppModel(4, "GEMILANG004", "Pinuji Rahayu"));

        CustomSpinAdapter adapter = new CustomSpinAdapter(BoxUnegUnegDetailActivity.this, android.R.layout.simple_spinner_item, models) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
//                return
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(R.id.tvlistitemSpinner);
                if (position == 0) {
                    // Set the hint text color gray
                    SystemUtil.changeColText(R.color.gray, tv);
                } else {
                    SystemUtil.changeColText(R.color.colorPrimaryDark, tv);
                }
                return view;
            }
        };
        spBoxDetailMotivatorChoosefvbi.setAdapter(adapter);
        spBoxDetailMotivatorChoosefvbi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    String sValue = adapter.getItem(position).getValue();
                    Toast.makeText(getApplicationContext(), sValue, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void updateUI(int roleId) {
        switch (roleId) {
            case ISeasonConfig.ROLE_MOTIVATOR:
                if (!TextUtils.isEmpty(model.getIsiMotivator())) {
                    etBoxDetailPesanMotivatorfvbi.setVisibility(View.GONE);
                    cvBoxDetailSendfvbi.setVisibility(View.GONE);
                } else {
                    tvBoxDetailPesanMotivatorfvbi.setVisibility(View.GONE);
                }
                SystemUtil.visibles(View.GONE, etBoxDetailPesanClientfvbi, spBoxDetailMotivatorChoosefvbi, cvBoxDetailSavefvbi, cvBoxDetailChatMotivatorfvbi, cvBoxDetailCallMotivatorfvbi);
                break;
            case ISeasonConfig.ROLE_CLIENT:
                if (!TextUtils.isEmpty(model.getIsiClient())) {
                    etBoxDetailPesanClientfvbi.setVisibility(View.GONE);
                    cvBoxDetailSendfvbi.setVisibility(View.GONE);
                } else {
                    tvBoxDetailPesanClientfvbi.setVisibility(View.GONE);
                }
                SystemUtil.visibles(View.GONE, etBoxDetailPesanMotivatorfvbi, spBoxDetailMotivatorChoosefvbi, cvBoxDetailSavefvbi);
                break;
            case ISeasonConfig.ROLE_HUMANCAPITAL:
                if (!TextUtils.isEmpty(model.getIsiClient()) && !TextUtils.isEmpty(model.getIsiMotivator())) {
                    spBoxDetailMotivatorChoosefvbi.setVisibility(View.GONE);
                } else if (!TextUtils.isEmpty(model.getIsiClient()) && TextUtils.isEmpty(model.getIsiMotivator())) {
                    spBoxDetailMotivatorChoosefvbi.setVisibility(View.VISIBLE);
                }

                SystemUtil.visibles(View.GONE, etBoxDetailPesanClientfvbi, etBoxDetailPesanMotivatorfvbi, cvBoxDetailSendfvbi, cvBoxDetailChatMotivatorfvbi, cvBoxDetailCallMotivatorfvbi);
                break;
        }

    }

    private void initListener() {
        cvBoxDetailSavefvbi.setOnClickListener(this);
        cvBoxDetailSendfvbi.setOnClickListener(this);
        cvBoxDetailBackfvbi.setOnClickListener(this);
        cvBoxDetailChatMotivatorfvbi.setOnClickListener(this);
        cvBoxDetailCallMotivatorfvbi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == cvBoxDetailSavefvbi) {
            Toast.makeText(getApplicationContext(), "simpan data", Toast.LENGTH_SHORT).show();
            onFinish();
        } else if (view == cvBoxDetailSendfvbi) {
            Toast.makeText(getApplicationContext(), "kirim data", Toast.LENGTH_SHORT).show();
        } else if (view == cvBoxDetailBackfvbi) {
            onBackPressed();
        } else if (view == cvBoxDetailChatMotivatorfvbi) {
            Intent intent = new Intent(BoxUnegUnegDetailActivity.this, AppActivity.class);
            intent.putExtra(ISeasonConfig.PARAM, "chat");
            startActivity(intent);
            finishAffinity();
        } else if (view == cvBoxDetailCallMotivatorfvbi) {
            Intent intent = new Intent(BoxUnegUnegDetailActivity.this, AppActivity.class);
            intent.putExtra(ISeasonConfig.PARAM, "call");
            startActivity(intent);
            finishAffinity();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onFinish() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }
}
