package id.halloarif.app.view.activity.Role.Client;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.model.startappquestion.QuestionMainModel;

public class SlideQuestionAdapter extends FragmentStatePagerAdapter {
    private List<QuestionMainModel> models = new ArrayList<>();

    SlideQuestionAdapter(FragmentManager fm, List<QuestionMainModel> models) {
        super(fm);
        this.models = models;
    }

    @Override
    public Fragment getItem(int position) {
        QuestionMainModel model = models.get(position);

        Bundle bundle = new Bundle();
        bundle.putInt(ISeasonConfig.Q_NO, position + 1);
        bundle.putString(ISeasonConfig.Q_ANSWER, model.getPertanyaan());
        if (!TextUtils.isEmpty(model.getKeterangan())) {
            bundle.putString(ISeasonConfig.Q_DESC, model.getKeterangan());
        }
        if (model.getIsi().size() != 0) {
            bundle.putParcelableArrayList(ISeasonConfig.Q_ISI, new ArrayList<>(model.getIsi()));
        }
        bundle.putInt(ISeasonConfig.Q_OTHER, model.getLainnya());
        bundle.putString(ISeasonConfig.Q_TYPE, model.getTipe());

        if (position == (models.size() - 1)) {
            bundle.putBoolean(ISeasonConfig.Q_LAST, true);
        } else {
            bundle.putBoolean(ISeasonConfig.Q_LAST, false);
        }

        ClientQuestionFragment fragment = new ClientQuestionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return models.size();
    }
}
