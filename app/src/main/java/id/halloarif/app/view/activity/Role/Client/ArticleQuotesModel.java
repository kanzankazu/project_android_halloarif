package id.halloarif.app.view.activity.Role.Client;

public class ArticleQuotesModel {
    private int id;
    private int image;
    private String imageUrl;
    private String desc;
    private Boolean status;
    private String type;

    public ArticleQuotesModel() {

    }

    public ArticleQuotesModel(int id, int image, String imageUrl, String desc, Boolean status, String type) {
        this.id = id;
        this.image = image;
        this.imageUrl = imageUrl;
        this.desc = desc;
        this.status = status;
        this.type = type;
    }

    public ArticleQuotesModel(int id, int image, String desc, String type) {
        this.id = id;
        this.image = image;
        this.desc = desc;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ArticleQuotesModel{" +
                "id=" + id +
                ", image=" + image +
                ", imageUrl='" + imageUrl + '\'' +
                ", desc='" + desc + '\'' +
                ", status=" + status +
                ", type='" + type + '\'' +
                '}';
    }
}
