package id.halloarif.app.view.activity.Logreg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.InputValidUtil;

public class RegisActivationActivity extends AppCompatActivity {

    private EditText etRecActiveEmailPhonefvbi, etRecActiveCodefvbi;
    private CardView cvRecActiveExecutefvbi;
    private TextView tvRecActiveExecutefvbi;

    private int userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_activation);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        etRecActiveEmailPhonefvbi = (EditText) findViewById(R.id.etRecActiveEmailPhone);
        etRecActiveCodefvbi = (EditText) findViewById(R.id.etRecActiveCode);
        cvRecActiveExecutefvbi = (CardView) findViewById(R.id.cvRecActiveExecute);
        tvRecActiveExecutefvbi = (TextView) findViewById(R.id.tvRecActiveExecute);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.KEY_USER_ID)) {
            userId = bundle.getIntExtra(ISeasonConfig.KEY_USER_ID, 0);
        }
    }

    private void initSession() {

    }

    private void initContent() {
        Log.d("Lihat", "initContent RegisActivationActivity : " + cvRecActiveExecutefvbi.getChildCount());
        Log.d("Lihat", "initContent RegisActivationActivity : " + cvRecActiveExecutefvbi.getChildAt(0));

        updateUi();
    }

    private void initListener() {
        etRecActiveEmailPhonefvbi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_GO) {
                    regisActivMainValid();
                }
                return handled;
            }
        });
        etRecActiveCodefvbi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_GO) {
                    regisActivMainValid();
                }
                return handled;
            }
        });

        cvRecActiveExecutefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regisActivMainValid();
            }
        });
    }

    private void updateUi() {
        if (userId == 0) {
            etRecActiveEmailPhonefvbi.setVisibility(View.VISIBLE);
            etRecActiveCodefvbi.setVisibility(View.GONE);
            tvRecActiveExecutefvbi.setText("Check Email");
        } else {
            etRecActiveEmailPhonefvbi.setVisibility(View.GONE);
            etRecActiveCodefvbi.setVisibility(View.VISIBLE);
            tvRecActiveExecutefvbi.setText("Aktifasi");
        }
    }

    private void regisActivMainValid() {
        if (userId == 0) {
            regisActivEmailPhoneValid();
        } else {
            regisActivCodeValid();
        }
    }

    private void regisActivEmailPhoneValid() {
        String sName = etRecActiveEmailPhonefvbi.getText().toString().trim();

        if (!InputValidUtil.isEmptyField("Kolom ini masih kosong", etRecActiveEmailPhonefvbi)) {
            if (InputValidUtil.isValidateEmail(sName)) {
                Toast.makeText(getApplicationContext(), sName + " = ini email", Toast.LENGTH_SHORT).show();
                userId = 1;
                updateUi();
            } else if (InputValidUtil.isValidatePhoneNumber(sName)) {
                Toast.makeText(getApplicationContext(), sName + " = ini no hp", Toast.LENGTH_SHORT).show();
                userId = 1;
                updateUi();
            } else {
                InputValidUtil.errorET(etRecActiveEmailPhonefvbi, sName + " = format Email/No Phone tidak terdeteksi");
            }
        }
    }

    private void regisActivCodeValid() {
        String sRegCode = etRecActiveCodefvbi.getText().toString().trim();
        if (!InputValidUtil.isEmptyField(etRecActiveCodefvbi)) {
            if (InputValidUtil.isLenghtChar(sRegCode, 6)) {
                Toast.makeText(getApplicationContext(), "Code Ok", Toast.LENGTH_SHORT).show();
                //userId = 1;//contoh
                //updateUi();
                finish();
            } else {
                InputValidUtil.errorET(etRecActiveCodefvbi, "Code kurang");
            }
        } else {
            InputValidUtil.errorET(etRecActiveCodefvbi, "Field masih kosong");
        }

    }

    @Override
    public void onBackPressed() {
        if (userId != 0) {
            userId = 0;
            updateUi();
        } else {
            finish();
        }
    }
}
