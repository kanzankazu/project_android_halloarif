package id.halloarif.app.view.activity.Role.Client;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import id.halloarif.app.R;

class SliderPagerAdapter extends PagerAdapter {
    private final Activity activity;
    private final List<SliderModel> models;

    public SliderPagerAdapter(Activity activity, List<SliderModel> models) {
        this.activity = activity;
        this.models = models;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        SliderModel model = models.get(position);

        View itemView = LayoutInflater.from(activity).inflate(R.layout.list_item_pager_slider, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.image_pager_item);

        if (TextUtils.isEmpty(model.getImage())) {
            imageView.setImageResource(R.drawable.slider3);
        } else {
            Glide.with(activity)
                    .load(model.getUrl())
                    .placeholder(R.drawable.promo_train)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .error(R.drawable.promo_train)
                    .into(imageView);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(model.getUrl())) {
                    Toast.makeText(activity, "Click = " + model.getUrl(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
