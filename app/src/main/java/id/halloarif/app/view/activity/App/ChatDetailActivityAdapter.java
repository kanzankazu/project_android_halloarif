package id.halloarif.app.view.activity.App;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.halloarif.app.R;

public class ChatDetailActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE_SEND = 0;
    private static final int ITEM_TYPE_RECEIVE = 1;
    private List<ChatDetailModel> models;
    private int userId;
    private ChatDetailActivityAdapterListener listener;
    private Activity activity;

    public ChatDetailActivityAdapter(Activity activity, List<ChatDetailModel> models, int userId, ChatDetailActivityAdapterListener listener) {

        this.activity = activity;
        this.models = models;
        this.userId = userId;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if ((models.get(position).getUser_id() == userId)) {
            return ITEM_TYPE_SEND;
        } else {
            return ITEM_TYPE_RECEIVE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (position) {
            case ITEM_TYPE_SEND:
                return new ChatDetailActivityAdapter.ViewHolderSend(inflater.inflate(R.layout.list_item_chat_send, parent, false));
            case ITEM_TYPE_RECEIVE:
                return new ChatDetailActivityAdapter.ViewHolderReceive(inflater.inflate(R.layout.list_item_chat_received, parent, false));
            default:
                return new ChatDetailActivityAdapter.ViewHolderSend(inflater.inflate(R.layout.list_item_chat_send, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ChatDetailModel model = models.get(position);
        Log.d("Lihat", "onBindViewHolder ChatDetailActivityAdapter : " + model.toString());

        if (viewHolder instanceof ViewHolderSend) {
            ViewHolderSend holder = (ViewHolderSend) viewHolder;
            holder.tvlistChatDetailSendMessagefvbi.setText(model.getMessage());
            holder.tvlistChatDetailSendJamfvbi.setText(model.getCreated_at());
            holder.cvlistChatDetailSendMainfvbi.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.click(model, position);
                    return false;
                }
            });
        } else if (viewHolder instanceof ViewHolderReceive) {
            ViewHolderReceive holder = (ViewHolderReceive) viewHolder;
            holder.tvlistChatDetailReceiveNamefvbi.setText("GEMILANG01");
            holder.tvlistChatDetailReceiveJamfvbi.setText(model.getCreated_at());
            holder.tvlistChatDetailReceiveMessagefvbi.setText(model.getMessage());
            holder.cvlistChatDetailReceiveMainfvbi.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.click(model, position);
                    return false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    private class ViewHolderSend extends RecyclerView.ViewHolder {

        private final TextView tvlistChatDetailSendMessagefvbi;
        private final TextView tvlistChatDetailSendJamfvbi;
        private final ImageView ivlistChatDetailSendCheklisfvbi;
        private final CardView cvlistChatDetailSendMainfvbi;

        ViewHolderSend(@NonNull View itemView) {
            super(itemView);
            cvlistChatDetailSendMainfvbi = (CardView) itemView.findViewById(R.id.cvlistChatDetailSendMain);
            tvlistChatDetailSendMessagefvbi = (TextView) itemView.findViewById(R.id.tvlistChatDetailSendMessage);
            tvlistChatDetailSendJamfvbi = (TextView) itemView.findViewById(R.id.tvlistChatDetailSendJam);
            ivlistChatDetailSendCheklisfvbi = (ImageView) itemView.findViewById(R.id.ivlistChatDetailSendCheklis);
        }
    }

    private class ViewHolderReceive extends RecyclerView.ViewHolder {

        private final TextView tvlistChatDetailReceiveNamefvbi;
        private final TextView tvlistChatDetailReceiveJamfvbi;
        private final TextView tvlistChatDetailReceiveMessagefvbi;
        private final CardView cvlistChatDetailReceiveMainfvbi;

        ViewHolderReceive(View itemView) {
            super(itemView);
            cvlistChatDetailReceiveMainfvbi = (CardView) itemView.findViewById(R.id.cvlistChatDetailReceiveMain);
            tvlistChatDetailReceiveNamefvbi = (TextView) itemView.findViewById(R.id.tvlistChatDetailReceiveName);
            tvlistChatDetailReceiveJamfvbi = (TextView) itemView.findViewById(R.id.tvlistChatDetailReceiveJam);
            tvlistChatDetailReceiveMessagefvbi = (TextView) itemView.findViewById(R.id.tvlistChatDetailReceiveMessage);
        }
    }

    public interface ChatDetailActivityAdapterListener {
        void click(ChatDetailModel model, int position);
    }

    public void setData(List<ChatDetailModel> datas) {
        if (models.size() > 0) {
            models.clear();
            models = datas;
        } else {
            models = datas;
        }
        notifyDataSetChanged();
    }

    public void replaceData(List<ChatDetailModel> datas) {
        models.clear();
        models.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<ChatDetailModel> datas) {
        models.addAll(datas);
        notifyItemRangeInserted(models.size(), datas.size());
    }

    public void addData(ChatDetailModel datas) {
        models.add(datas);
        notifyDataSetChanged();
    }

    public void addDataFirst(ChatDetailModel data) {
        int position = 0;
        models.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    public void removeDataFirst() {
        int position = 0;
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    public void restoreData(ChatDetailModel data, int position) {
        models.add(position, data);
        notifyItemInserted(position);
    }
}
