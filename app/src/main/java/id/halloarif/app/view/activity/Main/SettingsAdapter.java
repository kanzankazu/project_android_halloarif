package id.halloarif.app.view.activity.Main;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.halloarif.app.R;
import id.halloarif.app.model.ListSettingModel;

class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ViewHolder> {
    private final FragmentActivity activity;
    private List<ListSettingModel> models;

    public SettingsAdapter(FragmentActivity activity, List<ListSettingModel> models) {
        this.activity = activity;
        this.models = models;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_setting, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ListSettingModel model = models.get(i);

        viewHolder.ivListItemSettingsIconfvbi.setImageResource(model.getIcon());
        viewHolder.tvListItemSettingsTitlefvbi.setText(model.getTitle());
        viewHolder.tvListItemSettingsDescfvbi.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void setData(List<ListSettingModel> datas) {
        if (datas.size() > 0) {
            models.clear();
            models = datas;
        } else {
            models = datas;
        }
        notifyDataSetChanged();
    }

    public void replaceData(List<ListSettingModel> datas) {
        models.clear();
        models.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<ListSettingModel> datas) {
        models.addAll(datas);
        notifyItemRangeInserted(models.size(), datas.size());
    }

    public void addDataFirst(ListSettingModel data) {
        int position = 0;
        models.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    public void removeDataFirst() {
        int position = 0;
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    public interface ProfileListener {
        void click();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivListItemSettingsIconfvbi;
        private final TextView tvListItemSettingsTitlefvbi, tvListItemSettingsDescfvbi;

        public ViewHolder(View itemView) {
            super(itemView);
            ivListItemSettingsIconfvbi = (ImageView) itemView.findViewById(R.id.ivListItemSettingsIcon);
            tvListItemSettingsTitlefvbi = (TextView) itemView.findViewById(R.id.tvListItemSettingsTitle);
            tvListItemSettingsDescfvbi = (TextView) itemView.findViewById(R.id.tvListItemSettingsDesc);

        }
    }
}
