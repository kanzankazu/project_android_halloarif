package id.halloarif.app.view.activity.Role.Motivator;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.halloarif.app.R;

public class MotivatorScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<MotivatorScheduleModel> models;
    private final int roleId;
    private MotivatorScheduleAdapterListener listener;
    private Activity activity;

    public MotivatorScheduleAdapter(Activity activity, ArrayList<MotivatorScheduleModel> models, int roleId, MotivatorScheduleAdapterListener listener) {

        this.activity = activity;
        this.models = models;
        this.roleId = roleId;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_motivator_schedule, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        MotivatorScheduleModel model = models.get(position);

        if (viewHolder instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;
            holder.civMotivatorSchedulePhotofvbi.setImageResource(R.drawable.gallery_photo);
            holder.tvMotivatorScheduleDayfvbi.setText(model.getHari_jadwal());
            holder.ivMotivatorScheduleIndicatorfvbi.setColorFilter(activity.getResources().getColor(R.color.androidGreen));
            holder.tvMotivatorScheduleTextfvbi.setText("Offline");
            holder.tvMotivatorScheduleStartfvbi.setText(model.getJadwal_mulai());
            holder.tvMotivatorScheduleEndfvbi.setText(model.getJadwal_selesai());
            holder.tvMotivatorScheduleInfofvbi.setText("OK");
            holder.cvMotivatorScheduleEditfvbi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onEdit(model);
                }
            });
            holder.cvMotivatorScheduleDeletefvbi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onDelete(model);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CircleImageView civMotivatorSchedulePhotofvbi;
        private final TextView tvMotivatorScheduleDayfvbi, tvMotivatorScheduleTextfvbi, tvMotivatorScheduleStartfvbi, tvMotivatorScheduleEndfvbi, tvMotivatorScheduleInfofvbi;
        private final ImageView ivMotivatorScheduleIndicatorfvbi;
        private final CardView cvMotivatorScheduleEditfvbi, cvMotivatorScheduleDeletefvbi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            civMotivatorSchedulePhotofvbi = (CircleImageView) itemView.findViewById(R.id.civMotivatorSchedulePhoto);
            tvMotivatorScheduleDayfvbi = (TextView) itemView.findViewById(R.id.tvMotivatorScheduleDay);
            ivMotivatorScheduleIndicatorfvbi = (ImageView) itemView.findViewById(R.id.ivMotivatorScheduleIndicator);
            tvMotivatorScheduleTextfvbi = (TextView) itemView.findViewById(R.id.tvMotivatorScheduleText);
            tvMotivatorScheduleStartfvbi = (TextView) itemView.findViewById(R.id.tvMotivatorScheduleStart);
            tvMotivatorScheduleEndfvbi = (TextView) itemView.findViewById(R.id.tvMotivatorScheduleEnd);
            tvMotivatorScheduleInfofvbi = (TextView) itemView.findViewById(R.id.tvMotivatorScheduleInfo);
            cvMotivatorScheduleEditfvbi = (CardView) itemView.findViewById(R.id.cvMotivatorScheduleEdit);
            cvMotivatorScheduleDeletefvbi = (CardView) itemView.findViewById(R.id.cvMotivatorScheduleDelete);
        }
    }

    public interface MotivatorScheduleAdapterListener {
        void onEdit(MotivatorScheduleModel model);
        void onDelete(MotivatorScheduleModel model);
    }

    public void setData(List<MotivatorScheduleModel> datas) {
        if (models.size() > 0) {
            models.clear();
            models = datas;
        } else {
            models = datas;
        }
        notifyDataSetChanged();
    }

    public void replaceData(List<MotivatorScheduleModel> datas) {
        models.clear();
        models.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<MotivatorScheduleModel> datas) {
        models.addAll(datas);
        notifyItemRangeInserted(models.size(), datas.size());
    }

    public void addData(MotivatorScheduleModel data) {
        models.add(data);
        notifyDataSetChanged();
    }

    public void addDataFirst(MotivatorScheduleModel data) {
        int position = 0;
        models.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    public void removeDataFirst() {
        int position = 0;
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }
}
