package id.halloarif.app.view.activity.App;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.halloarif.app.R;

class CustomSpinAdapter extends ArrayAdapter<AppModel> {
    private final Activity activity;
    private final int view;
    private final List<AppModel> models;

    public CustomSpinAdapter(Activity activity, int view, List<AppModel> models) {
        super(activity, view, models);
        this.activity = activity;
        this.view = view;
        this.models = models;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Nullable
    @Override
    public AppModel getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_item_spinner_user, parent, false);
            mViewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvlistitemSpinner);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvlistitemSpinner);
        }

        mViewHolder.tvTitle.setText(getItem(position).getTitle().toUpperCase());

        /*LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_item_spinner_user, parent, false);
        TextView tvlistitemUserMotivatorfvbi = (TextView) view.findViewById(R.id.tvlistitemUserMotivator);
        tvlistitemUserMotivatorfvbi.setText(getItem(position).getTitle());*/

        /*View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_spinner_user, parent, false);
        TextView tvlistitemUserMotivatorfvbi = (TextView) view.findViewById(R.id.tvlistitemUserMotivator);
        tvlistitemUserMotivatorfvbi.setText(getItem(position).getTitle());*/

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_item_spinner_user_dropdown, parent, false);
        TextView tvlistitemUserMotivatorfvbi = (TextView) view.findViewById(R.id.tvlistitemSpinner);
        tvlistitemUserMotivatorfvbi.setText(getItem(position).getTitle());
        return view;

        /*AppModel model = models.get(position);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_spinner_user, parent, false);
        TextView tvlistitemUserMotivatorfvbi = (TextView) view.findViewById(R.id.tvlistitemUserMotivator);
        tvlistitemUserMotivatorfvbi.setText(model.getTitle());
        return view;*/

        //return getView(position, convertView, parent);
    }

    private class ViewHolder {
        TextView tvTitle;
    }
}
