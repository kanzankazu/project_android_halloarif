package id.halloarif.app.view.activity.Main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import id.halloarif.app.R;
import id.halloarif.app.util.support.Recycleritemclicklistener;

public class NotifFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView rvNotifFragmentMainfvbi;

    private NotifAdapter adapter;
    private ArrayList<NotifModel> models;

    public NotifFragment() {
        // Required empty public constructor
    }

    public static NotifFragment newInstance(String param1, String param2) {
        NotifFragment fragment = new NotifFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        NotifFragment fragment = new NotifFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notif, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        rvNotifFragmentMainfvbi = (RecyclerView) view.findViewById(R.id.rvNotifFragmentMain);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        models = new ArrayList<>();
        models.add(new NotifModel("", "Pembayaran atas Invoice ", "18/03/2019 11:05", true));
        models.add(new NotifModel("", "Pembayaran atas Invoice Pembayaran atas Invoice ", "18/03/2019 11:05", false));
        models.add(new NotifModel("", "Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice ", "18/03/2019 11:05", false));
        models.add(new NotifModel("", "Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice ", "18/03/2019 11:05", true));
        models.add(new NotifModel("", "Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice ", "18/03/2019 11:05", false));
        models.add(new NotifModel("", "Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice ", "18/03/2019 11:05", true));
        models.add(new NotifModel("", "Pembayaran atas Invoice Pembayaran atas Invoice Pembayaran atas Invoice ", "18/03/2019 11:05", false));
        models.add(new NotifModel("", "Pembayaran atas Invoice Pembayaran atas Invoice ", "18/03/2019 11:05", true));
        models.add(new NotifModel("", "Pembayaran atas Invoice", "18/03/2019 11:05", true));

        adapter = new NotifAdapter(getActivity(), models);
        rvNotifFragmentMainfvbi.setAdapter(adapter);
        rvNotifFragmentMainfvbi.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initListener() {
        rvNotifFragmentMainfvbi.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(),
                new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        NotifModel model = models.get(position);
                        Toast.makeText(getActivity(), model.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

}
