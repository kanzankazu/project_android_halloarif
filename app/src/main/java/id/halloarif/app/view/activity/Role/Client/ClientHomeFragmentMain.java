package id.halloarif.app.view.activity.Role.Client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.util.support.Recycleritemclicklistener;
import id.halloarif.app.view.activity.App.AppActivity;
import id.halloarif.app.view.activity.App.AppHistoryActivity;
import id.halloarif.app.view.activity.App.BoxUnegUnegActivity;
import id.halloarif.app.view.activity.App.BoxUnegUnegHistoryActivity;
import id.halloarif.app.view.activity.App.BoxUnegUnegInputActivity;

public class ClientHomeFragmentMain extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final int REQ_CODE_BOX = 123;
    public static final int REQ_CODE_CHAT = 213;
    public static final int REQ_CODE_CALL = 312;

    private String mParam1;
    private String mParam2;

    private CardView cvClientHomeInfofvbi;
    private LinearLayout llClientHomeInfoBoxUnegfvbi, llClientHomeInfoChatfvbi, llClientHomeInfoCallfvbi, llClientHomePromoSliderDotfvbi, llClientHomeAppfvbi, llClientHomeArticlefvbi, llClientHomeQuotesfvbi;
    private RelativeLayout rlClientHomePromofvbi;
    private ViewPager vpClientHomePromofvbi;
    private RecyclerView rvClientHomeAppfvbi, rvClientHomeArticlefvbi, rvClientHomeQuotesfvbi;

    private boolean isEmptyProfile = true;

    private SliderPagerAdapter vpaPromoHot;
    private int iPagerCountPromoHot;
    private ImageView[] ivIndicatorPromo;
    private int slider_count = 0;

    private ClientHomeAppAdapter adapterClientHomeApp;
    private ClientHomeArticleQuotesAdapter adapterClientHomeArticleQuotes;

    public ClientHomeFragmentMain() {
        // Required empty public constructor
    }

    public static ClientHomeFragmentMain newInstance(String param1, String param2) {
        ClientHomeFragmentMain fragment = new ClientHomeFragmentMain();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        ClientHomeFragmentMain fragment = new ClientHomeFragmentMain();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_home, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        cvClientHomeInfofvbi = (CardView) view.findViewById(R.id.cvClientHomeInfo);

        llClientHomeInfoBoxUnegfvbi = (LinearLayout) view.findViewById(R.id.llClientHomeInfoBoxUneg);
        llClientHomeInfoChatfvbi = (LinearLayout) view.findViewById(R.id.llClientHomeInfoChat);
        llClientHomeInfoCallfvbi = (LinearLayout) view.findViewById(R.id.llClientHomeInfoCall);

        vpClientHomePromofvbi = (ViewPager) view.findViewById(R.id.vpClientHomePromo);
        rlClientHomePromofvbi = (RelativeLayout) view.findViewById(R.id.rlClientHomePromo);
        llClientHomePromoSliderDotfvbi = (LinearLayout) view.findViewById(R.id.llClientHomePromoSliderDot);
        llClientHomeAppfvbi = (LinearLayout) view.findViewById(R.id.llClientHomeApp);
        rvClientHomeAppfvbi = (RecyclerView) view.findViewById(R.id.rvClientHomeApp);
        llClientHomeArticlefvbi = (LinearLayout) view.findViewById(R.id.llClientHomeArticle);
        rvClientHomeArticlefvbi = (RecyclerView) view.findViewById(R.id.rvClientHomeArticle);
        llClientHomeQuotesfvbi = (LinearLayout) view.findViewById(R.id.llClientHomeQuotes);
        rvClientHomeQuotesfvbi = (RecyclerView) view.findViewById(R.id.rvClientHomeQuotes);

    }

    private void initParam() {

    }

    private void initSession() {
    }

    private void initContent() {
        initSlider();
        initApps();
        initQuotes();
        initArticles();

        checkProfile();
    }

    private void initListener() {

        llClientHomeInfoBoxUnegfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), BoxUnegUnegHistoryActivity.class);
                intent.putExtra(ISeasonConfig.PARAM,"box");
                startActivity(intent);
            }
        });
        llClientHomeInfoChatfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AppHistoryActivity.class);
                intent.putExtra(ISeasonConfig.PARAM,"chat");
                startActivity(intent);
            }
        });
        llClientHomeInfoCallfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AppHistoryActivity.class);
                intent.putExtra(ISeasonConfig.PARAM,"call");
                startActivity(intent);
            }
        });

    }

    private void initSlider() {
        new SliderModel();
        List<SliderModel> models = new ArrayList<>();
        List<SliderModel> modelsnew = new ArrayList<>();
        models.add(new SliderModel(1, "", "", true, 1));
        models.add(new SliderModel(2, "1", "halloarif.com", true, 2));
        models.add(new SliderModel(3, "1", "halloarif.co.id/news", true, 3));
        models.add(new SliderModel(4, "", "halloarif.id", true, 4));
        models.add(new SliderModel(5, "1", "halloarif.id", false, 5));
        models.add(new SliderModel(6, "1", "halloarif.id", true, 6));
        models.add(new SliderModel(7, "", "halloarif.id", true, 7));
        models.add(new SliderModel(8, "", "", false, 8));

        for (int i = 0; i < models.size(); i++) {
            SliderModel model = models.get(i);
            if (model.getStatus()) {
                modelsnew.add(model);
            }
        }

        vpaPromoHot = new SliderPagerAdapter(getActivity(), modelsnew);
        vpClientHomePromofvbi.setAdapter(vpaPromoHot);
        vpClientHomePromofvbi.setCurrentItem(0);
        vpClientHomePromofvbi.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < iPagerCountPromoHot; i++) {
                    ivIndicatorPromo[i].setImageResource(R.drawable.nonselected_item);
                }
                ivIndicatorPromo[position].setImageResource(R.drawable.selected_item);
                slider_count = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        iPagerCountPromoHot = vpaPromoHot.getCount();
        ivIndicatorPromo = new ImageView[iPagerCountPromoHot];
        for (int i = 0; i < iPagerCountPromoHot; i++) {
            ivIndicatorPromo[i] = new ImageView(getActivity());
            ivIndicatorPromo[i].setImageResource(R.drawable.nonselected_item);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(10, 0, 10, 0);
            llClientHomePromoSliderDotfvbi.addView(ivIndicatorPromo[i], params);
            ivIndicatorPromo[0].setImageResource(R.drawable.selected_item);
        }
        //iPagerCountPromoHot = vpaPromoHot.getCount();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                vpClientHomePromofvbi.setCurrentItem(slider_count);
                if (slider_count == iPagerCountPromoHot) {
                    slider_count = 0;
                } else {
                    slider_count++;
                }
                handler.postDelayed(this, 3000);
            }
        }, 3000);
    }

    private void initApps() {
        new AppModel();
        List<AppModel> models = new ArrayList<>();
        models.add(new AppModel(1, "Box uneg uneg", R.drawable.ic_box_uneg));
        models.add(new AppModel(2, "Chat motivator", R.drawable.ic_chat));
        models.add(new AppModel(3, "Call Motivator", R.drawable.ic_call));

        adapterClientHomeApp = new ClientHomeAppAdapter(getActivity(), models);
        rvClientHomeAppfvbi.setAdapter(adapterClientHomeApp);
        rvClientHomeAppfvbi.setLayoutManager(new GridLayoutManager(getActivity(), 5));

        rvClientHomeAppfvbi.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(),
                new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        AppModel model = models.get(position);
                        Toast.makeText(getActivity(), model.toString(), Toast.LENGTH_SHORT).show();

                        if (model.getAppName().equalsIgnoreCase("Box uneg uneg")) {
                            Intent intent = new Intent(getActivity(), BoxUnegUnegActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, "box");
                            startActivity(intent);
                            getActivity().finish();

                            SessionUtil.removeKeyPreferences(ISeasonConfig.KEY_QNA_DATA_CATEGORY);
                        } else if (model.getAppName().equalsIgnoreCase("Chat motivator")) {
                            Intent intent = new Intent(getActivity(), AppActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, "chat");
                            startActivity(intent);
                            getActivity().finish();

                            SessionUtil.removeKeyPreferences(ISeasonConfig.KEY_QNA_DATA_CATEGORY);
                        } else if (model.getAppName().equalsIgnoreCase("Call Motivator")) {
                            Intent intent = new Intent(getActivity(), AppActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, "call");
                            startActivity(intent);
                            getActivity().finish();

                            SessionUtil.removeKeyPreferences(ISeasonConfig.KEY_QNA_DATA_CATEGORY);
                        }
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    private void initQuotes() {
        new ArticleQuotesModel();
        List<ArticleQuotesModel> models = new ArrayList<>();
        models.add(new ArticleQuotesModel(1, R.drawable.quoar1, "kanzankazu", "quotes"));
        models.add(new ArticleQuotesModel(2, R.drawable.quoar2, "kanzankazukanzankazu", "quotes"));
        models.add(new ArticleQuotesModel(3, R.drawable.quoar3, "kanzankazukanzankazukanzankazu", "quotes"));
        models.add(new ArticleQuotesModel(4, R.drawable.quoar1, "kanzankazukanzankazukanzankazukanzankazu", "quotes"));
        models.add(new ArticleQuotesModel(5, R.drawable.quoar2, "kanzankazukanzankazukanzankazukanzankazukanzankazu", "quotes"));

        adapterClientHomeArticleQuotes = new ClientHomeArticleQuotesAdapter(getActivity(), models);
        rvClientHomeQuotesfvbi.setAdapter(adapterClientHomeArticleQuotes);
        rvClientHomeQuotesfvbi.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        rvClientHomeQuotesfvbi.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(),
                new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ArticleQuotesModel model = models.get(position);
                        Toast.makeText(getActivity(), model.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    private void initArticles() {
        new ArticleQuotesModel();
        List<ArticleQuotesModel> models = new ArrayList<>();
        models.add(new ArticleQuotesModel(1, R.drawable.quoar3, "kanzankazukanzankazukanzankazukanzankazukanzankazu", "article"));
        models.add(new ArticleQuotesModel(2, R.drawable.quoar1, "kanzankazukanzankazu", "article"));
        models.add(new ArticleQuotesModel(3, R.drawable.quoar2, "kanzankazukanzankazukanzankazu", "article"));
        models.add(new ArticleQuotesModel(4, R.drawable.quoar3, "kanzankazukanzankazukanzankazukanzankazu", "article"));
        models.add(new ArticleQuotesModel(5, R.drawable.quoar1, "kanzankazukanzankazukanzankazukanzankazukanzankazu", "article"));

        adapterClientHomeArticleQuotes = new ClientHomeArticleQuotesAdapter(getActivity(), models);
        rvClientHomeArticlefvbi.setAdapter(adapterClientHomeArticleQuotes);
        rvClientHomeArticlefvbi.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        LinearSnapHelper linearSnapHelper = new LinearSnapHelper();
        linearSnapHelper.attachToRecyclerView(rvClientHomeArticlefvbi);

        rvClientHomeArticlefvbi.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(),
                new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ArticleQuotesModel model = models.get(position);
                        Toast.makeText(getActivity(), model.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    public static void moveToClientQuestion(Activity activity, boolean isEmptyProfiles, int reqCode) {
        SessionUtil.removeKeyPreferences(ISeasonConfig.KEY_QNA_DATA);

        Intent intent = new Intent(activity, ClientQuestionActivity.class);
        intent.putExtra(ISeasonConfig.KEY_QNA_IS_EMPTY_PROFILE, isEmptyProfiles);
        activity.startActivityForResult(intent, reqCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (REQ_CODE_BOX == requestCode) {
            Log.d("Lihat", "onActivityResult ClientHomeFragmentMain : " + "RESULT_OK");
            checkResult(getActivity(), isEmptyProfile, requestCode, resultCode, data, BoxUnegUnegInputActivity.class, "box");
        } else if (requestCode == REQ_CODE_CHAT) {
            Log.d("Lihat", "onActivityResult ClientHomeFragmentMain : " + "REQ_CODE_CHAT");
            checkResult(getActivity(), isEmptyProfile, requestCode, resultCode, data, AppActivity.class, "chat");
        } else if (requestCode == REQ_CODE_CALL) {
            Log.d("Lihat", "onActivityResult ClientHomeFragmentMain : " + "REQ_CODE_CALL");
            checkResult(getActivity(), isEmptyProfile, requestCode, resultCode, data, AppActivity.class, "call");
        }
    }

    /**
     * @param activity
     * @param isEmptyProfile
     * @param defaultRequestCode
     * @param defaultResultCode
     * @param defaultData
     * @param targetIntentClass
     * @param type
     * @return if target class null return true/false
     */
    public static boolean checkResult(Activity activity, boolean isEmptyProfile, int defaultRequestCode, int defaultResultCode, Intent defaultData, @Nullable Class<?> targetIntentClass, @Nullable String type) {
        if (defaultResultCode == Activity.RESULT_OK) {
            Log.d("Lihat", "checkResult ClientHomeFragmentMain : " + "RESULT_OK");
            if (defaultData.hasExtra(ISeasonConfig.KEY_QNA_DATA)) {
                String dataQA = defaultData.getStringExtra(ISeasonConfig.KEY_QNA_DATA);

                boolean isEmptyProfileDone = defaultData.getBooleanExtra(ISeasonConfig.KEY_QNA_IS_EMPTY_PROFILE_DONE, false);

                if (isEmptyProfileDone) {
                    saveProfile(dataQA);
                    moveToClientQuestion(activity, false, defaultRequestCode);
                } else {
                    if (targetIntentClass == null) {
                        SessionUtil.setStringPreferences(ISeasonConfig.KEY_QNA_DATA, dataQA);
                        return true;
                    } else {
                        Intent intent = new Intent(activity, targetIntentClass);
                        intent.putExtra(ISeasonConfig.KEY_QNA_DATA, dataQA);
                        if (type != null)
                            intent.putExtra(ISeasonConfig.PARAM, type);
                        activity.startActivity(intent);
                        activity.finish();
                    }
                }
            } else {
                return false;
            }
        } else {
            Log.d("Lihat", "checkResult ClientHomeFragmentMain : " + "RESULT_CANCEL");
            if (isEmptyProfile) {
                Toast.makeText(activity, "Sahabat harus melengkapi data diri untuk melanjutkan proses selanjutnya.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(activity, "Sahabat harus melengkapi data permasalahan yang kami berikan agar kami tau bagaimana menyelesaikan permasalahan sahabat.", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return false;
    }

    private static void saveProfile(String dataQA) {

    }

    private void checkProfile() {
        isEmptyProfile = true;
    }
}
