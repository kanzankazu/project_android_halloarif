package id.halloarif.app.view.activity.Role.Motivator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pusher.pushnotifications.PushNotifications;

import java.nio.channels.Channel;
import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.view.activity.App.AppInfoModel;
import id.halloarif.app.view.activity.App.BoxUnegUnegDetailActivity;
import id.halloarif.app.view.activity.App.BoxUnegUnegModel;
import id.halloarif.app.view.activity.App.CallDetailActivity;
import id.halloarif.app.view.activity.App.ChatDetailActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MotivatorHomeFragmentMain extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private LinearLayout llMotivatorHomeInfoBoxUnegfvbi;
    private LinearLayout llMotivatorHomeInfoChatfvbi;
    private LinearLayout llMotivatorHomeInfoCallfvbi;
    private LinearLayout llMotivatorHomeAppfvbi;
    private RecyclerView rvMotivatorHomeAppfvbi;

    public MotivatorHomeFragmentMain() {
    }

    public static Fragment newInstance() {
        MotivatorHomeFragmentMain fragment = new MotivatorHomeFragmentMain();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_motivator_home, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        llMotivatorHomeInfoBoxUnegfvbi = (LinearLayout) view.findViewById(R.id.llMotivatorHomeInfoBoxUneg);
        llMotivatorHomeInfoChatfvbi = (LinearLayout) view.findViewById(R.id.llMotivatorHomeInfoChat);
        llMotivatorHomeInfoCallfvbi = (LinearLayout) view.findViewById(R.id.llMotivatorHomeInfoCall);
        llMotivatorHomeAppfvbi = (LinearLayout) view.findViewById(R.id.llMotivatorHomeApp);
        rvMotivatorHomeAppfvbi = (RecyclerView) view.findViewById(R.id.rvMotivatorHomeApp);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        initRecycleNew();
    }

    private void initListener() {
        llMotivatorHomeInfoBoxUnegfvbi.setOnClickListener(this);
        llMotivatorHomeInfoChatfvbi.setOnClickListener(this);
        llMotivatorHomeInfoCallfvbi.setOnClickListener(this);
    }

    private void initRecycleNew() {
        List<AppInfoModel> models = new ArrayList<>();
        models.add(new AppInfoModel(1, "Chat", "moveon", true, "12-11-1991", "Faisal", "percintaan", "marah", "diduain", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(2, "Call", "bangkit", true, "12-11-1991", "Bahri", "pertemanan", "sedih", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(2, "Call", "bangkit", true, "12-11-1991", "Bahri", "pertemanan", "sedih", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(3, "Chat", "moveon", true, "12-11-1991", "Kanzan", "keluarga", "bahagia", "as", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(5, "box", "", true, "12-11-1991", "Kazu", "studi & karir", "entahlah", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(4, "Chat", "moveon", true, "12-11-1991", "Kazu", "studi & karir", "entahlah", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(2, "Call", "bangkit", true, "12-11-1991", "Bahri", "pertemanan", "sedih", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(4, "Chat", "moveon", true, "12-11-1991", "Kazu", "studi & karir", "entahlah", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(5, "box", "", true, "12-11-1991", "Kazu", "studi & karir", "entahlah", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));

        MotivatorHomeFragmentMainAdapter adapter = new MotivatorHomeFragmentMainAdapter(getActivity(), models,
                new MotivatorHomeFragmentMainAdapter.MotivatorHomeFragmentMainAdapterListener() {
                    @Override
                    public void click(AppInfoModel model) {
                        Toast.makeText(getActivity(), model.toString(), Toast.LENGTH_SHORT).show();
                        BoxUnegUnegModel unegModel = new BoxUnegUnegModel();
                        unegModel.setId(model.getId());
                        unegModel.setKategori(model.getKategori() + ",=," + model.getPermasalahan() + ",=," + model.getPerasaan());
                        unegModel.setIdClient(1);
                        unegModel.setNamaClient(model.getUsername());
                        unegModel.setNamaAliasClient("CLIENT01");
                        unegModel.setIsiClient(model.getIsi());
                        unegModel.setDateTimeCreate("12/12/2012 12:12");
                        //unegModel.setIdMotivator(2);
                        //unegModel.setNamaMotivator();
                        //unegModel.setNamaAliasMotivator();
                        //unegModel.setIsiMotivator();
                        //unegModel.setDateTimeResponse();

                        if (model.getTipe_paket().equalsIgnoreCase("Box")) {
                            Intent intent = new Intent(getActivity(), BoxUnegUnegDetailActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, unegModel);
                            startActivityForResult(intent, ISeasonConfig.REQ_CODE_BOX);
                        } else if (model.getTipe_paket().equalsIgnoreCase("Chat")) {
                            Intent intent = new Intent(getActivity(), ChatDetailActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, model);
                            startActivity(intent);
                        } else if (model.getTipe_paket().equalsIgnoreCase("Call")) {
                            Intent intent = new Intent(getActivity(), CallDetailActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, model);
                            startActivity(intent);
                        }
                    }
                });
        rvMotivatorHomeAppfvbi.setAdapter(adapter);
        rvMotivatorHomeAppfvbi.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ISeasonConfig.REQ_CODE_BOX) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(getActivity(), "OK BOX", Toast.LENGTH_SHORT).show();
            } else {

            }
        } else if (requestCode == ISeasonConfig.REQ_CODE_CHAT) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(getActivity(), "OK CHAT", Toast.LENGTH_SHORT).show();
            } else {

            }
        } else if (requestCode == ISeasonConfig.REQ_CODE_CALL) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(getActivity(), "OK CALL", Toast.LENGTH_SHORT).show();
            } else {

            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == llMotivatorHomeInfoBoxUnegfvbi) {
            Intent intent = new Intent(getActivity(), HistoryProblemActivity.class);
            intent.putExtra(ISeasonConfig.PARAM, "box");
            startActivity(intent);
        } else if (view == llMotivatorHomeInfoChatfvbi) {
            Intent intent = new Intent(getActivity(), HistoryProblemActivity.class);
            intent.putExtra(ISeasonConfig.PARAM, "chat");
            startActivity(intent);
        } else if (view == llMotivatorHomeInfoCallfvbi) {
            Intent intent = new Intent(getActivity(), HistoryProblemActivity.class);
            intent.putExtra(ISeasonConfig.PARAM, "call");
            startActivity(intent);
        }
    }
}
