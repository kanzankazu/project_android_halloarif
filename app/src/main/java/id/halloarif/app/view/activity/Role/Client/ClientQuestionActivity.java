package id.halloarif.app.view.activity.Role.Client;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.model.startappquestion.QuestionMainModel;
import id.halloarif.app.util.ListArrayUtil;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.util.SystemUtil;
import id.halloarif.app.util.support.NonSwipeableViewPager;

/**
 * param (profile-review) true-true=QNA profile, true-false=QNA profile, false-true=QNA review, false-false=QNA problem
 * data json
 * return QNAdata(separator(,=,)) & check profile
 */
public class ClientQuestionActivity extends AppCompatActivity {

    public boolean isLastPageSwiped;
    public StringBuilder builderAnswer = new StringBuilder();
    public NonSwipeableViewPager vpClientHomeActivityQuestionfvbi;
    public List<QuestionMainModel> models = new ArrayList<>();
    public List<QuestionMainModel> models1 = new ArrayList<>();
    public List<QuestionMainModel> mainModels = new ArrayList<>();
    private SlideQuestionAdapter mPagerAdapter;
    private boolean isEmptyProfile;
    private boolean isReview;

    private ActionBar actionBar;
    private int iCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_home_question);

        initBar();
        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initBar() {
        actionBar = getSupportActionBar();
    }

    private void initComponent() {
        vpClientHomeActivityQuestionfvbi = (NonSwipeableViewPager) findViewById(R.id.vpClientHomeActivityQuestion);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.KEY_QNA_IS_EMPTY_PROFILE)) {
            isEmptyProfile = bundle.getBooleanExtra(ISeasonConfig.KEY_QNA_IS_EMPTY_PROFILE, false);
        } else {
            isEmptyProfile = false;
        }
        if (bundle.hasExtra(ISeasonConfig.KEY_QNA_IS_REVIEW)) {
            isReview = bundle.getBooleanExtra(ISeasonConfig.KEY_QNA_IS_REVIEW, false);
        } else {
            isReview = false;
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_QNA_DATA_CATEGORY)) {
            iCategory = SessionUtil.getIntPreferences(ISeasonConfig.KEY_QNA_DATA_CATEGORY, 0);
        } else {
            iCategory = -1;
        }
        Log.d("Lihat", "initSession ClientQuestionActivity : " + iCategory);
    }

    private void initContent() {
        initJsonQuestion();
    }

    private void initListener() {
        vpClientHomeActivityQuestionfvbi.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // changing the next button text 'NEXT' / 'GOT IT'
                if (position == models.size() - 1) {
                    // last page. make button text to GOT IT
                    isLastPageSwiped = true;
                } else {
                    // still pages are left
                    isLastPageSwiped = false;
                }
            }

            @Override
            public void onPageSelected(int i) {
                Log.d("Lihat", "onPageSelected ClientQuestionActivity : " + builderAnswer);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void initJsonQuestion() {
        String s;
        String s1 = null;

        if (isEmptyProfile) {
            actionBar.setTitle("Profile Input");
            s = SystemUtil.loadJSONFromAsset(ClientQuestionActivity.this, "questionProfileGet.json");
        } else {
            if (isReview) {
                actionBar.setTitle("Review Input");
                s = SystemUtil.loadJSONFromAsset(ClientQuestionActivity.this, "questionProblemReview.json");
            } else {
                actionBar.setTitle("Problem Input");
                s = SystemUtil.loadJSONFromAsset(ClientQuestionActivity.this, "questionProblemInfo.json");
                Log.d("Lihat", "initJsonQuestion ClientQuestionActivity : " + s);
                s1 = SystemUtil.loadJSONFromAsset(ClientQuestionActivity.this, "questionProblemInfoSub.json");
                Log.d("Lihat", "initJsonQuestion ClientQuestionActivity : " + s1);
            }
        }

        if (iCategory == -1) {

            JsonParser parser = new JsonParser();
            JsonElement mJson = parser.parse(s);
            Gson gson = new Gson();
            QuestionMainModel[] json = gson.fromJson(mJson, QuestionMainModel[].class);
            models = Arrays.asList(json);

            mainModels.addAll(models);

            initTablayoutViewpager(mainModels);
        } else {

            JsonParser parser = new JsonParser();
            JsonElement mJson = parser.parse(s);
            Gson gson = new Gson();
            QuestionMainModel[] json = gson.fromJson(mJson, QuestionMainModel[].class);
            models = Arrays.asList(json);

            JsonParser parser1 = new JsonParser();
            JsonElement mJson1 = parser1.parse(s1);
            Gson gson1 = new Gson();
            QuestionMainModel[] json1 = gson1.fromJson(mJson1, QuestionMainModel[].class);
            models1 = Arrays.asList(json1);

            mainModels.addAll(models);
            Log.d("Lihat", "initJsonQuestion ClientQuestionActivity 1 : " + mainModels.size());
            mainModels.add(models1.get(iCategory));
            Log.d("Lihat", "initJsonQuestion ClientQuestionActivity 2 : " + mainModels.size());

            initTablayoutViewpager(mainModels);
        }

    }

    private void initTablayoutViewpager(List<QuestionMainModel> models) {
        /*int tabIconColorSelect = ContextCompat.getColor(ClientQuestionActivity.this, R.color.colorPrimary);
        int tabIconColorUnSelect = ContextCompat.getColor(ClientQuestionActivity.this, R.color.grey);
        for (int i = 0; i < titleTab.length; i++) {
            if (i == 0) {
                tlHistoryPaymentMainfvbi.getTabAt(i).getIcon().setColorFilter(tabIconColorSelect, PorterDuff.Mode.SRC_IN);
            } else {
                tlHistoryPaymentMainfvbi.getTabAt(i).getIcon().setColorFilter(tabIconColorUnSelect, PorterDuff.Mode.SRC_IN);
            }
        }
        tlHistoryPaymentMainfvbi.setTabGravity(TabLayout.GRAVITY_FILL);
        tlHistoryPaymentMainfvbi.setEnabled(false);*/

        mPagerAdapter = new SlideQuestionAdapter(ClientQuestionActivity.this.getSupportFragmentManager(), models);
        vpClientHomeActivityQuestionfvbi.setAdapter(mPagerAdapter);
        vpClientHomeActivityQuestionfvbi.setOffscreenPageLimit(models.size());
        vpClientHomeActivityQuestionfvbi.setCurrentItem(0);
    }

    public void onFinish() {
        String dataQNA = ListArrayUtil.removeLastSeparatorStringBuilder(builderAnswer, ",=,");

        Log.d("Lihat", "onFinish ClientQuestionActivity : " + "OK");

        Intent intent = new Intent();
        intent.putExtra(ISeasonConfig.KEY_QNA_DATA, dataQNA);

        if (isEmptyProfile) {
            intent.putExtra(ISeasonConfig.KEY_QNA_IS_EMPTY_PROFILE_DONE, true);
        } else {
            intent.putExtra(ISeasonConfig.KEY_QNA_IS_EMPTY_PROFILE_DONE, false);
        }

        Log.d("Lihat", "onFinish ClientQuestionActivity : " + dataQNA);
        Toast.makeText(getApplicationContext(), dataQNA, Toast.LENGTH_SHORT).show();

        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah sahabat ingin membatalkan konsultasi ini?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                Log.d("Lihat", "onClick ClientQuestionActivity : " + "Canceled");
                Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();

            }
        });
        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
