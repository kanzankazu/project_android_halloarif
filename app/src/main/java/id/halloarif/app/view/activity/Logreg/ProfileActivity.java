package id.halloarif.app.view.activity.Logreg;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.InputValidUtil;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.util.support.cropimage.CropImage;
import id.halloarif.app.util.support.cropimage.InternalStorageContentProvider;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
    private static final int RequestPermissionCode = 1;

    private CardView cvProfileChangePicfvbi;
    private CircleImageView civProfileUserfvbi;
    private EditText etProfileNamefvbi, etProfileEmailfvbi, etProfilePhoneNofvbi, etProfileBirthdtfvbi, etProfileProfesionfvbi, etProfileCityfvbi, etProfileProvincyfvbi;
    private ImageView ivProfileBirthdtfvbi;
    private Spinner spProfileReligionfvbi;
    private Button bProfileSavefvbi;

    private int userId;

    private File mFileTemp;
    private String str_path;
    private Bitmap bitmap;
    private String image_path;
    private byte[] imageBytes;
    private String encodedImage;

    public static void copyStream(InputStream input, OutputStream output) throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        cvProfileChangePicfvbi = (CardView) findViewById(R.id.cvProfileChangePic);
        civProfileUserfvbi = (CircleImageView) findViewById(R.id.civProfileUser);
        etProfileNamefvbi = (EditText) findViewById(R.id.etProfileName);
        etProfileEmailfvbi = (EditText) findViewById(R.id.etProfileEmail);
        etProfilePhoneNofvbi = (EditText) findViewById(R.id.etProfilePhoneNo);
        etProfileBirthdtfvbi = (EditText) findViewById(R.id.etProfileBirthdt);
        ivProfileBirthdtfvbi = (ImageView) findViewById(R.id.ivProfileBirthdt);
        spProfileReligionfvbi = (Spinner) findViewById(R.id.spProfileReligion);
        etProfileProfesionfvbi = (EditText) findViewById(R.id.etProfileProfesion);
        etProfileCityfvbi = (EditText) findViewById(R.id.etProfileCity);
        etProfileProvincyfvbi = (EditText) findViewById(R.id.etProfileProvincy);
        bProfileSavefvbi = (Button) findViewById(R.id.bProfileSave);

    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_USER_ID, 0);
        }
    }

    private void initContent() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
            System.out.println("---path 3v---" + mFileTemp.getPath());
            str_path = mFileTemp.getPath();
        } else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
            System.out.println("---path 3---" + mFileTemp.getPath());
            str_path = mFileTemp.getPath();
        }
    }

    private void initListener() {
        civProfileUserfvbi.setOnClickListener(this);
        ivProfileBirthdtfvbi.setOnClickListener(this);
        bProfileSavefvbi.setOnClickListener(this);
        cvProfileChangePicfvbi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        etProfileProvincyfvbi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_GO) {
                    regisValid();
                }
                return handled;
            }
        });

        if (view == civProfileUserfvbi) {
        } else if (view == ivProfileBirthdtfvbi) {

        } else if (view == bProfileSavefvbi) {
            regisValid();
        } else if (view == cvProfileChangePicfvbi) {
            if (checkPermission()) {
                camera();
                Log.e("PERMISSION", "CHECK");
            } else {
                requestPermission();
            }
        }
    }

    private void regisValid() {
        String sName = etProfileNamefvbi.getText().toString().trim();
        String sEmail = etProfileEmailfvbi.getText().toString().trim();
        String sPhoneNo = etProfilePhoneNofvbi.getText().toString().trim();
        String sBirthdt = etProfileBirthdtfvbi.getText().toString().trim();
        String sProfesion = etProfileProfesionfvbi.getText().toString().trim();
        String sCity = etProfileCityfvbi.getText().toString().trim();
        String sProvincy = etProfileProvincyfvbi.getText().toString().trim();

        if (!InputValidUtil.isEmptyField("Kolom ini masih kosong", etProfileNamefvbi, etProfileEmailfvbi, etProfilePhoneNofvbi, etProfileBirthdtfvbi, etProfileProfesionfvbi, etProfileCityfvbi, etProfileProvincyfvbi)) {
            if (!InputValidUtil.isValidateEmail(sEmail)) {
                InputValidUtil.errorET(etProfileEmailfvbi, "Email Format Salah, yyyyy@yyyyy.com");
            } else if (!InputValidUtil.isValidatePhoneNumber(sPhoneNo)) {
                InputValidUtil.errorET(etProfilePhoneNofvbi, "No HP Format Salah, menggunakan 0/+62");
            } else {
                Toast.makeText(getApplicationContext(), "data aman", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Field masih ada yang kosong", Toast.LENGTH_SHORT).show();
        }
    }

    private void onFinish() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Sahabat ingin keluar dari profile ?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });
        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {

            case REQUEST_CODE_GALLERY:
                try {

                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {

                    Log.d("Lihat", "onActivityResult ProfileActivity : " + e.toString());
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;
            case REQUEST_CODE_CROP_IMAGE:

                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }
                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());

                image_path = getStringImage(bitmap);

                civProfileUserfvbi.setImageBitmap(bitmap);

                System.out.println("---path 3---" + path);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void startCropImage() {
        Intent intent = new Intent(getApplicationContext(), CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        imageBytes = baos.toByteArray();

        Log.e("imageBytes", "" + imageBytes);

        encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        Log.e("encodedImage", encodedImage);
        return encodedImage;
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(this, CAMERA);
        int read_result1 = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && read_result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, CAMERA, READ_EXTERNAL_STORAGE}, RequestPermissionCode);
    }

    private void camera() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.YourDialogStyle).setIcon(R.drawable.ic_add_photo).setTitle("Add Photo!");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    try {
                        Uri mImageCaptureUri = null;
                        String state = Environment.getExternalStorageState();
                        if (Environment.MEDIA_MOUNTED.equals(state)) {
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                mImageCaptureUri = FileProvider.getUriForFile(getApplicationContext(), getResources().getString(R.string.provider), mFileTemp);
                            } else {
                                mImageCaptureUri = Uri.fromFile(mFileTemp);
                            }
                        } else {
                            mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                mImageCaptureUri);
                        intent.putExtra("return-data", true);
                        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
                    } catch (ActivityNotFoundException e) {
                        Log.d("Lihat", "onClick ProfileActivity : " + e.toString());
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean camaraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean readPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (StoragePermission && camaraPermission && readPermission) {
                        // Toast.makeText(this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        //  Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }
}
