package id.halloarif.app.view.activity.App;

class ChatDetailModel {
    private int id;
    private String created_at;
    private String message;
    private boolean is_read;
    private int topup_id;
    private int user_id;
    private int motivator_id;
    private int kategori_id;

    public ChatDetailModel(int id, String created_at, boolean is_read, int topup_id, int user_id, String message) {
        this.id = id;
        this.created_at = created_at;
        this.message = message;
        this.is_read = is_read;
        this.topup_id = topup_id;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIs_read() {
        return is_read;
    }

    public void setIs_read(boolean is_read) {
        this.is_read = is_read;
    }

    public int getTopup_id() {
        return topup_id;
    }

    public void setTopup_id(int topup_id) {
        this.topup_id = topup_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getMotivator_id() {
        return motivator_id;
    }

    public void setMotivator_id(int motivator_id) {
        this.motivator_id = motivator_id;
    }

    public int getKategori_id() {
        return kategori_id;
    }

    public void setKategori_id(int kategori_id) {
        this.kategori_id = kategori_id;
    }

    @Override
    public String toString() {
        return "ChatDetailModel{" +
                "id=" + id +
                ", created_at='" + created_at + '\'' +
                ", message='" + message + '\'' +
                ", is_read=" + is_read +
                ", topup_id=" + topup_id +
                ", user_id=" + user_id +
                ", motivator_id=" + motivator_id +
                ", kategori_id=" + kategori_id +
                '}';
    }
}
