package id.halloarif.app.view.activity.Role.Motivator;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.DateTimeUtil;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.util.SystemUtil;
import id.halloarif.app.util.support.TimePickerFragment;

public class MotivatorScheduleFragmentMain extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private RecyclerView rvSceduleMotivatorfvbi;
    private Toolbar tbMotivatorSchedulefvbi;

    private DatePickerDialog dpd;
    private TimePickerDialog tpd;
    private int roleId;
    private MotivatorScheduleAdapter scheduleAdapter;
    private int userId;

    public MotivatorScheduleFragmentMain() {
        // Required empty public constructor
    }

    public static MotivatorScheduleFragmentMain newInstance(String param1, String param2) {
        MotivatorScheduleFragmentMain fragment = new MotivatorScheduleFragmentMain();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        MotivatorScheduleFragmentMain fragment = new MotivatorScheduleFragmentMain();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_motivator_schedule, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        rvSceduleMotivatorfvbi = (RecyclerView) view.findViewById(R.id.rvSceduleMotivator);
    }

    private void initParam() {

    }

    private void initSession() {
        /*if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_USER_ID, 0);
        }*/
        userId = 1;
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
            roleId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
        }
    }

    private void initContent() {
        initRecycleJadwal();
    }

    private void initListener() {

    }

    private void initRecycleJadwal() {
        ArrayList<MotivatorScheduleModel> models = new ArrayList<>();
        models.add(new MotivatorScheduleModel(1, "Senin", "12:12:12", "12:12:12", 1));
        models.add(new MotivatorScheduleModel(2, "Selasa", "12:12:12", "12:12:12", 1));
        models.add(new MotivatorScheduleModel(3, "Rabu", "12:12:12", "12:12:12", 1));
        models.add(new MotivatorScheduleModel(4, "Kamis", "12:12:12", "12:12:12", 1));

        scheduleAdapter = new MotivatorScheduleAdapter(getActivity(), models, roleId,
                new MotivatorScheduleAdapter.MotivatorScheduleAdapterListener() {
                    @Override
                    public void onEdit(MotivatorScheduleModel model) {
                        Toast.makeText(getActivity(), "Edit " + model.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDelete(MotivatorScheduleModel model) {
                        Toast.makeText(getActivity(), "Delete " + model.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
        rvSceduleMotivatorfvbi.setAdapter(scheduleAdapter);
        rvSceduleMotivatorfvbi.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_add, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_bar_add:
                dialogAddSchedule();
                return false;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    private void dialogAddSchedule() {
        Dialog dialogAddSchedule;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogAddSchedule = new Dialog(getActivity(), android.R.style.Theme_DeviceDefault_Light_Dialog);
        } else {
            dialogAddSchedule = new Dialog(getActivity());
        }
        dialogAddSchedule.requestWindowFeature(Window.FEATURE_NO_TITLE);//untuk tidak ada title
        dialogAddSchedule.setContentView(R.layout.popaddschedule);
        dialogAddSchedule.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));//untuk menghilangkan background
        dialogAddSchedule.setTitle("");
        dialogAddSchedule.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams layoutparams = new WindowManager.LayoutParams();
        layoutparams.copyFrom(dialogAddSchedule.getWindow().getAttributes());
        layoutparams.width = WindowManager.LayoutParams.MATCH_PARENT;//ukuran lebar layout
        layoutparams.height = WindowManager.LayoutParams.WRAP_CONTENT;//ukuran tinggi layout

        // set the custom dialogAddSchedule components - text, image and button
        //AddSchedule = () dialogAddSchedule.findViewById(R.id.);
        LinearLayout llPopAddScheduleMotivator = (LinearLayout) dialogAddSchedule.findViewById(R.id.llPopAddScheduleMotivator);
        Spinner spPopAddScheduleMotivator = (Spinner) dialogAddSchedule.findViewById(R.id.spPopAddScheduleMotivator);
        Spinner spPopAddScheduleDayAddSchedule = (Spinner) dialogAddSchedule.findViewById(R.id.spPopAddScheduleDay);
        TextView tvPopAddScheduleStartAddSchedule = (TextView) dialogAddSchedule.findViewById(R.id.tvPopAddScheduleStart);
        TextView tvPopAddScheduleEndAddSchedule = (TextView) dialogAddSchedule.findViewById(R.id.tvPopAddScheduleEnd);
        CardView cvPopAddScheduleSaveAddSchedule = (CardView) dialogAddSchedule.findViewById(R.id.cvPopAddScheduleSave);
        CardView cvPopAddScheduleBackAddSchedule = (CardView) dialogAddSchedule.findViewById(R.id.cvPopAddScheduleBack);

        if (roleId == ISeasonConfig.ROLE_HUMANCAPITAL) {
            llPopAddScheduleMotivator.setVisibility(View.VISIBLE);
        } else {
            llPopAddScheduleMotivator.setVisibility(View.GONE);
        }

        Calendar now = Calendar.getInstance();

        List listDay = new ArrayList<String>();
        listDay.add("Pilih Hari");
        listDay.add("Minggu");
        listDay.add("Senin");
        listDay.add("Selasa");
        listDay.add("Rabu");
        listDay.add("Kamis");
        listDay.add("Jumat");
        listDay.add("Sabtu");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listDay) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    SystemUtil.changeColText(R.color.gray, tv);
                } else {
                    SystemUtil.changeColText(R.color.colorPrimary, tv);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPopAddScheduleDayAddSchedule.setAdapter(adapter);

        tvPopAddScheduleEndAddSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePicker(tvPopAddScheduleEndAddSchedule);
            }
        });
        tvPopAddScheduleStartAddSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePicker(tvPopAddScheduleStartAddSchedule);
            }
        });

        cvPopAddScheduleSaveAddSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (roleId == ISeasonConfig.ROLE_HUMANCAPITAL) {
                    if (spPopAddScheduleMotivator.getSelectedItemPosition() != 0) {
                        if (spPopAddScheduleDayAddSchedule.getSelectedItemPosition() != 0) {
                            if (!TextUtils.isEmpty(tvPopAddScheduleStartAddSchedule.getText().toString()) && !TextUtils.isEmpty(tvPopAddScheduleEndAddSchedule.getText().toString())) {

                                /*Date currentDate = DateTimeUtil.getCurrentDate();
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                                String timeString = DateTimeUtil.dateToString(currentDate, timeFormat);
                                String dateString = DateTimeUtil.dateToString(currentDate, dateFormat);
                                String dayFromDateString = DateTimeUtil.getDayFromDateString(dateString, dateFormat);*/

                                scheduleAdapter.addData(new MotivatorScheduleModel(adapter.getCount() + 1,
                                        spPopAddScheduleDayAddSchedule.getSelectedItem().toString().trim(),
                                        tvPopAddScheduleStartAddSchedule.getText().toString(),
                                        tvPopAddScheduleEndAddSchedule.getText().toString(),
                                        userId));

                                dialogAddSchedule.dismiss();

                                Snackbar.make(getActivity().findViewById(android.R.id.content),
                                        spPopAddScheduleDayAddSchedule.getSelectedItem().toString().trim()
                                                + ",=," +
                                                tvPopAddScheduleStartAddSchedule.getText().toString().trim()
                                                + ",=," +
                                                tvPopAddScheduleEndAddSchedule.getText().toString().trim(), Snackbar.LENGTH_SHORT).show();
                            } else {
                                Snackbar.make(getActivity().findViewById(android.R.id.content), "jam masih kosong", Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            Snackbar.make(getActivity().findViewById(android.R.id.content), "hari masih kosong", Snackbar.LENGTH_SHORT).show();
                        }
                    } else {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Motivator masih kosong", Snackbar.LENGTH_SHORT).show();
                    }

                } else {
                    if (spPopAddScheduleDayAddSchedule.getSelectedItemPosition() != 0) {
                        if (!TextUtils.isEmpty(tvPopAddScheduleStartAddSchedule.getText().toString()) && !TextUtils.isEmpty(tvPopAddScheduleEndAddSchedule.getText().toString())) {

                            scheduleAdapter.addData(new MotivatorScheduleModel(adapter.getCount() + 1,
                                    spPopAddScheduleDayAddSchedule.getSelectedItem().toString().trim(),
                                    tvPopAddScheduleStartAddSchedule.getText().toString(),
                                    tvPopAddScheduleEndAddSchedule.getText().toString(),
                                    userId));

                            dialogAddSchedule.dismiss();

                            Snackbar.make(getActivity().findViewById(android.R.id.content),
                                    spPopAddScheduleDayAddSchedule.getSelectedItem().toString().trim()
                                            + ",=," +
                                            tvPopAddScheduleStartAddSchedule.getText().toString().trim()
                                            + ",=," +
                                            tvPopAddScheduleEndAddSchedule.getText().toString().trim(), Snackbar.LENGTH_SHORT).show();
                        } else {
                            Snackbar.make(getActivity().findViewById(android.R.id.content), "jam masih kosong", Snackbar.LENGTH_SHORT).show();
                        }
                    } else {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "hari masih kosong", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });
        cvPopAddScheduleBackAddSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogAddSchedule.dismiss();
            }
        });

        dialogAddSchedule.show();
        dialogAddSchedule.getWindow().setAttributes(layoutparams);
    }

    private void showTimePicker(TextView edittext) {
        TimePickerFragment time = new TimePickerFragment();

        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("hour", calender.get(Calendar.HOUR_OF_DAY));
        args.putInt("minute", calender.get(Calendar.MINUTE));
        time.setArguments(args);

        time.setCallBack(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                edittext.setText(String.format("%02d:%02d", hour, minute));
            }
        });
        time.show(getFragmentManager(), "Date Picker");
    }
}
