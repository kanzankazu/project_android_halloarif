package id.halloarif.app.view;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.javiersantos.appupdater.AppUpdater;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PushNotificationsInstance;

import java.net.URISyntaxException;
import java.util.Set;

import id.halloarif.app.IConfig;
import id.halloarif.app.R;

public class MainActivity extends AppCompatActivity {
    private static final String url = IConfig.API_BASE_URL;
    private static final String TAG = "WebViewCookies";
    private static final int REQUEST_SELECT_FILE = 100;
    private static int FILECHOOSER_RESULTCODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 101;

    WebView wvMainActivityfvbi;
    ProgressBar pbMainActivityfvbi;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    private MainActivity mBinding;
    private PermissionRequest myRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        pbMainActivityfvbi = findViewById(R.id.pbMainActivity);
        wvMainActivityfvbi = findViewById(R.id.wvMainActivity);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        pbMainActivityfvbi.setVisibility(View.GONE);
        pbMainActivityfvbi.setMax(100);

        WebSettings mWebSettings = wvMainActivityfvbi.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowContentAccess(true);

        wvMainActivityfvbi.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                pbMainActivityfvbi.setProgress(progress);
                if (progress == 100) {
                    pbMainActivityfvbi.setVisibility(View.GONE);
                } else {
                    pbMainActivityfvbi.setVisibility(View.VISIBLE);
                }
            }

            // For 3.0+ Devices (Start)
            // onActivityResult attached before constructor
            protected void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Browser"), FILECHOOSER_RESULTCODE);
            }

            // For Lollipop 5.0+ Devices
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = fileChooserParams.createIntent();
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(MainActivity.this.getApplicationContext(), "Cannot Open File Chooser", Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "File Browser"), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
            }

            @SuppressLint("NewApi")
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                myRequest = request;

                for (String permission : request.getResources()) {
                    switch (permission) {
                        case "android.webkit.resource.AUDIO_CAPTURE": {
                            askForPermission(request.getOrigin().toString(), android.Manifest.permission.RECORD_AUDIO, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
                            break;
                        }
                    }
                }
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
                AlertDialog.Builder b = new AlertDialog.Builder(view.getContext())
                        .setTitle(null)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                result.confirm();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                result.cancel();
                            }
                        });

                b.show();

                // Indicate that we're handling this manually
                return true;
            }
        });
        wvMainActivityfvbi.setWebViewClient(new MyBrowser());
        wvMainActivityfvbi.loadUrl(url);

        checkUpdate();
    }

    private void initListener() {

    }

    private void checkUpdate() {
        AppUpdater appUpdater = new AppUpdater(this)
                .setTitleOnUpdateAvailable("Pembaruan Tersedia!")
                .setContentOnUpdateAvailable("Ada versi terbaru dari aplikasi Hallo Arif, Anda ingin melakukan pembaruan aplikasi sekarang?")
                .setButtonUpdate("Ya, Perbarui")
                .setButtonDismiss("Tidak, Terima Kasih")
                .setButtonDoNotShowAgain(null);
        appUpdater.start();
    }

    public String getInstanceId() {
        return "3d73514f-b811-4161-80e2-a1cd1bbd068e";
    }

    public String getCookie(String siteName, String CookieName) {
        String CookieValue = null;

        CookieManager cookieManager = CookieManager.getInstance();
        String cookies = cookieManager.getCookie(siteName);
        cookieManager.setAcceptCookie(true);
        if (cookies != null) {
            String[] temp = cookies.split(";");
            for (String ar1 : temp) {
                if (ar1.contains(CookieName)) {
                    String[] temp1 = ar1.split("=");
                    CookieValue = temp1[1];
                }
            }
        }
        Log.i(TAG, "all cookie: " + cookies);
        Log.i(TAG, CookieName + " cookie: " + CookieValue);
        return CookieValue;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && wvMainActivityfvbi.canGoBack()) {
            wvMainActivityfvbi.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL,
                        Uri.parse(url));
                startActivity(intent);
            } else if (url.startsWith("whatsapp:")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    if (intent.resolveActivity(getPackageManager()) != null)
                        startActivity(intent);
                    return true;
                } catch (URISyntaxException use) {
                    Log.e(TAG, use.getMessage());
                }
            } else if (url.startsWith("gojek:")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    if (intent.resolveActivity(getPackageManager()) != null)
                        startActivity(intent);
                    return true;
                } catch (URISyntaxException use) {
                    Log.e(TAG, use.getMessage());
                }
            } else if (url.startsWith("http:") || url.startsWith("https:")) {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            view.loadData("Maaf Internet Anda tidak stabil", "text/html", "utf-8");
            super.onReceivedError(view, request, error);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.i(TAG, "url: " + url);

            String channel = getCookie(url, "channel");
            String channel_destroy = getCookie(url, "channel_destroy");

            PushNotificationsInstance start = PushNotifications.start(getApplicationContext(), getInstanceId());

            if (channel != null) {
                PushNotifications.subscribe(channel);
                Log.i(TAG, "subscribe channel: " + channel);

                /*
                List<String> list = Arrays.asList(channel);
                Set<String> set = new HashSet<>(list);
                PushNotifications.setSubscriptions(set);
                Log.i(TAG, "subscribe channel: " + set);
                */

            } else {
                PushNotifications.unsubscribeAll();
                Log.i(TAG, "unsubscribe all channel");

                CookieManager.getInstance().removeAllCookie();
                Log.i(TAG, "remove all cookie");
            }

            if (channel != null && channel.equals("public") && channel_destroy != null) {
                PushNotifications.unsubscribe(channel_destroy);
                Log.i(TAG, "unsubscribe " + channel_destroy + " channel");

                CookieManager.getInstance().removeAllCookie();
                Log.i(TAG, "remove all cookie");
            }

            Set<String> subList = PushNotifications.getSubscriptions();
            Log.i(TAG, "subscribe list: " + subList);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
            // Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
            // Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = intent == null || resultCode != MainActivity.RESULT_OK ? null : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else
            Toast.makeText(MainActivity.this.getApplicationContext(), "Failed to Upload Image", Toast.LENGTH_LONG).show();
    }

    @SuppressLint("NewApi")
    public void askForPermission(String origin, String permission, int requestCode) {
        Log.d("WebView", "inside askForPermission for" + origin + "with" + permission);

        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            myRequest.grant(myRequest.getResources());
        }
    }
}
