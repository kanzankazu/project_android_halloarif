package id.halloarif.app.view.activity.Role.Motivator;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.halloarif.app.R;
import id.halloarif.app.view.activity.App.AppInfoModel;

public class MotivatorHomeFragmentMainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<AppInfoModel> models;
    private MotivatorHomeFragmentMainAdapterListener listener;
    private Activity activity;

    public MotivatorHomeFragmentMainAdapter(Activity activity, List<AppInfoModel> models, MotivatorHomeFragmentMainAdapterListener listener) {
        this.activity = activity;
        this.models = models;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_motivator_new_response, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AppInfoModel model = models.get(position);

        if (viewHolder instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;

            if (model.isEnable()) {

            } else {

            }

            holder.civListItemNewResponseImagefvbi.setImageResource(R.drawable.gallery_photo);
            holder.tvListItemNewResponseNamefvbi.setText(model.getUsername());
            holder.tvListItemNewResponseDateTimefvbi.setText(model.getDate_time_detail_expired());
            holder.tvListItemNewResponsePaketTypefvbi.setText(model.getTipe_paket());
            holder.tvListItemNewResponsePaketNamefvbi.setText(model.getNama_paket());
            holder.tvListItemNewResponseKategorifvbi.setText(model.getKategori());
            holder.tvListItemNewResponsePerasaanfvbi.setText(model.getPerasaan());
            holder.tvListItemNewResponsePermasalahanfvbi.setText(model.getPermasalahan());
            holder.cvListItemNewResponseExecutefvbi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.click(model);
                }
            });
            holder.cvListItemNewResponseLihatfvbi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.click(model);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView civListItemNewResponseImagefvbi;
        private TextView tvListItemNewResponseNamefvbi;
        private TextView tvListItemNewResponseDateTimefvbi;
        private TextView tvListItemNewResponsePaketTypefvbi;
        private TextView tvListItemNewResponsePaketNamefvbi;
        private TextView tvListItemNewResponseKategorifvbi;
        private TextView tvListItemNewResponsePerasaanfvbi;
        private TextView tvListItemNewResponsePermasalahanfvbi;
        private CardView cvListItemNewResponseExecutefvbi;
        private CardView cvListItemNewResponseLihatfvbi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            civListItemNewResponseImagefvbi = (CircleImageView) itemView.findViewById(R.id.civListItemNewResponseImage);
            tvListItemNewResponseNamefvbi = (TextView) itemView.findViewById(R.id.tvListItemNewResponseName);
            tvListItemNewResponseDateTimefvbi = (TextView) itemView.findViewById(R.id.tvListItemNewResponseDateTime);
            tvListItemNewResponsePaketTypefvbi = (TextView) itemView.findViewById(R.id.tvListItemNewResponsePaketType);
            tvListItemNewResponsePaketNamefvbi = (TextView) itemView.findViewById(R.id.tvListItemNewResponsePaketName);
            tvListItemNewResponseKategorifvbi = (TextView) itemView.findViewById(R.id.tvListItemNewResponseKategori);
            tvListItemNewResponsePerasaanfvbi = (TextView) itemView.findViewById(R.id.tvListItemNewResponsePerasaan);
            tvListItemNewResponsePermasalahanfvbi = (TextView) itemView.findViewById(R.id.tvListItemNewResponsePermasalahan);
            cvListItemNewResponseExecutefvbi = (CardView) itemView.findViewById(R.id.cvListItemNewResponseExecute);
            cvListItemNewResponseLihatfvbi = (CardView) itemView.findViewById(R.id.cvListItemNewResponseLihat);
        }
    }

    public interface MotivatorHomeFragmentMainAdapterListener {
        void click(AppInfoModel model);
    }

}
