package id.halloarif.app.view.activity.Main;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.text.ClipboardManager;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.model.ListSettingModel;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.util.support.Recycleritemclicklistener;
import id.halloarif.app.view.activity.Logreg.LoginActivity;
import id.halloarif.app.view.activity.Logreg.ProfileActivity;
import id.halloarif.app.view.activity.Role.Finance.FinanceConfirmActivity;
import id.halloarif.app.view.activity.Role.Finance.FinanceCouponActivity;
import id.halloarif.app.view.activity.Role.Finance.FinancePaketActivity;

public class SettingsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQ_CODE_PROFILE = 1223;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private List<ListSettingModel> models = new ArrayList<>();
    private RecyclerView rvSettingsMainfvbi;
    private SettingsAdapter adapter;
    private int userId;
    private int roleId;
    private CircleImageView civSettingsUserPhotofvbi;
    private TextView tvSettingsUserNamefvbi, tvSettingsUserRolefvbi, tvSettingsUserPhoneNofvbi;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        civSettingsUserPhotofvbi = (CircleImageView) view.findViewById(R.id.civSettingsUserPhoto);
        tvSettingsUserNamefvbi = (TextView) view.findViewById(R.id.tvSettingsUserName);
        tvSettingsUserRolefvbi = (TextView) view.findViewById(R.id.tvSettingsUserRole);
        tvSettingsUserPhoneNofvbi = (TextView) view.findViewById(R.id.tvSettingsUserPhoneNo);
        rvSettingsMainfvbi = (RecyclerView) view.findViewById(R.id.rvSettingsMain);

    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_USER_ID, 0);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
            roleId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
        }
    }

    private void initContent() {
        if (roleId == ISeasonConfig.ROLE_MOTIVATOR) {
            models.add(new ListSettingModel("Code of Conduct", R.drawable.ic_help));
        } else if (roleId == ISeasonConfig.ROLE_CLIENT) {
            models.add(new ListSettingModel("Term of Use", R.drawable.ic_term));
        } else if (roleId == ISeasonConfig.ROLE_FINANCE) {
            models.add(new ListSettingModel("Konfirmasi Pembayaran", R.drawable.ic_call));
            models.add(new ListSettingModel("Paket", R.drawable.ic_term));
            models.add(new ListSettingModel("Kode Kupon", R.drawable.ic_help));
        } else if (roleId == ISeasonConfig.ROLE_HUMANCAPITAL) {
            models.add(new ListSettingModel("Slideshow", R.drawable.ic_term));
            models.add(new ListSettingModel("Settings", R.drawable.ic_help));
        } else if (roleId == ISeasonConfig.ROLE_ADMIN) {
            models.add(new ListSettingModel("Code of Conduct", R.drawable.ic_help));
        }
        /*ALL*/
        models.add(new ListSettingModel("Program Referal", R.drawable.ic_share));
        models.add(new ListSettingModel("Bantuan", R.drawable.ic_call));
        models.add(new ListSettingModel("FAQ", R.drawable.ic_help));
        models.add(new ListSettingModel("Keluar", R.drawable.ic_logout));

        adapter = new SettingsAdapter(getActivity(), models);
        rvSettingsMainfvbi.setAdapter(adapter);
        rvSettingsMainfvbi.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSettingsMainfvbi.setItemAnimator(new DefaultItemAnimator());
        rvSettingsMainfvbi.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL));

        tvSettingsUserRolefvbi.setText(roleIdToString(roleId));
    }

    private String roleIdToString(int roleId) {
        String role = null;
        if (roleId == ISeasonConfig.ROLE_MOTIVATOR) {
            role = "Motivator";
        } else if (roleId == ISeasonConfig.ROLE_CLIENT) {
            role = "Client";
        } else if (roleId == ISeasonConfig.ROLE_FINANCE) {
            role = "Finance";
        } else if (roleId == ISeasonConfig.ROLE_HUMANCAPITAL) {
            role = "Humancapital";
        } else if (roleId == ISeasonConfig.ROLE_ADMIN) {
            role = "Admin";
        }
        return role;
    }

    private void initListener() {
        civSettingsUserPhotofvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivityForResult(intent, REQ_CODE_PROFILE);
            }
        });

        rvSettingsMainfvbi.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(),
                new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ListSettingModel model = models.get(position);
                        Toast.makeText(getActivity(), model.toString(), Toast.LENGTH_SHORT).show();

                        /*MOTIVATOR*/
                        if (model.getTitle().equalsIgnoreCase("Code of Conduct")) {
                        } else /*CLIENT*/ if (model.getTitle().equalsIgnoreCase("term of use")) {
                            LoginActivity.dialogTermUseAggrement(getActivity(), getString(R.string.term_of_use));
                        } else /*FINANCE*/ if (model.getTitle().equalsIgnoreCase("Konfirmasi Pembayaran")) {
                            Intent intent = new Intent(getActivity(), FinanceConfirmActivity.class);
                            startActivity(intent);
                            //finish();
                        } else if (model.getTitle().equalsIgnoreCase("Kode Kupon")) {
                            Intent intent = new Intent(getActivity(), FinanceCouponActivity.class);
                            startActivity(intent);
                            //finish();
                        } else if (model.getTitle().equalsIgnoreCase("Paket")) {
                            Intent intent = new Intent(getActivity(), FinancePaketActivity.class);
                            startActivity(intent);
                            //finish();
                        } else /*HUMANCAPITAL*/ if (model.getTitle().equalsIgnoreCase("Slideshow")) {
                        } else if (model.getTitle().equalsIgnoreCase("Settings")) {
                        } else /*ALL*/ if (model.getTitle().equalsIgnoreCase("program referal")) {
                            dialogReveral();
                        } else if (model.getTitle().equalsIgnoreCase("bantuan")) {
                            dialogHelp();
                        } else if (model.getTitle().equalsIgnoreCase("faq")) {
                        } else if (model.getTitle().equalsIgnoreCase("keluar")) {
                            SessionUtil.removeAllSharedPreferences();

                            getActivity().finishAffinity();

                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    private void dialogReveral() {
        Dialog dialogReveral;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogReveral = new Dialog(getActivity(), android.R.style.Theme_DeviceDefault_Light_Dialog);
        } else {
            dialogReveral = new Dialog(getActivity());
        }
        dialogReveral.requestWindowFeature(Window.FEATURE_NO_TITLE);//untuk tidak ada title
        dialogReveral.setContentView(R.layout.popreferalcode);
        dialogReveral.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));//untuk menghilangkan background
        dialogReveral.setTitle("");

        WindowManager.LayoutParams layoutparams = new WindowManager.LayoutParams();
        layoutparams.copyFrom(dialogReveral.getWindow().getAttributes());
        layoutparams.width = WindowManager.LayoutParams.MATCH_PARENT;//ukuran lebar layout
        layoutparams.height = WindowManager.LayoutParams.WRAP_CONTENT;//ukuran tinggi layout

        // set the custom dialogReveral components - text, image and button
        //Reveral = () dialogReveral.findViewById(R.id.);
        EditText etPopReferalCodeReveral = (EditText) dialogReveral.findViewById(R.id.etPopReferalCode);
        CardView cvPopReferalCodeShareReveral = (CardView) dialogReveral.findViewById(R.id.cvPopReferalCodeShare);
        CardView cvPopReferalCodeCopyReveral = (CardView) dialogReveral.findViewById(R.id.cvPopReferalCodeCopy);

        String referal = etPopReferalCodeReveral.getText().toString().trim();

        cvPopReferalCodeShareReveral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(referal)) {
                    Intent intentSend = new Intent();
                    intentSend.setAction(Intent.ACTION_SEND);
                    intentSend.putExtra(Intent.EXTRA_TEXT, "Silahkan buka dan kemudian daftar, " +
                            "https://www.halloarif.id/ atau " +
                            "https://play.google.com/store/apps/details?id=id.halloarif.app&hl=en " +
                            "kemudian masukan kode referal ini saat mendaftar : " + referal +
                            " . Sahabat akan langsung mendapat paket curhat dan box uneg-uneg");
                    intentSend.setType("text/plain");
                    startActivity(intentSend);
                }
            }
        });
        cvPopReferalCodeCopyReveral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(referal)) {
                    int sdk = Build.VERSION.SDK_INT;
                    if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboard.setText(referal);
                        Toast.makeText(getActivity(), "tersalin", Toast.LENGTH_SHORT).show();
                    } else {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("text label", referal);
                        clipboard.setPrimaryClip(clip);
                        Toast.makeText(getActivity(), "Referal telah di salin", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        dialogReveral.show();
        dialogReveral.getWindow().setAttributes(layoutparams);
    }

    private void dialogHelp() {
        Dialog dialogHelp;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogHelp = new Dialog(getActivity(), android.R.style.Theme_DeviceDefault_Light_Dialog);
        } else {
            dialogHelp = new Dialog(getActivity());
        }
        dialogHelp.requestWindowFeature(Window.FEATURE_NO_TITLE);//untuk tidak ada title
        dialogHelp.setContentView(R.layout.pophelp);
        dialogHelp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));//untuk menghilangkan background
        dialogHelp.setTitle("");

        WindowManager.LayoutParams layoutparams = new WindowManager.LayoutParams();
        layoutparams.copyFrom(dialogHelp.getWindow().getAttributes());
        layoutparams.width = WindowManager.LayoutParams.MATCH_PARENT;//ukuran lebar layout
        layoutparams.height = WindowManager.LayoutParams.WRAP_CONTENT;//ukuran tinggi layout

        // set the custom dialogHelp components - text, image and button
        //Help = () dialogHelp.findViewById(R.id.);
        TextView tvPopHelpMainHelp = (TextView) dialogHelp.findViewById(R.id.tvPopHelpMain);

        customText(tvPopHelpMainHelp);

        dialogHelp.show();
        dialogHelp.getWindow().setAttributes(layoutparams);
    }

    private void customText(TextView textView) {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder("Sahabat dapat menghubungi bagian Client Relations kami di ");

        spanTxt.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 10, spanTxt.length(), 0);
        spanTxt.append("0857-66620096");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                String phone = "+6285766620096";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        }, spanTxt.length() - "0857-66620096".length(), spanTxt.length(), 0);

        /*spanTxt.append(" atau ");

        spanTxt.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 10, spanTxt.length(), 0);
        spanTxt.append(" Whatsapp ");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                try {
                    String number = "+" + "6283872203508";

                    *//*String toNumber = "6282180000081";
                    Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + toNumber + "?body=" + ""));
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);*//*

                    *//*Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);*//*

                    *//*Intent sendIntent = new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                    sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number) + "@s.whatsapp.net");
                    startActivity(sendIntent);*//*

                    *//*Intent sendIntent = new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "");
                    sendIntent.putExtra("jid", number + "@s.whatsapp.net");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);*//*

                    *//*SUCCESS*//*
                    String smsNumber = "6285766620096"; //without '+'
                    Intent sendIntent = new Intent("android.intent.action.MAIN");
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "");//text
                    sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "it may be you dont have whats app", Toast.LENGTH_LONG).show();

                }
            }
        }, spanTxt.length() - " Whatsapp ".length(), spanTxt.length(), 0);*/

        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_PROFILE) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().finish();
                startActivity(getActivity().getIntent());
            }
        }
    }
}
