package id.halloarif.app.view.activity.App;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;

public class AppPriceAdapter extends RecyclerView.Adapter<AppPriceAdapter.ViewHolder> {
    private Activity activity;
    private List<AppInfoModel> models;
    private int roleId;
    private String type;
    private final AppPriceAdapterListener listener;

    AppPriceAdapter(Activity activity, List<AppInfoModel> models, int roleId, String type, AppPriceAdapterListener listener) {
        this.activity = activity;
        this.models = models;
        this.roleId = roleId;
        this.type = type;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AppPriceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_price, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AppPriceAdapter.ViewHolder holder, int position) {
        AppInfoModel model = models.get(position);
        holder.tvlistPriceNamefvbi.setText(model.getNama_paket());
        holder.tvlistPricePricefvbi.setText(model.getHarga_paket());

        String s1 = null;
        if (type.equalsIgnoreCase("chat")) {
            s1 = model.isUnlimited() ? "Unlimited Chat" : (model.getApp_tersisa() + " Chat");
        } else if (type.equalsIgnoreCase("call")) {
            s1 = model.isUnlimited() ? "Unlimited Call" : (model.getApp_tersisa() + " Menit");
        }
        holder.tvlistPriceFeaturefvbi.setText(s1);

        holder.tvlistPriceDurasifvbi.setText("Masa Aktif " + model.getDate_expired() + " Hari");

        if (roleId == ISeasonConfig.ROLE_MOTIVATOR) {
            holder.cvlistPriceBuyfvbi.setVisibility(View.GONE);
            holder.cvlistPriceChatfvbi.setVisibility(View.VISIBLE);
            holder.cvlistPriceEditfvbi.setVisibility(View.GONE);
            holder.cvlistPriceRemovefvbi.setVisibility(View.GONE);
        } else if (roleId == ISeasonConfig.ROLE_CLIENT) {
            holder.cvlistPriceBuyfvbi.setVisibility(View.VISIBLE);
            holder.cvlistPriceChatfvbi.setVisibility(View.GONE);
            holder.cvlistPriceEditfvbi.setVisibility(View.GONE);
            holder.cvlistPriceRemovefvbi.setVisibility(View.GONE);
        } else if (roleId == ISeasonConfig.ROLE_FINANCE) {
            holder.cvlistPriceBuyfvbi.setVisibility(View.GONE);
            holder.cvlistPriceChatfvbi.setVisibility(View.GONE);
            holder.cvlistPriceEditfvbi.setVisibility(View.GONE);
            holder.cvlistPriceRemovefvbi.setVisibility(View.GONE);
        } else if (roleId == ISeasonConfig.ROLE_HUMANCAPITAL) {
            holder.cvlistPriceBuyfvbi.setVisibility(View.GONE);
            holder.cvlistPriceChatfvbi.setVisibility(View.GONE);
            holder.cvlistPriceEditfvbi.setVisibility(View.VISIBLE);
            holder.cvlistPriceRemovefvbi.setVisibility(View.VISIBLE);
        }

        holder.cvlistPriceEditfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onEdit();
            }
        });
        holder.cvlistPriceRemovefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onRemove();
            }
        });
        holder.cvlistPriceBuyfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBuy();
            }
        });
        holder.cvlistPriceChatfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onchat();
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvlistPriceNamefvbi;
        private final TextView tvlistPricePricefvbi;
        private final TextView tvlistPriceFeaturefvbi;
        private final TextView tvlistPriceDurasifvbi;
        private final CardView cvlistPriceEditfvbi;
        private final CardView cvlistPriceRemovefvbi;
        private final CardView cvlistPriceBuyfvbi;
        private final CardView cvlistPriceChatfvbi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvlistPriceNamefvbi = (TextView) itemView.findViewById(R.id.tvlistPriceName);
            tvlistPricePricefvbi = (TextView) itemView.findViewById(R.id.tvlistPricePrice);
            tvlistPriceFeaturefvbi = (TextView) itemView.findViewById(R.id.tvlistPriceFeature);
            tvlistPriceDurasifvbi = (TextView) itemView.findViewById(R.id.tvlistPriceDurasi);
            cvlistPriceEditfvbi = (CardView) itemView.findViewById(R.id.cvlistPriceEdit);
            cvlistPriceRemovefvbi = (CardView) itemView.findViewById(R.id.cvlistPriceRemove);
            cvlistPriceBuyfvbi = (CardView) itemView.findViewById(R.id.cvlistPriceBuy);
            cvlistPriceChatfvbi = (CardView) itemView.findViewById(R.id.cvlistPriceChat);
        }
    }

    public interface AppPriceAdapterListener {
        void onEdit();

        void onRemove();

        void onBuy();

        void onchat();
    }
}
