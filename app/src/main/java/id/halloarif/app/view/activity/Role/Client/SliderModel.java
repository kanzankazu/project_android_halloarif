package id.halloarif.app.view.activity.Role.Client;

class SliderModel {
    private int id;
    private String image;
    private String url;
    private Boolean status;
    private int sort;

    public SliderModel() {
    }

    public SliderModel(int id, String image, String url, Boolean status, int sort) {
        this.id = id;
        this.image = image;
        this.url = url;
        this.status = status;
        this.sort = sort;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "SliderModel{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", url='" + url + '\'' +
                ", status=" + status +
                ", sort=" + sort +
                '}';
    }
}
