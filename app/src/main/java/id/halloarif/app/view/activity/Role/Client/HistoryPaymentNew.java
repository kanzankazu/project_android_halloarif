package id.halloarif.app.view.activity.Role.Client;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.models.BillInfoModel;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;

import java.util.ArrayList;

import id.halloarif.app.R;

public class HistoryPaymentNew extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private Spinner spClientHistoryPaymentNewPaketfvbi;
    private EditText etClientHistoryPaymentNewKodePromofvbi;
    private EditText etClientHistoryPaymentNewNISfvbi;
    private EditText etClientHistoryPaymentNewSekolahfvbi;
    private Button bClientHistoryPaymentNewBelifvbi;

    public HistoryPaymentNew() {
        // Required empty public constructor
    }

    public static HistoryPaymentNew newInstance(String param1, String param2) {
        HistoryPaymentNew fragment = new HistoryPaymentNew();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        HistoryPaymentNew fragment = new HistoryPaymentNew();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_history_payment_new, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        spClientHistoryPaymentNewPaketfvbi = (Spinner) view.findViewById(R.id.spClientHistoryPaymentNewPaket);
        etClientHistoryPaymentNewKodePromofvbi = (EditText) view.findViewById(R.id.etClientHistoryPaymentNewKodePromo);
        etClientHistoryPaymentNewNISfvbi = (EditText) view.findViewById(R.id.etClientHistoryPaymentNewNIS);
        etClientHistoryPaymentNewSekolahfvbi = (EditText) view.findViewById(R.id.etClientHistoryPaymentNewSekolah);
        bClientHistoryPaymentNewBelifvbi = (Button) view.findViewById(R.id.bClientHistoryPaymentNewBeli);

    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        /*SdkUIFlowBuilder.init()
                .setClientKey("SB-Mid-client-KG_fodSSGT24kE0b") // client_key is mandatory
                .setContext(getActivity()) // context is mandatory
                .setTransactionFinishedCallback(new TransactionFinishedCallback() {
                    @Override
                    public void onTransactionFinished(TransactionResult result) {
                        if (result.getDetail() != null) {
                            switch (result.getStatus()) {
                                case TransactionResult.STATUS_SUCCESS:
                                    Toast.makeText(getActivity(), "Transaction Finished. ID: " + result.getDetail().getTransactionId(), Toast.LENGTH_LONG).show();
                                    break;
                                case TransactionResult.STATUS_PENDING:
                                    Toast.makeText(getActivity(), "Transaction Pending. ID: " + result.getDetail().getTransactionId(), Toast.LENGTH_LONG).show();
                                    break;
                                case TransactionResult.STATUS_FAILED:
                                    Toast.makeText(getActivity(), "Transaction Failed. ID: " + result.getDetail().getTransactionId() + ". Message: " + result.getDetail().getStatusMessage(), Toast.LENGTH_LONG).show();
                                    break;
                            }
                            result.getDetail().getValidationMessages();
                        } else if (result.isTransactionCanceled()) {
                            Toast.makeText(getActivity(), "Transaction Canceled", Toast.LENGTH_LONG).show();
                        } else {
                            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                                Toast.makeText(getActivity(), "Transaction Invalid", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Transaction Finished with failure.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl("https://ayokode.com/midtrans/checkout.php") //set merchant url (required) BASE_URL
                .enableLog(true) // enable sdk log (optional)
                .buildSDK();

        UserDetail userDetail = LocalDataHandler.readObject("user_details", UserDetail.class);
        if (userDetail == null) {
            userDetail = new UserDetail();
            userDetail.setUserFullName("B. Erfransyah Levi Darmawan");
            userDetail.setEmail("franslevi008@gmail.com");
            userDetail.setPhoneNumber("08123456789");

            ArrayList<UserAddress> userAddresses = new ArrayList<>();
            UserAddress userAddress = new UserAddress();
            userAddress.setAddress("Jl Slipi No 15");
            userAddress.setCity("Jakarta");
            userAddress.setAddressType(com.midtrans.sdk.corekit.core.Constants.ADDRESS_TYPE_BOTH);
            userAddress.setZipcode("40184");
            userAddress.setCountry("IDN");
            userAddresses.add(userAddress);
            userDetail.setUserAddresses(userAddresses);
            LocalDataHandler.saveObject("user_details", userDetail);
        }

        TransactionRequest transactionRequest = new TransactionRequest(System.currentTimeMillis() + "", 50000);
        ItemDetails itemDetails1 = new ItemDetails("1", 10000, 2, "Sandal");
        ItemDetails itemDetails2 = new ItemDetails("2", 30000, 1, "Gelas");

        // Create array list and add above item details in it and then set it to transaction request.
        ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();
        itemDetailsList.add(itemDetails1);
        itemDetailsList.add(itemDetails2);

        transactionRequest.setItemDetails(itemDetailsList);
        BillInfoModel billInfoModel = new BillInfoModel("demo_label", "demo_value");
        transactionRequest.setBillInfoModel(billInfoModel);

        MidtransSDK.getInstance().setTransactionRequest(transactionRequest);*/
    }

    private void initListener() {
        bClientHistoryPaymentNewBelifvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*MidtransSDK.getInstance().startPaymentUiFlow(getActivity());
                getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);*/
            }
        });
    }
}
