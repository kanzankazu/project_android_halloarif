package id.halloarif.app.view.activity.Role.Motivator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.view.activity.App.AppInfoModel;
import id.halloarif.app.view.activity.App.BoxUnegUnegDetailActivity;
import id.halloarif.app.view.activity.App.BoxUnegUnegModel;
import id.halloarif.app.view.activity.App.CallDetailActivity;
import id.halloarif.app.view.activity.App.ChatDetailActivity;

public class HistoryProblemActivity extends AppCompatActivity {

    private RecyclerView rvMotivatorHistoryAppfvbi;
    private String type;
    private int roleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_problem);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        rvMotivatorHistoryAppfvbi = (RecyclerView) findViewById(R.id.rvMotivatorHistoryApp);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.PARAM)) {
            type = bundle.getStringExtra(ISeasonConfig.PARAM);
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
            roleId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
        }
    }

    private void initContent() {
        initRecycleHistory();
    }

    private void initListener() {

    }

    private void initRecycleHistory() {
        List<AppInfoModel> models = new ArrayList<>();
        models.add(new AppInfoModel(1, "Chat", "moveon", true, "12-11-1991", "Faisal", "percintaan", "marah", "diduain", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(2, "Call", "bangkit", true, "12-11-1991", "Bahri", "pertemanan", "sedih", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(2, "Call", "bangkit", true, "12-11-1991", "Bahri", "pertemanan", "sedih", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(3, "Chat", "moveon", true, "12-11-1991", "Kanzan", "keluarga", "bahagia", "as", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(5, "box", "", true, "12-11-1991", "Kazu", "studi & karir", "entahlah", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(4, "Chat", "moveon", true, "12-11-1991", "Kazu", "studi & karir", "entahlah", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(2, "Call", "bangkit", true, "12-11-1991", "Bahri", "pertemanan", "sedih", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(4, "Chat", "moveon", true, "12-11-1991", "Kazu", "studi & karir", "entahlah", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));
        models.add(new AppInfoModel(5, "box", "", true, "12-11-1991", "Kazu", "studi & karir", "entahlah", "dicuekin", "Mohon bantuannya saya sedang mencoba membuat aplikasi ini dengan baik, tolong review ya"));

        List<AppInfoModel> newModels = new ArrayList<>();
        for (int i = 0; i < models.size(); i++) {
            AppInfoModel model = models.get(i);
            if (model.getTipe_paket().equalsIgnoreCase(type)) {
                newModels.add(model);
            }
        }

        MotivatorHomeFragmentMainAdapter adapter = new MotivatorHomeFragmentMainAdapter(HistoryProblemActivity.this, newModels,
                new MotivatorHomeFragmentMainAdapter.MotivatorHomeFragmentMainAdapterListener() {
                    @Override
                    public void click(AppInfoModel model) {
                        Toast.makeText(HistoryProblemActivity.this, model.toString(), Toast.LENGTH_SHORT).show();
                        BoxUnegUnegModel unegModel = new BoxUnegUnegModel();
                        unegModel.setId(model.getId());
                        unegModel.setKategori(model.getKategori() + ",=," + model.getPermasalahan() + ",=," + model.getPerasaan());
                        unegModel.setIdClient(1);
                        unegModel.setNamaClient(model.getUsername());
                        unegModel.setNamaAliasClient("CLIENT01");
                        unegModel.setIsiClient(model.getIsi());
                        unegModel.setDateTimeCreate("12/12/2012 12:12");
                        //unegModel.setIdMotivator(2);
                        //unegModel.setNamaMotivator();
                        //unegModel.setNamaAliasMotivator();
                        //unegModel.setIsiMotivator();
                        //unegModel.setDateTimeResponse();

                        if (model.getTipe_paket().equalsIgnoreCase("Box")) {
                            Intent intent = new Intent(HistoryProblemActivity.this, BoxUnegUnegDetailActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, unegModel);
                            startActivityForResult(intent, ISeasonConfig.REQ_CODE_BOX);
                        } else if (model.getTipe_paket().equalsIgnoreCase("Chat")) {
                            Intent intent = new Intent(HistoryProblemActivity.this, ChatDetailActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, model);
                            startActivity(intent);
                        } else if (model.getTipe_paket().equalsIgnoreCase("Call")) {
                            Intent intent = new Intent(HistoryProblemActivity.this, CallDetailActivity.class);
                            intent.putExtra(ISeasonConfig.PARAM, model);
                            startActivity(intent);
                        }
                    }
                });
        rvMotivatorHistoryAppfvbi.setAdapter(adapter);
        rvMotivatorHistoryAppfvbi.setLayoutManager(new LinearLayoutManager(HistoryProblemActivity.this));
    }
}
