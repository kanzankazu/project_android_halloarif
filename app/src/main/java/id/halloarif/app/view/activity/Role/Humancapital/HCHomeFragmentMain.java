package id.halloarif.app.view.activity.Role.Humancapital;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import id.halloarif.app.R;

public class HCHomeFragmentMain extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private LinearLayout llHCHomeInfoMotivatorfvbi;

    public HCHomeFragmentMain() {
        // Required empty public constructor
    }

    public static HCHomeFragmentMain newInstance(String param1, String param2) {
        HCHomeFragmentMain fragment = new HCHomeFragmentMain();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        HCHomeFragmentMain fragment = new HCHomeFragmentMain();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hc_home, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        llHCHomeInfoMotivatorfvbi = (LinearLayout) view.findViewById(R.id.llHCHomeInfoMotivator);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {

    }

    private void initListener() {
        llHCHomeInfoMotivatorfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}
