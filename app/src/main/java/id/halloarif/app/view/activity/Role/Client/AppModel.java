package id.halloarif.app.view.activity.Role.Client;

public class AppModel {
    private int id;
    private String appName;
    private int appImage;
    private String appImageUrl;

    public AppModel() {
    }

    public AppModel(int id, String appName, int appImage) {
        this.id = id;
        this.appName = appName;
        this.appImage = appImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getAppImage() {
        return appImage;
    }

    public void setAppImage(int appImage) {
        this.appImage = appImage;
    }

    public String getAppImageUrl() {
        return appImageUrl;
    }

    public void setAppImageUrl(String appImageUrl) {
        this.appImageUrl = appImageUrl;
    }

    @Override
    public String toString() {
        return "AppModel{" +
                "id=" + id +
                ", appName='" + appName + '\'' +
                ", appImage=" + appImage +
                ", appImageUrl='" + appImageUrl + '\'' +
                '}';
    }
}
