package id.halloarif.app.view.activity.Role.Client;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.R;
import id.halloarif.app.util.support.Recycleritemclicklistener;

public class HistoryPaymentDetail extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private RecyclerView rvClientHistoryPaymentDetailMainfvbi;
    private HistoryPaymentDetailAdapter adapter;
    private ArrayList<HistoryPaymentDetailModel> models;

    public HistoryPaymentDetail() {
        // Required empty public constructor
    }

    public static HistoryPaymentDetail newInstance(String param1, String param2) {
        HistoryPaymentDetail fragment = new HistoryPaymentDetail();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        HistoryPaymentDetail fragment = new HistoryPaymentDetail();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_history_payment_detail, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        rvClientHistoryPaymentDetailMainfvbi = (RecyclerView) view.findViewById(R.id.rvClientHistoryPaymentDetailMain);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        initRecycleviewHistoryPaymentDetail();
    }

    private void initListener() {
        rvClientHistoryPaymentDetailMainfvbi.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                HistoryPaymentDetailModel model = models.get(position);
                Toast.makeText(getActivity(), model.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void initRecycleviewHistoryPaymentDetail() {
        models = new ArrayList<HistoryPaymentDetailModel>();
        models.add(new HistoryPaymentDetailModel(1, 1, "Chat Motivasi", "Move On", "17 / 05 / 2018 00:00", 100000, "approve", "Transfer"));
        models.add(new HistoryPaymentDetailModel(2, 12, "Motiva Kerja", "Berhasil", "20 / 02 / 2019 11:16", 400000, "pending", "gopay"));
        models.add(new HistoryPaymentDetailModel(3, 123, "Chat Motivasi", "Tumbuh", "25 / 02 / 2019 11:25", 75000, "approve", ""));
        models.add(new HistoryPaymentDetailModel(4, 1234, "Chat Motivasi", "Tumbuh", "25 / 02 / 2019 11:26", 75000, "reject", ""));
        models.add(new HistoryPaymentDetailModel(5, 12345, "Call Motivasi", "Move On", "26 / 02 / 2019 14:12", 45000, "approve", "bank_transfer"));
        models.add(new HistoryPaymentDetailModel(5, 123456, "Chat Motivasi", "Move On", "26 / 02 / 2019 14:12", 450000, "reject", "bank_transfer"));

        adapter = new HistoryPaymentDetailAdapter(getActivity(), models);
        rvClientHistoryPaymentDetailMainfvbi.setAdapter(adapter);
        rvClientHistoryPaymentDetailMainfvbi.setLayoutManager(new LinearLayoutManager(getActivity()));
    }
}
