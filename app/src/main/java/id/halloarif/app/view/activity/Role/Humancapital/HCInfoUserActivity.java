package id.halloarif.app.view.activity.Role.Humancapital;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import id.halloarif.app.R;

public class HCInfoUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hc_info_user);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {

    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {

    }

    private void initListener() {

    }
}
