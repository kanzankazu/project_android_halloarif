package id.halloarif.app.view.activity.Role.Client;

public class HistoryPaymentDetailModel {
    private int id;
    private int invoice;
    private String layanan;
    private String paket;
    private String tanggal;
    private int harga;
    private String status;
    private String methode;

    private String username;
    private String name;
    private int diskon;
    private int kodeunik;

    public HistoryPaymentDetailModel() {
    }

    public HistoryPaymentDetailModel(int id, String layanan, String paket, String tanggal, int harga, String status, String methode, String username, String name, int diskon, int kodeunik) {
        this.id = id;
        this.layanan = layanan;
        this.paket = paket;
        this.tanggal = tanggal;
        this.harga = harga;
        this.status = status;
        this.methode = methode;
        this.username = username;
        this.name = name;
        this.diskon = diskon;
        this.kodeunik = kodeunik;
    }

    public HistoryPaymentDetailModel(int id,int invoice, String layanan, String paket, String tanggal, int harga, String status, String methode) {
        this.id = id;
        this.invoice = invoice;
        this.layanan = layanan;
        this.paket = paket;
        this.tanggal = tanggal;
        this.harga = harga;
        this.status = status;
        this.methode = methode;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public String getPaket() {
        return paket;
    }

    public void setPaket(String paket) {
        this.paket = paket;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMethode() {
        return methode;
    }

    public void setMethode(String methode) {
        this.methode = methode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDiskon() {
        return diskon;
    }

    public void setDiskon(int diskon) {
        this.diskon = diskon;
    }

    public int getKodeunik() {
        return kodeunik;
    }

    public void setKodeunik(int kodeunik) {
        this.kodeunik = kodeunik;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInvoice() {
        return invoice;
    }

    public void setInvoice(int invoice) {
        this.invoice = invoice;
    }

    @Override
    public String toString() {
        return "HistoryPaymentDetailModel{" +
                "id=" + id +
                ", invoice=" + invoice +
                ", layanan='" + layanan + '\'' +
                ", paket='" + paket + '\'' +
                ", tanggal='" + tanggal + '\'' +
                ", harga=" + harga +
                ", status='" + status + '\'' +
                ", methode='" + methode + '\'' +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", diskon=" + diskon +
                ", kodeunik=" + kodeunik +
                '}';
    }
}
