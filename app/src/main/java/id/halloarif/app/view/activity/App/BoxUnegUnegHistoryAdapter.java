package id.halloarif.app.view.activity.App;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.R;

public class BoxUnegUnegHistoryAdapter extends RecyclerView.Adapter<BoxUnegUnegHistoryAdapter.ViewHolder> {
    private List<BoxUnegUnegModel> modelsMain;
    private List<BoxUnegUnegModel> modelsTemp;
    private Listener listener;
    private BoxUnegUnegHistoryAdapter.SubjectDataFilter subjectDataFilter;
    private Activity activity;

    public BoxUnegUnegHistoryAdapter(Activity activity, ArrayList<BoxUnegUnegModel> models, Listener listener) {
        this.activity = activity;
        this.modelsMain = models;
        this.modelsTemp = models;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BoxUnegUnegHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_box_uneg_uneg_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BoxUnegUnegHistoryAdapter.ViewHolder holder, int position) {
        BoxUnegUnegModel model = modelsTemp.get(position);

        holder.tvListBoxHistoryDateTimefvbi.setText(model.getDateTimeCreate());
        holder.tvListBoxHistoryCategoryfvbi.setText(model.getKategori());
        holder.tvListBoxHistoryStatusfvbi.setText(model.getStatus());
        holder.tvListBoxHistoryIsifvbi.setText(model.getIsiClient());

        holder.cvListBoxHistoryfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.click(model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelsTemp.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvListBoxHistoryDateTimefvbi, tvListBoxHistoryCategoryfvbi, tvListBoxHistoryStatusfvbi, tvListBoxHistoryIsifvbi;
        private final CardView cvListBoxHistoryfvbi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cvListBoxHistoryfvbi = (CardView) itemView.findViewById(R.id.cvListBoxHistory);
            tvListBoxHistoryDateTimefvbi = (TextView) itemView.findViewById(R.id.tvListBoxHistoryDateTime);
            tvListBoxHistoryCategoryfvbi = (TextView) itemView.findViewById(R.id.tvListBoxHistoryCategory);
            tvListBoxHistoryStatusfvbi = (TextView) itemView.findViewById(R.id.tvListBoxHistoryStatus);
            tvListBoxHistoryIsifvbi = (TextView) itemView.findViewById(R.id.tvListBoxHistoryIsi);

        }
    }

    public Filter getFilter() {
        if (subjectDataFilter == null) {
            subjectDataFilter = new BoxUnegUnegHistoryAdapter.SubjectDataFilter();
        }
        return subjectDataFilter;
    }

    private class SubjectDataFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            charSequence = charSequence.toString().toLowerCase();
            FilterResults filterResults = new FilterResults();

            if (!TextUtils.isEmpty(charSequence)) {
                ArrayList<BoxUnegUnegModel> arrayList1 = new ArrayList<BoxUnegUnegModel>();

                for (int i = 0, l = modelsMain.size(); i < l; i++) {
                    BoxUnegUnegModel subject = modelsMain.get(i);
                    if (subject.toString().toLowerCase().contains(charSequence)) {
                        arrayList1.add(subject);
                    }
                }

                filterResults.count = arrayList1.size();
                filterResults.values = arrayList1;
            } else {
                synchronized (this) {
                    filterResults.count = modelsMain.size();
                    filterResults.values = modelsMain;
                }
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            modelsTemp = (ArrayList<BoxUnegUnegModel>) filterResults.values;

            notifyDataSetChanged();

            /*modelsTemp.clear();

            for (int i = 0, l = modelsTemp.size(); i < l; i++) {
                modelsTemp.add(modelsTemp.get(i));
            }
            notifyDataSetChanged();*/
        }
    }

    public interface Listener {
        void click(BoxUnegUnegModel model);
    }
}
