package id.halloarif.app.view.activity.App;

class AppModel {
    int id;
    private String value;
    private String title;

    AppModel(int id, String title, String value) {
        this.id = id;
        this.title = title;
        this.value = value;
    }

    AppModel(String title, String value){
        this.title = title;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String name) {
        this.value = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String alias_name) {
        this.title = alias_name;
    }
}
