package id.halloarif.app.view.activity.App;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SystemUtil;

public class BoxUnegUnegHistoryActivity extends AppCompatActivity {

    private RecyclerView rvBoxHistoryfvbi;
    private BoxUnegUnegHistoryAdapter rvBoxHistoryadapter;
    private ArrayList<BoxUnegUnegModel> models = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_uneg_uneg_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        rvBoxHistoryfvbi = (RecyclerView) findViewById(R.id.rvBoxHistory);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        initRecycleBoxHistory();
    }

    private void initListener() {
    }

    private void initRecycleBoxHistory() {
        models.add(new BoxUnegUnegModel(1, "18/03/2019 09:10", "Keluarga", 0, "saya enggak di ijinin masuk oleh istri bagaimana pemecahan masalahnya ?", ""));
        models.add(new BoxUnegUnegModel(2, "17/03/2019 06:20", "Percintaan", 1, "sampai sekarang belum punya pacar", "kanzankazu"));
        models.add(new BoxUnegUnegModel(3, "17/03/2019 06:13", "Keluarga", 1, "Sy py tetangga yg tdk kooperatif. Namun setiap ada perlu selalu datang meminjam uang. Bagaimana mengatasinuya", "kanzankazu"));
        models.add(new BoxUnegUnegModel(4, "15/03/2019 10:22", "Percintaan", 0, "Saya cemburu dengan pasangan saya karena pasangan saya lebih dekat dan mudah ketawa dengan teman saya dibandingkan dengan saya.. apa yang saya harus lakukan??", ""));
        models.add(new BoxUnegUnegModel(5, "15/03/2019 10:06", "Ekonomi", 1, "Ekonomi saya sekrang sedang berada jauh di bawah sedangkan saya sebentar lagi akan menikah", ""));

        rvBoxHistoryadapter = new BoxUnegUnegHistoryAdapter(BoxUnegUnegHistoryActivity.this, models,
                new BoxUnegUnegHistoryAdapter.Listener() {
                    @Override
                    public void click(BoxUnegUnegModel model) {
                        Intent intent = new Intent(BoxUnegUnegHistoryActivity.this, BoxUnegUnegDetailActivity.class);
                        intent.putExtra(ISeasonConfig.PARAM, model);
                        startActivity(intent);

                    }
                });
        rvBoxHistoryfvbi.setAdapter(rvBoxHistoryadapter);
        rvBoxHistoryfvbi.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setQueryHint("Cari History...");
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);// Do not iconify the widget; expand it by default
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                rvBoxHistoryadapter.getFilter().filter(newText);
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                SystemUtil.hideKeyBoard(BoxUnegUnegHistoryActivity.this);
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchViewItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                SystemUtil.showKeyBoard(BoxUnegUnegHistoryActivity.this);
                searchView.requestFocus();
                Toast.makeText(BoxUnegUnegHistoryActivity.this, "onMenuItemActionExpand called", Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                SystemUtil.showKeyBoard(BoxUnegUnegHistoryActivity.this);
                Toast.makeText(BoxUnegUnegHistoryActivity.this, "onMenutItemActionCollapse called", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
