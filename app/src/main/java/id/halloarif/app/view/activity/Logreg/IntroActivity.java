package id.halloarif.app.view.activity.Logreg;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;

public class IntroActivity extends AppCompatActivity {

    private ViewPager vpIntroMainfvbi;
    private Button bIntroSkipfvbi, bIntroNextfvbi;
    private LinearLayout llIntroDotfvbi;
    private TextView[] dots;
    private int[] layouts;
    private MyViewPagerAdapter myViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        vpIntroMainfvbi = (ViewPager) findViewById(R.id.vpIntroMain);
        bIntroSkipfvbi = (Button) findViewById(R.id.bIntroSkip);
        llIntroDotfvbi = (LinearLayout) findViewById(R.id.llIntroDot);
        bIntroNextfvbi = (Button) findViewById(R.id.bIntroNext);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        layouts = new int[]{
                /*R.layout.layout_intro_slide1,
                R.layout.layout_intro_slide2,
                R.layout.layout_intro_slide3*/
        };

        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        vpIntroMainfvbi.setAdapter(myViewPagerAdapter);
        vpIntroMainfvbi.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);

                // changing the next button text 'NEXT' / 'GOT IT'
                if (position == layouts.length - 1) {
                    // last page. make button text to GOT IT
                    bIntroNextfvbi.setText("Start");
                    bIntroSkipfvbi.setVisibility(View.INVISIBLE);
                } else {
                    // still pages are left
                    bIntroNextfvbi.setText("Next");
                    bIntroSkipfvbi.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    private void initListener() {
        bIntroSkipfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionUtil.setBoolPreferences(ISeasonConfig.HAS_INTRO, true);
                moveToLogin();
            }
        });
        bIntroNextfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    vpIntroMainfvbi.setCurrentItem(current);
                } else {
                    SessionUtil.setBoolPreferences(ISeasonConfig.HAS_INTRO, true);
                    moveToLogin();
                }
            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        //int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        //int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        llIntroDotfvbi.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            //dots[i].setTextColor(colorsInactive[currentPage]);
            dots[i].setTextColor(getResources().getColor(R.color.grey));
            llIntroDotfvbi.addView(dots[i]);
        }

        if (dots.length > 0)
            //dots[currentPage].setTextColor(colorsActive[currentPage]);
            dots[currentPage].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    private int getItem(int i) {
        return vpIntroMainfvbi.getCurrentItem() + i;
    }

    private void moveToLogin() {
        Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
