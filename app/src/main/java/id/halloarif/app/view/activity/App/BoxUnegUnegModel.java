package id.halloarif.app.view.activity.App;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class BoxUnegUnegModel implements Parcelable {
    private int id;
    private String kategori;
    private String status;
    private int idClient;
    private String namaClient;
    private String namaAliasClient;
    private String isiClient;
    private String dateTimeCreate;
    private int idMotivator;
    private String namaMotivator;
    private String namaAliasMotivator;
    private String isiMotivator;
    private String dateTimeResponse;

    /**
     * @param id
     * @param kategori
     * @param status
     * @param idClient
     * @param namaClient
     * @param namaAliasClient
     * @param dateTimeCreate
     *
     * @apiNote untuk client blm menulis
     */
    public BoxUnegUnegModel(int id, String kategori, String status, int idClient, String namaClient, String namaAliasClient, String dateTimeCreate) {
        this.id = id;
        this.kategori = kategori;
        this.status = status;
        this.idClient = idClient;
        this.namaClient = namaClient;
        this.namaAliasClient = namaAliasClient;
        this.dateTimeCreate = dateTimeCreate;
    }

    /**
     * @param id
     * @param kategori
     * @param status
     * @param idClient
     * @param namaClient
     * @param namaAliasClient
     * @param isiClient
     * @param dateTimeCreate
     *
     * @apiNote untuk client sudah menulis
     */
    public BoxUnegUnegModel(int id, String kategori, String status, int idClient, String namaClient, String namaAliasClient, String isiClient, String dateTimeCreate) {
        this.id = id;
        this.kategori = kategori;
        this.status = status;
        this.idClient = idClient;
        this.namaClient = namaClient;
        this.namaAliasClient = namaAliasClient;
        this.isiClient = isiClient;
        this.dateTimeCreate = dateTimeCreate;
    }

    /**
     * @param id
     * @param kategori
     * @param status
     * @param idClient
     * @param namaClient
     * @param namaAliasClient
     * @param isiClient
     * @param dateTimeCreate
     * @param idMotivator
     * @param namaMotivator
     * @param namaAliasMotivator
     * @param isiMotivator
     * @param dateTimeResponse
     *
     * @apiNote client & motivator sudah menulis
     */
    public BoxUnegUnegModel(int id, String kategori, String status, int idClient, String namaClient, String namaAliasClient, String isiClient, String dateTimeCreate, int idMotivator, String namaMotivator, String namaAliasMotivator, String isiMotivator, String dateTimeResponse) {
        this.id = id;
        this.kategori = kategori;
        this.status = status;
        this.idClient = idClient;
        this.namaClient = namaClient;
        this.namaAliasClient = namaAliasClient;
        this.isiClient = isiClient;
        this.dateTimeCreate = dateTimeCreate;
        this.idMotivator = idMotivator;
        this.namaMotivator = namaMotivator;
        this.namaAliasMotivator = namaAliasMotivator;
        this.isiMotivator = isiMotivator;
        this.dateTimeResponse = dateTimeResponse;
    }

    public BoxUnegUnegModel(int id, String dateTimeCreate, String kategori, int idMotivator, String isiClient, String isiMotivator) {

        this.id = id;
        this.dateTimeCreate = dateTimeCreate;
        this.kategori = kategori;
        this.idMotivator = idMotivator;
        this.isiClient = isiClient;
        this.isiMotivator = isiMotivator;
    }

    public BoxUnegUnegModel() {

    }

    protected BoxUnegUnegModel(Parcel in) {
        id = in.readInt();
        kategori = in.readString();
        status = in.readString();
        idClient = in.readInt();
        namaClient = in.readString();
        namaAliasClient = in.readString();
        isiClient = in.readString();
        dateTimeCreate = in.readString();
        idMotivator = in.readInt();
        namaMotivator = in.readString();
        namaAliasMotivator = in.readString();
        isiMotivator = in.readString();
        dateTimeResponse = in.readString();
    }

    public static final Creator<BoxUnegUnegModel> CREATOR = new Creator<BoxUnegUnegModel>() {
        @Override
        public BoxUnegUnegModel createFromParcel(Parcel in) {
            return new BoxUnegUnegModel(in);
        }

        @Override
        public BoxUnegUnegModel[] newArray(int size) {
            return new BoxUnegUnegModel[size];
        }
    };

    @Override
    public String toString() {
        return "BoxUnegUnegModel{" +
                "id=" + id +
                ", dateTimeCreate='" + dateTimeCreate + '\'' +
                ", kategori='" + kategori + '\'' +
                ", status='" + status + '\'' +
                ", isiClient='" + isiClient + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(kategori);
        parcel.writeString(status);
        parcel.writeInt(idClient);
        parcel.writeString(namaClient);
        parcel.writeString(namaAliasClient);
        parcel.writeString(isiClient);
        parcel.writeString(dateTimeCreate);
        parcel.writeInt(idMotivator);
        parcel.writeString(namaMotivator);
        parcel.writeString(namaAliasMotivator);
        parcel.writeString(isiMotivator);
        parcel.writeString(dateTimeResponse);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getStatus() {
        if (getIdMotivator() == 0) {
            return "Mencari Motivator";
        } else {
            if (TextUtils.isEmpty(getIsiMotivator())) {
                return "Menunggu Respon Motivator";
            } else {
                return "Telah Di Respon Motivator";
            }
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNamaClient() {
        return namaClient;
    }

    public void setNamaClient(String namaClient) {
        this.namaClient = namaClient;
    }

    public String getNamaAliasClient() {
        return namaAliasClient;
    }

    public void setNamaAliasClient(String namaAliasClient) {
        this.namaAliasClient = namaAliasClient;
    }

    public String getIsiClient() {
        return isiClient;
    }

    public void setIsiClient(String isiClient) {
        this.isiClient = isiClient;
    }

    public String getDateTimeCreate() {
        return dateTimeCreate;
    }

    public void setDateTimeCreate(String dateTimeCreate) {
        this.dateTimeCreate = dateTimeCreate;
    }

    public int getIdMotivator() {
        return idMotivator;
    }

    public void setIdMotivator(int idMotivator) {
        this.idMotivator = idMotivator;
    }

    public String getNamaMotivator() {
        return namaMotivator;
    }

    public void setNamaMotivator(String namaMotivator) {
        this.namaMotivator = namaMotivator;
    }

    public String getNamaAliasMotivator() {
        return namaAliasMotivator;
    }

    public void setNamaAliasMotivator(String namaAliasMotivator) {
        this.namaAliasMotivator = namaAliasMotivator;
    }

    public String getIsiMotivator() {
        return isiMotivator;
    }

    public void setIsiMotivator(String isiMotivator) {
        this.isiMotivator = isiMotivator;
    }

    public String getDateTimeResponse() {
        return dateTimeResponse;
    }

    public void setDateTimeResponse(String dateTimeResponse) {
        this.dateTimeResponse = dateTimeResponse;
    }

}
