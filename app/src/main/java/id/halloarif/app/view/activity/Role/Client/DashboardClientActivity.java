package id.halloarif.app.view.activity.Role.Client;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.view.activity.Main.NotifFragment;
import id.halloarif.app.view.activity.Main.SettingsFragment;

public class DashboardClientActivity extends AppCompatActivity {

    private FrameLayout flDashboardClientfvbi;
    private BottomNavigationView bnvDashboardClientfvbi;

    private Boolean loggedin;
    private int userId;
    private int roleId;

    private int tabPagerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_client);

        initComponent();
        initParam();
        initSession();
        initTab();
        initContent();
        initListener();
    }

    private void initComponent() {
        flDashboardClientfvbi = (FrameLayout) findViewById(R.id.flDashboardClient);
        bnvDashboardClientfvbi = (BottomNavigationView) findViewById(R.id.bnvDashboardClient);

    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.KEY_POSITION_TABPAGER)) {
            tabPagerId = bundle.getIntExtra(ISeasonConfig.KEY_POSITION_TABPAGER, 0);
        } else {
            tabPagerId = -1;
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.HAS_LOGIN)) {
            loggedin = SessionUtil.getBoolPreferences(ISeasonConfig.HAS_LOGIN, false);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_USER_ID, 0);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
            roleId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
        }
    }

    private void initTab() {
        Menu menu = bnvDashboardClientfvbi.getMenu();
    }

    private void initContent() {
        if (tabPagerId != -1) {
            //bnvDashboardClientfvbi.getMenu().getItem(tabPagerId).setChecked(true);
            //((BottomNavigationView) bnvDashboardClientfvbi.getChildAt(0)).getChildAt(tabPagerId).performClick();
        }

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flDashboardClient, ClientHomeFragmentMain.newInstance());
        transaction.commit();
    }

    private void initListener() {
        bnvDashboardClientfvbi.setOnNavigationItemSelectedListener(menuItem -> {
            Fragment selectedFragment = null;
            switch (menuItem.getItemId()) {
                case R.id.action_item1:
                    selectedFragment = ClientHomeFragmentMain.newInstance();
                    break;
                case R.id.action_item2:
                    selectedFragment = NotifFragment.newInstance();
                    break;
                case R.id.action_item3:
                    selectedFragment = HistoryPaymentFragmentMain.newInstance();
                    break;
                case R.id.action_item4:
                    selectedFragment = SettingsFragment.newInstance();
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.flDashboardClient, selectedFragment);
            transaction.commit();
            return true;
        });
    }
}
