package id.halloarif.app.view.activity.Main;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import id.halloarif.app.R;

public class NotifAdapter extends RecyclerView.Adapter<NotifAdapter.ViewHolder> {
    private final Activity activity;
    private final ArrayList<NotifModel> models;

    public NotifAdapter(Activity activity, ArrayList<NotifModel> models) {

        this.activity = activity;
        this.models = models;
    }

    @NonNull
    @Override
    public NotifAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notif, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotifAdapter.ViewHolder holder, int i) {
        NotifModel model = models.get(i);
        if (TextUtils.isEmpty(model.getImage())) {
            holder.civListItemNotifIconfvbi.setImageResource(R.drawable.gallery_photo);
        } else {
            holder.civListItemNotifIconfvbi.setImageResource(R.drawable.gallery_photo);
        }

        holder.tvListItemNotifTitlefvbi.setText(model.  getDetail());
        holder.tvListItemNotifDateTimefvbi.setText(model.getDateTime());

        if (model.isRead()) {
            holder.ivListItemNotifDotfvbi.setVisibility(View.GONE);
        } else {
            holder.ivListItemNotifDotfvbi.setColorFilter(activity.getResources().getColor(R.color.androidBluelight));
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final CircleImageView civListItemNotifIconfvbi;
        private final TextView tvListItemNotifTitlefvbi;
        private final TextView tvListItemNotifDateTimefvbi;
        private final ImageView ivListItemNotifDotfvbi;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            civListItemNotifIconfvbi = (CircleImageView) itemView.findViewById(R.id.civListItemNotifIcon);
            tvListItemNotifTitlefvbi = (TextView) itemView.findViewById(R.id.tvListItemNotifTitle);
            tvListItemNotifDateTimefvbi = (TextView) itemView.findViewById(R.id.tvListItemNotifDateTime);
            ivListItemNotifDotfvbi = (ImageView) itemView.findViewById(R.id.ivListItemNotifDot);

        }
    }
}
