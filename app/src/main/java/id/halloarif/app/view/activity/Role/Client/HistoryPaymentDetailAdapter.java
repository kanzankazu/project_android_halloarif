package id.halloarif.app.view.activity.Role.Client;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.halloarif.app.R;

public class HistoryPaymentDetailAdapter extends RecyclerView.Adapter<HistoryPaymentDetailAdapter.ViewHolder> {
    private final Activity activity;
    private final List<HistoryPaymentDetailModel> models;

    public HistoryPaymentDetailAdapter(Activity activity, List<HistoryPaymentDetailModel> models) {
        this.activity = activity;
        this.models = models;
    }

    @NonNull
    @Override
    public HistoryPaymentDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_client_history_payment_detail, parent, false);
        return new HistoryPaymentDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryPaymentDetailAdapter.ViewHolder holder, int position) {
        HistoryPaymentDetailModel model = models.get(position);

        if (model.getLayanan().equalsIgnoreCase("box uneg uneg")) {
            holder.ivClientHistoryPaymentDetailListfvbi.setImageResource(R.drawable.ic_box_uneg);
        } else if (model.getLayanan().equalsIgnoreCase("Chat Motivasi")) {
            holder.ivClientHistoryPaymentDetailListfvbi.setImageResource(R.drawable.ic_chat);
        } else if (model.getLayanan().equalsIgnoreCase("Call Motivasi")) {
            holder.ivClientHistoryPaymentDetailListfvbi.setImageResource(R.drawable.ic_call);
        }

        holder.tvClientHistoryPaymentDetailListLayananPaketfvbi.setText(model.getLayanan() + " - " + model.getPaket());
        holder.tvClientHistoryPaymentDetailListHargafvbi.setText("Rp. " + model.getHarga());
        holder.tvClientHistoryPaymentDetailListInvoicefvbi.setText("# " + model.getInvoice());

        holder.tvClientHistoryPaymentDetailListStatusfvbi.setText(model.getStatus());
        if (model.getStatus().equalsIgnoreCase("approve")) {
            holder.tvClientHistoryPaymentDetailListStatusfvbi.setBackgroundColor(activity.getResources().getColor(R.color.androidSgreen));
        } else if (model.getStatus().equalsIgnoreCase("pending")) {
            holder.tvClientHistoryPaymentDetailListStatusfvbi.setBackgroundColor(activity.getResources().getColor(R.color.graylight));
        } else if (model.getStatus().equalsIgnoreCase("reject")) {
            holder.tvClientHistoryPaymentDetailListStatusfvbi.setBackgroundColor(activity.getResources().getColor(R.color.androidRed));
        } else {
            holder.tvClientHistoryPaymentDetailListStatusfvbi.setBackgroundColor(activity.getResources().getColor(R.color.yellowlight));
        }

        holder.tvClientHistoryPaymentDetailListTanggalfvbi.setText(model.getTanggal());

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView ivClientHistoryPaymentDetailListfvbi;
        private final TextView tvClientHistoryPaymentDetailListLayananPaketfvbi;
        private final TextView tvClientHistoryPaymentDetailListInvoicefvbi;
        private final TextView tvClientHistoryPaymentDetailListStatusfvbi;
        private final TextView tvClientHistoryPaymentDetailListTanggalfvbi;
        private final TextView tvClientHistoryPaymentDetailListHargafvbi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivClientHistoryPaymentDetailListfvbi = (ImageView) itemView.findViewById(R.id.ivClientHistoryPaymentDetailList);
            tvClientHistoryPaymentDetailListLayananPaketfvbi = (TextView) itemView.findViewById(R.id.tvClientHistoryPaymentDetailListLayananPaket);
            tvClientHistoryPaymentDetailListInvoicefvbi = (TextView) itemView.findViewById(R.id.tvClientHistoryPaymentDetailListInvoice);
            tvClientHistoryPaymentDetailListHargafvbi = (TextView) itemView.findViewById(R.id.tvClientHistoryPaymentDetailListHarga);
            tvClientHistoryPaymentDetailListStatusfvbi = (TextView) itemView.findViewById(R.id.tvClientHistoryPaymentDetailListStatus);
            tvClientHistoryPaymentDetailListTanggalfvbi = (TextView) itemView.findViewById(R.id.tvClientHistoryPaymentDetailListTanggal);

        }
    }
}
