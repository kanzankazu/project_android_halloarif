package id.halloarif.app.view.activity.Role.Client;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import id.halloarif.app.R;

class ClientHomeAppAdapter extends RecyclerView.Adapter<ClientHomeAppAdapter.ViewHolder> {
    private final Activity activity;
    private List<AppModel> models;

    public ClientHomeAppAdapter(Activity activity, List<AppModel> models) {
        this.activity = activity;
        this.models = models;
    }

    @NonNull
    @Override
    public ClientHomeAppAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_client_home_app, parent, false);
        return new ClientHomeAppAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientHomeAppAdapter.ViewHolder viewHolder, int position) {
        AppModel model = models.get(position);

        viewHolder.ivListItemClientHomeAppIconfvbi.setImageResource(model.getAppImage());

        /*if (model.getAppName().equalsIgnoreCase("Box uneg uneg")){
        }else if (model.getAppName().equalsIgnoreCase("Chat motivator")){
        }else if (model.getAppName().equalsIgnoreCase("Call Motivator")){
        }*/
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CardView cvListItemClientHomeAppIconfvbi;
        private final ImageView ivListItemClientHomeAppIconfvbi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cvListItemClientHomeAppIconfvbi = (CardView) itemView.findViewById(R.id.cvListItemClientHomeAppIcon);
            ivListItemClientHomeAppIconfvbi = (ImageView) itemView.findViewById(R.id.ivListItemClientHomeAppIcon);

        }
    }

    public void setData(List<AppModel> datas) {
        if (datas.size() > 0) {
            models.clear();
            models = datas;
        } else {
            models = datas;
        }
        notifyDataSetChanged();
    }

    public void replaceData(List<AppModel> datas) {
        models.clear();
        models.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<AppModel> datas) {
        models.addAll(datas);
        notifyItemRangeInserted(models.size(), datas.size());
    }

    public void addDataFirst(AppModel data) {
        int position = 0;
        models.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    public void removeDataFirst() {
        int position = 0;
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }
}
