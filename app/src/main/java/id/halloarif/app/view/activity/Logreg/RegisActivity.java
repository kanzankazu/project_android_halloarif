package id.halloarif.app.view.activity.Logreg;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.InputValidUtil;

public class RegisActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etRegNamefvbi, etRegEmailfvbi, etRegPhoneNofvbi, etRegPassword1fvbi, etRegPassword2fvbi, etRegCodeReferalfvbi;
    private ImageView ibRegPassword1fvbi, ibRegPassword2fvbi;
    private CardView bRegExecutefvbi;
    private TextView tvRegNowLoginfvbi;

    private boolean passwordShown1;
    private boolean passwordShown2;
    private String sTokenBug;
    private int userId = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        etRegNamefvbi = (EditText) findViewById(R.id.etRegName);
        etRegEmailfvbi = (EditText) findViewById(R.id.etRegEmail);
        etRegPhoneNofvbi = (EditText) findViewById(R.id.etRegPhoneNo);
        etRegPassword1fvbi = (EditText) findViewById(R.id.etRegPassword1);
        etRegPassword2fvbi = (EditText) findViewById(R.id.etRegPassword2);
        ibRegPassword1fvbi = (ImageView) findViewById(R.id.ibRegPassword1);
        ibRegPassword2fvbi = (ImageView) findViewById(R.id.ibRegPassword2);
        etRegCodeReferalfvbi = (EditText) findViewById(R.id.etRegCodeReferal);
        bRegExecutefvbi = (CardView) findViewById(R.id.bRegExecute);
        tvRegNowLoginfvbi = (TextView) findViewById(R.id.tvRegNowLogin);

    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        sTokenBug = FirebaseInstanceId.getInstance().getToken();
    }

    private void initListener() {
        etRegPassword2fvbi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_GO) {
                    regisValid();
                }
                return handled;
            }
        });

        ibRegPassword1fvbi.setOnClickListener(this);
        ibRegPassword2fvbi.setOnClickListener(this);
        bRegExecutefvbi.setOnClickListener(this);
        tvRegNowLoginfvbi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == ibRegPassword1fvbi) {
            if (passwordShown1) {//tidak terlihat
                passwordShown1 = false;
                etRegPassword1fvbi.setInputType(129);//edittest
                etRegPassword1fvbi.setTypeface(Typeface.SANS_SERIF);
                ibRegPassword1fvbi.setImageResource(R.drawable.ic_visibility_off_black_24dp);
            } else {//terlihat
                passwordShown1 = true;
                etRegPassword1fvbi.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                ibRegPassword1fvbi.setImageResource(R.drawable.ic_visibility_black_24dp);
            }
        } else if (view == ibRegPassword2fvbi) {
            if (passwordShown2) {//tidak terlihat
                passwordShown2 = false;
                etRegPassword2fvbi.setInputType(129);//edittest
                etRegPassword2fvbi.setTypeface(Typeface.SANS_SERIF);
                ibRegPassword2fvbi.setImageResource(R.drawable.ic_visibility_off_black_24dp);
            } else {//terlihat
                passwordShown2 = true;
                etRegPassword2fvbi.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                ibRegPassword2fvbi.setImageResource(R.drawable.ic_visibility_black_24dp);
            }
        } else if (view == bRegExecutefvbi) {
            regisValid();
        } else if (view == tvRegNowLoginfvbi) {
            onBackPressed();
        }
    }

    private void regisValid() {
        String sName = etRegNamefvbi.getText().toString().trim();
        String sEmail = etRegEmailfvbi.getText().toString().trim();
        String sPhoneNo = etRegPhoneNofvbi.getText().toString().trim();
        String sPassword1 = etRegPassword1fvbi.getText().toString().trim();
        String sPassword2 = etRegPassword2fvbi.getText().toString().trim();

        if (!InputValidUtil.isEmptyField("Kolom ini masih kosong", etRegNamefvbi, etRegEmailfvbi, etRegPhoneNofvbi, etRegPassword1fvbi, etRegPassword2fvbi)) {
            if (!InputValidUtil.isValidateEmail(sEmail)) {
                InputValidUtil.errorET(etRegEmailfvbi, "Email Format Salah, yyyyy@yyyyy.com");
            } else if (!InputValidUtil.isValidatePhoneNumber(sPhoneNo)) {
                InputValidUtil.errorET(etRegPhoneNofvbi, "No HP Format Salah, menggunakan 0/+62");
            } else if (!InputValidUtil.isMatch(sPassword1, sPassword2)) {
                Toast.makeText(getApplicationContext(), "Password tidak cocok", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "data aman", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(RegisActivity.this, RegisActivationActivity.class);
                intent.putExtra(ISeasonConfig.KEY_USER_ID, userId);
                startActivity(intent);
                finish();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Field masih ada yang kosong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
