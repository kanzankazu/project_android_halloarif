package id.halloarif.app.view;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.HttpAuthHandler;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import id.halloarif.app.IConfig;
import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;

public class MainActivityNew extends AppCompatActivity {

    private static final String url = IConfig.API_BASE_URL;
    private static final String TAG = "WebViewCookies";
    private static final int REQUEST_SELECT_FILE = 100;
    private static int FILECHOOSER_RESULTCODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 101;

    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    private MainActivity mBinding;
    private PermissionRequest myRequest;

    private ProgressBar pbMainActivityfvbi;
    private WebView wvMainActivityfvbi;
    private SwipeRefreshLayout srlMainActivityfvbi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        pbMainActivityfvbi =(ProgressBar) findViewById(R.id.pbMainActivity);
        srlMainActivityfvbi =(SwipeRefreshLayout) findViewById(R.id.srlMainActivity);
        wvMainActivityfvbi =(WebView) findViewById(R.id.wvMainActivity);

    }

    private void initParam() {

    }

    private void initSession() {

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initContent() {
        pbMainActivityfvbi.setVisibility(View.GONE);
        pbMainActivityfvbi.setMax(100);

        //WEBSETTING
        wvMainActivityfvbi.getSettings().setJavaScriptEnabled(true);
        wvMainActivityfvbi.getSettings().setAppCacheEnabled(true);
        wvMainActivityfvbi.getSettings().setLoadWithOverviewMode(true);
        wvMainActivityfvbi.getSettings().setSupportZoom(false);
        wvMainActivityfvbi.setWebViewClient(new KanzanWEbView());
        wvMainActivityfvbi.setWebChromeClient(new KanzanWEbChromeView());
        wvMainActivityfvbi.loadUrl(url);

    }

    private void initListener() {
        wvMainActivityfvbi.getViewTreeObserver().addOnScrollChangedListener(() -> {
            if (wvMainActivityfvbi.getScrollY() == 0) {
                srlMainActivityfvbi.setEnabled(true);
            } else {
                srlMainActivityfvbi.setEnabled(false);
            }
        });
        srlMainActivityfvbi.setOnRefreshListener(() -> {
            srlMainActivityfvbi.setRefreshing(false);
            wvMainActivityfvbi.reload();
        });
    }

    private class KanzanWEbChromeView extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivityNew.this);
            builder.setMessage(message)
                    .setNeutralButton("OK", (arg0, arg1) -> result.confirm())
                    .setCancelable(true)
                    .show();
            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivityNew.this);
            builder.setMessage(message)
                    .setPositiveButton("Yes", (dialog, which) -> result.confirm())
                    .setNegativeButton("No", (dialog, which) -> result.cancel())
                    .setCancelable(true)
                    .show();
            return true;
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            pbMainActivityfvbi.setProgress(newProgress);
        }

    }

    private class KanzanWEbView extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            wvMainActivityfvbi.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            pbMainActivityfvbi.setVisibility(View.VISIBLE);
            pbMainActivityfvbi.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            srlMainActivityfvbi.setRefreshing(false);
            pbMainActivityfvbi.setVisibility(View.GONE);
            pbMainActivityfvbi.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }
    }
}
