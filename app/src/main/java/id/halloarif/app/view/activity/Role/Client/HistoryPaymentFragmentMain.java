package id.halloarif.app.view.activity.Role.Client;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import id.halloarif.app.R;

public class HistoryPaymentFragmentMain extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private String[] titleTab = new String[]{"Riwayat Pembelian", "Pembelian Baru"};
    private int[] iconTab = new int[]{R.drawable.ic_email, R.drawable.ic_add_photo};
    private ViewPager vpClientHistoryPaymentfvbi;
    private TabLayout tlHistoryPaymentMainfvbi;
    private SlideHistoryPaymentAdapter mPagerAdapter;
    private SearchView searchView;
    private SearchView.OnQueryTextListener queryTextListener;

    public HistoryPaymentFragmentMain() {
    }

    public static Fragment newInstance() {
        HistoryPaymentFragmentMain fragment = new HistoryPaymentFragmentMain();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_history_payment, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        initToolBar(view);
        vpClientHistoryPaymentfvbi = (ViewPager) view.findViewById(R.id.vpClientHistoryPayment);
    }

    private void initToolBar(View view) {
        tlHistoryPaymentMainfvbi = (TabLayout) view.findViewById(R.id.tlHistoryPaymentMain);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        initTablayoutViewpager();
    }

    private void initListener() {
        vpClientHistoryPaymentfvbi.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlHistoryPaymentMainfvbi));

        tlHistoryPaymentMainfvbi.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpClientHistoryPaymentfvbi.setCurrentItem(tab.getPosition());
                int tabIconColor = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(getActivity(), R.color.grey);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void initTablayoutViewpager() {
        int tabIconColorSelect = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
        int tabIconColorUnSelect = ContextCompat.getColor(getActivity(), R.color.grey);
        for (int i = 0; i < titleTab.length; i++) {
            tlHistoryPaymentMainfvbi.addTab(tlHistoryPaymentMainfvbi.newTab().setIcon(iconTab[i]));
            if (i == 0) {
                tlHistoryPaymentMainfvbi.getTabAt(i).getIcon().setColorFilter(tabIconColorSelect, PorterDuff.Mode.SRC_IN);
            } else {
                tlHistoryPaymentMainfvbi.getTabAt(i).getIcon().setColorFilter(tabIconColorUnSelect, PorterDuff.Mode.SRC_IN);
            }
        }
        tlHistoryPaymentMainfvbi.setTabGravity(TabLayout.GRAVITY_FILL);
        tlHistoryPaymentMainfvbi.setEnabled(false);

        mPagerAdapter = new SlideHistoryPaymentAdapter(getActivity().getSupportFragmentManager(), titleTab);
        vpClientHistoryPaymentfvbi.setAdapter(mPagerAdapter);
        vpClientHistoryPaymentfvbi.setOffscreenPageLimit(titleTab.length);
        vpClientHistoryPaymentfvbi.setCurrentItem(0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // Not implemented here
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }
}
