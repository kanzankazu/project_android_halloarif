package id.halloarif.app.view.activity.Main;

public class NotifModel {
    private String image;
    private String detail;
    private String dateTime;
    private boolean read;
    private String target;

    public NotifModel(String image, String detail, String dateTime, boolean isRead) {
        this.image = image;
        this.detail = detail;
        this.dateTime = dateTime;
        this.read = isRead;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "NotifModel{" +
                "image='" + image + '\'' +
                ", detail='" + detail + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", read=" + read +
                ", target='" + target + '\'' +
                '}';
    }
}
