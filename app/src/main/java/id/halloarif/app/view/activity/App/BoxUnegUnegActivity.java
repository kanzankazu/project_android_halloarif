package id.halloarif.app.view.activity.App;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.InputValidUtil;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.util.SystemUtil;
import id.halloarif.app.view.activity.Logreg.LoginActivity;
import id.halloarif.app.view.activity.Role.Client.ClientHomeFragmentMain;

public class BoxUnegUnegActivity extends AppCompatActivity {

    private TextView tvBoxMainInfofvbi;
    private LinearLayout llBoxMainHistoryfvbi;
    private CardView cvBoxMainHistoryfvbi;
    private LinearLayout llBoxMainInputfvbi;
    private CardView cvBoxMainInputStartfvbi;
    private CardView cvBoxMainInputFormfvbi;
    private Spinner spBoxInputKategorifvbi;
    private LinearLayout llBoxInputKategoriDetailfvbi;
    private TextView tvBoxInputKategoriDetailKeadaanfvbi;
    private TextView tvBoxInputKategoriDetailSubfvbi;
    private EditText etBoxInputPesanfvbi;
    private CardView bBoxInputExecutefvbi;
    private CardView bBoxInputRecordfvbi;
    private CardView bBoxInputPlayStopfvbi;
    private LinearLayout llBoxMainPricefvbi;
    private RecyclerView rvBoxPricefvbi;

    private boolean isReachLimit = false;
    private String sMasalah;
    private String qnaData;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_uneg_uneg);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        tvBoxMainInfofvbi = (TextView) findViewById(R.id.tvBoxMainInfo);
        llBoxMainHistoryfvbi = (LinearLayout) findViewById(R.id.llBoxMainHistory);
        cvBoxMainHistoryfvbi = (CardView) findViewById(R.id.cvBoxMainHistory);

        cvBoxMainInputStartfvbi = (CardView) findViewById(R.id.cvBoxMainInputStart);

        llBoxMainInputfvbi = (LinearLayout) findViewById(R.id.llBoxMainInput);
        cvBoxMainInputFormfvbi = (CardView) findViewById(R.id.cvBoxMainInputForm);
        spBoxInputKategorifvbi = (Spinner) findViewById(R.id.spBoxInputKategori);
        llBoxInputKategoriDetailfvbi = (LinearLayout) findViewById(R.id.llBoxInputKategoriDetail);
        tvBoxInputKategoriDetailKeadaanfvbi = (TextView) findViewById(R.id.tvBoxInputKategoriDetailKeadaan);
        tvBoxInputKategoriDetailSubfvbi = (TextView) findViewById(R.id.tvBoxInputKategoriDetailSub);
        etBoxInputPesanfvbi = (EditText) findViewById(R.id.etBoxInputPesan);
        bBoxInputExecutefvbi = (CardView) findViewById(R.id.bBoxInputExecute);
        bBoxInputRecordfvbi = (CardView) findViewById(R.id.bBoxInputRecord);
        bBoxInputPlayStopfvbi = (CardView) findViewById(R.id.bBoxInputPlayStop);

        llBoxMainPricefvbi = (LinearLayout) findViewById(R.id.llBoxMainPrice);
        rvBoxPricefvbi = (RecyclerView) findViewById(R.id.rvBoxPrice);

    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        cvBoxMainInputStartfvbi.setVisibility(View.GONE);
        llBoxInputKategoriDetailfvbi.setVisibility(View.GONE);

        if (isReachLimit) {
            tvBoxMainInfofvbi.setVisibility(View.VISIBLE);
            llBoxMainInputfvbi.setVisibility(View.GONE);
        } else {
            tvBoxMainInfofvbi.setVisibility(View.GONE);
            llBoxMainInputfvbi.setVisibility(View.VISIBLE);
        }

        initSpinnerCategory();

    }

    private void initListener() {
        cvBoxMainHistoryfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BoxUnegUnegActivity.this, BoxUnegUnegHistoryActivity.class);
                startActivity(intent);
            }
        });

        bBoxInputExecutefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boxInputValidation();
            }
        });
    }

    private void boxInputValidation() {
        int iCategory = spBoxInputKategorifvbi.getSelectedItemPosition();
        String sKategory = spBoxInputKategorifvbi.getSelectedItem().toString();
        String sSubCategory = tvBoxInputKategoriDetailSubfvbi.getText().toString().trim();
        String sKeadaan = tvBoxInputKategoriDetailKeadaanfvbi.getText().toString().trim();
        String sPesan = etBoxInputPesanfvbi.getText().toString().trim();

        if (iCategory == 0) {
            Snackbar.make(findViewById(android.R.id.content), "Sahabat belum memilih kategori", Snackbar.LENGTH_SHORT).show();
        } else {
            if (TextUtils.isEmpty(sPesan)) {
                InputValidUtil.errorET(etBoxInputPesanfvbi, "Sahabat belum menceritakan permasalahan sahabat");
            } else {
                Toast.makeText(getApplicationContext(), sKategory + ",=," + sSubCategory + ",=," + sKeadaan + ",=," + sPesan, Toast.LENGTH_SHORT).show();
                Log.d("Lihat", "boxInputValidation BoxUnegUnegActivity : " + sKategory + ",=," + sSubCategory + ",=," + sKeadaan + ",=," + sPesan);
            }
        }
    }

    private void initSpinnerCategory() {
        List<String> stringsCategory = new ArrayList<>();
        stringsCategory.add("Pilih kategori permasalahan sahabat");
        stringsCategory.add("Percintaan");
        stringsCategory.add("Keuangan");
        stringsCategory.add("Keluarga");
        stringsCategory.add("Pertemanan");
        stringsCategory.add("Studi dan Karir");
        stringsCategory.add("Permasalahan Diri");
        SystemUtil.initSpinnerCategory(BoxUnegUnegActivity.this, spBoxInputKategorifvbi, stringsCategory);
        spBoxInputKategorifvbi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int iCategory = adapterView.getSelectedItemPosition();

                if (iCategory != 0) {
                    SessionUtil.setIntPreferences(ISeasonConfig.KEY_QNA_DATA_CATEGORY, iCategory - 1);
                    ClientHomeFragmentMain.moveToClientQuestion(BoxUnegUnegActivity.this, false, ClientHomeFragmentMain.REQ_CODE_BOX);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ClientHomeFragmentMain.REQ_CODE_BOX) {
            boolean b = ClientHomeFragmentMain.checkResult(BoxUnegUnegActivity.this, true, requestCode, resultCode, data, null, null);
            if (b) {
                qnaData = SessionUtil.getStringPreferences(ISeasonConfig.KEY_QNA_DATA, null);
                llBoxInputKategoriDetailfvbi.setVisibility(View.VISIBLE);
                String[] qnaDatas = qnaData.split(",=,");
                tvBoxInputKategoriDetailKeadaanfvbi.setText(qnaDatas[0].split("\\.")[1].trim());
                tvBoxInputKategoriDetailSubfvbi.setText(qnaDatas[1].split("\\.")[1].trim());
            } else {
                Toast.makeText(getApplicationContext(), "Gagal", Toast.LENGTH_SHORT).show();
                llBoxInputKategoriDetailfvbi.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(BoxUnegUnegActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
