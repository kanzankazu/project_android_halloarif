package id.halloarif.app.view.activity.Logreg;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.javiersantos.appupdater.AppUpdater;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.view.MainActivity;
import id.halloarif.app.view.MainActivityNew;

public class SplashscreenActivity extends AppCompatActivity {

    private ImageView ivSplaashLogofvbi;
    private TextView tvSplashVersionfvbi;

    private boolean loggedin;
    private String userId;
    private String appVersionName;
    private int appVersionCode;
    private String appName;
    private Object appPkg;
    private AppUpdater appUpdater;
    private boolean introin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        getAppdata();
        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void getAppdata() {
        try {
            appVersionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            appVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        appName = getResources().getString(R.string.app_name);
        appPkg = SplashscreenActivity.this.getBaseContext().getPackageName();
    }

    private void initComponent() {
        ivSplaashLogofvbi = (ImageView) findViewById(R.id.ivSplaashLogo);
        tvSplashVersionfvbi = (TextView) findViewById(R.id.tvSplashVersion);

    }

    private void initParam() {
        SessionUtil.removeKeyPreferences(ISeasonConfig.KEY_QNA_DATA_CATEGORY);
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.HAS_INTRO)) {
            introin = SessionUtil.getBoolPreferences(ISeasonConfig.HAS_INTRO, false);
        } else {
            introin = false;
        }
    }

    private void initContent() {
        tvSplashVersionfvbi.setText(appVersionName + "." + appVersionCode);

        //checkUpdate();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Ada ingin ke webview atau android?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Webview", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(SplashscreenActivity.this, MainActivityNew.class);
                startActivity(intent);
                finish();
            }
        });
        alertDialogBuilder.setNegativeButton("Android", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        //code here
                        if (introin) {
                            moveToLogin();
                        } else {
                            moveToIntro();
                        }
                    }
                }, 1500);
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void initListener() {

    }

    private void checkUpdate() {
        appUpdater = new AppUpdater(this)
                .setTitleOnUpdateAvailable("Pembaruan Tersedia!")
                .setContentOnUpdateAvailable("Ada versi terbaru dari aplikasi Hallo Arif, Anda ingin melakukan pembaruan aplikasi sekarang?")
                .setButtonUpdate("Ya, Perbarui")
                .setButtonDismiss("Tidak, Terima Kasih")
                .setButtonDoNotShowAgain(null);
        appUpdater.start();
    }

    private void moveToLogin() {
        Intent intent = new Intent(SplashscreenActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void moveToIntro() {
        Intent intent = new Intent(SplashscreenActivity.this, IntroActivity.class);
        startActivity(intent);
        finish();
    }
}
