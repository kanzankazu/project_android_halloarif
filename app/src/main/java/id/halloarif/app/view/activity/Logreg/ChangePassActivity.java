package id.halloarif.app.view.activity.Logreg;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.InputValidUtil;
import id.halloarif.app.util.SessionUtil;

public class ChangePassActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etChangePassPassword1fvbi, etChangePassPassword2fvbi;
    private ImageView ibChangePassPassword1fvbi, ibChangePassPassword2fvbi;
    private CardView bChangePassExecutefvbi;
    private boolean passwordShown1;
    private boolean passwordShown2;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        etChangePassPassword1fvbi = (EditText) findViewById(R.id.etChangePassPassword1);
        etChangePassPassword2fvbi = (EditText) findViewById(R.id.etChangePassPassword2);
        ibChangePassPassword1fvbi = (ImageView) findViewById(R.id.ibChangePassPassword1);
        ibChangePassPassword2fvbi = (ImageView) findViewById(R.id.ibChangePassPassword2);
        bChangePassExecutefvbi = (CardView) findViewById(R.id.bChangePassExecute);

    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.KEY_USER_ID)) {
            userId = bundle.getIntExtra(ISeasonConfig.KEY_USER_ID, 0);
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_USER_ID, 0);
        }
    }

    private void initContent() {

    }

    private void initListener() {
        etChangePassPassword2fvbi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_GO) {
                    changePassValid();
                }
                return handled;
            }
        });

        ibChangePassPassword1fvbi.setOnClickListener(this);
        ibChangePassPassword2fvbi.setOnClickListener(this);
        bChangePassExecutefvbi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == ibChangePassPassword1fvbi) {
            if (passwordShown1) {//tidak terlihat
                passwordShown1 = false;
                etChangePassPassword1fvbi.setInputType(129);//edittest
                etChangePassPassword1fvbi.setTypeface(Typeface.SANS_SERIF);
                ibChangePassPassword1fvbi.setImageResource(R.drawable.ic_visibility_off_black_24dp);
            } else {//terlihat
                passwordShown1 = true;
                etChangePassPassword1fvbi.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                ibChangePassPassword1fvbi.setImageResource(R.drawable.ic_visibility_black_24dp);
            }
        } else if (view == ibChangePassPassword2fvbi) {
            if (passwordShown2) {//tidak terlihat
                passwordShown2 = false;
                etChangePassPassword2fvbi.setInputType(129);//edittest
                etChangePassPassword2fvbi.setTypeface(Typeface.SANS_SERIF);
                ibChangePassPassword2fvbi.setImageResource(R.drawable.ic_visibility_off_black_24dp);
            } else {//terlihat
                passwordShown2 = true;
                etChangePassPassword2fvbi.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                ibChangePassPassword2fvbi.setImageResource(R.drawable.ic_visibility_black_24dp);
            }
        } else if (view == bChangePassExecutefvbi) {
            changePassValid();
        }
    }

    private void changePassValid() {
        String sPass1 = etChangePassPassword1fvbi.getText().toString().trim();
        String sPass2 = etChangePassPassword2fvbi.getText().toString().trim();

        if (!InputValidUtil.isEmptyField("Kolom ini masih kosong", etChangePassPassword1fvbi, etChangePassPassword2fvbi)) {
            if (!InputValidUtil.isMatch(sPass1, sPass2)) {
                Toast.makeText(getApplicationContext(), "Password tidak cocok", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Password cocok", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Field masih ada yang kosong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
