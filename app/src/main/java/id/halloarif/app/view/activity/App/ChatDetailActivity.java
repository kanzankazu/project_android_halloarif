package id.halloarif.app.view.activity.App;

import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.ClipboardManager;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.DateTimeUtil;
import id.halloarif.app.util.InputValidUtil;
import id.halloarif.app.util.SessionUtil;

public class ChatDetailActivity extends AppCompatActivity {

    private RecyclerView rvChatDetailMainfvbi;
    private LinearLayout llChatDetailInputfvbi;
    private EditText etChatDetailInputfvbi;
    private CardView cvChatDetailInputfvbi;

    public static int userId;
    private int roleId;
    private ChatDetailActivityAdapter adapter;
    private ArrayList<ChatDetailModel> models;
    private AppInfoModel model;

    boolean isExpired = false;
    boolean isHasLimit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_motivator_detail);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        rvChatDetailMainfvbi = (RecyclerView) findViewById(R.id.rvChatDetailMain);
        llChatDetailInputfvbi = (LinearLayout) findViewById(R.id.llChatDetailInput);
        etChatDetailInputfvbi = (EditText) findViewById(R.id.etChatDetailInput);
        cvChatDetailInputfvbi = (CardView) findViewById(R.id.cvChatDetailInput);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.PARAM)) {
            model = (AppInfoModel) bundle.getParcelableExtra(ISeasonConfig.PARAM);
        }
    }

    private void initSession() {
        /*if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }*/
        userId = 1;

        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
            roleId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
        }
    }

    private void initContent() {

        initChatDetailAdapter();

        updateUI();
    }

    private void updateUI() {
        if (isExpired || isHasLimit) {
            llChatDetailInputfvbi.setVisibility(View.GONE);
        } else {
            llChatDetailInputfvbi.setVisibility(View.VISIBLE);
        }
    }

    private void initListener() {
        cvChatDetailInputfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sMessage = etChatDetailInputfvbi.getText().toString().trim();
                String sTime = DateTimeUtil.dateToString(DateTimeUtil.getCurrentDate(), new SimpleDateFormat("HH:mm"));

                if (InputValidUtil.isEmptyField(etChatDetailInputfvbi)) {
                    InputValidUtil.errorET(etChatDetailInputfvbi, "Sahabat belum mengetik sesuatu");
                } else {
                    adapter.addData(new ChatDetailModel((models.size() + 2), sTime, false, 1, 1, sMessage));
                    etChatDetailInputfvbi.setText("");
                    rvChatDetailMainfvbi.smoothScrollToPosition(adapter.getItemCount());
                }
            }
        });

        rvChatDetailMainfvbi.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    rvChatDetailMainfvbi.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rvChatDetailMainfvbi.smoothScrollToPosition(adapter.getItemCount() - 1);
                        }
                    }, 100);
                }
            }
        });
    }

    private void initChatDetailAdapter() {
        models = new ArrayList<>();
        models.add(new ChatDetailModel(2, "11:51", true, 1, 2, "Semoga sang maha pencipta memberikan keselamatan dan kesejahteraan untuk kita di manapun berada. Amin."));
        models.add(new ChatDetailModel(1, "11:50", true, 1, 1, "hallo"));
        models.add(new ChatDetailModel(3, "11:51", true, 1, 1, "saya baik"));
        models.add(new ChatDetailModel(4, "11:51", true, 1, 2, "oke"));

        adapter = new ChatDetailActivityAdapter(ChatDetailActivity.this, models, userId,
                new ChatDetailActivityAdapter.ChatDetailActivityAdapterListener() {
                    @Override
                    public void click(ChatDetailModel model, int position) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChatDetailActivity.this);
                        alertDialogBuilder.setMessage("Pilih Opsi?");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if (model.getUser_id() == userId) {
                                    ChatDetailModel modelTemp = model;
                                    adapter.removeAt(position);

                                    Snackbar.make(findViewById(android.R.id.content), "Chat berhasil di hapus", Snackbar.LENGTH_LONG)
                                            .setAction("UNDO", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    adapter.restoreData(modelTemp, position);
                                                    Snackbar.make(findViewById(android.R.id.content), "Chat berhasil di pulihkan", Snackbar.LENGTH_SHORT).show();
                                                }
                                            }).show();
                                } else {
                                    Snackbar.make(findViewById(android.R.id.content), "Sahabat tidak bisa menghapus chat motivator", Snackbar.LENGTH_SHORT).show();
                                }
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Salin Tulisan", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                ChatDetailModel model = models.get(position);

                                int sdk = Build.VERSION.SDK_INT;
                                if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                    clipboard.setText(model.getMessage());
                                } else {
                                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                    ClipData clip = ClipData.newPlainText("text label", model.getMessage());
                                    clipboard.setPrimaryClip(clip);
                                }

                                Snackbar.make(findViewById(android.R.id.content), "Pesan Tersalin", Snackbar.LENGTH_SHORT).show();
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                });
        rvChatDetailMainfvbi.setAdapter(adapter);
        /*LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        rvChatDetailMainfvbi.setLayoutManager(layoutManager);*/
        rvChatDetailMainfvbi.setLayoutManager(new LinearLayoutManager(this) {
            @Override
            public boolean requestChildRectangleOnScreen(RecyclerView parent, View child, Rect rect, boolean immediate) {
                return false;
            }
        });
    }
}
