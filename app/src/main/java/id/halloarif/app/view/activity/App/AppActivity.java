package id.halloarif.app.view.activity.App;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.view.activity.Logreg.LoginActivity;

public class AppActivity extends AppCompatActivity {

    private TextView tvAppMainInfofvbi;
    private LinearLayout llAppMainHistoryfvbi;
    private CardView cvAppMainHistoryfvbi;
    private TextView tvAppMainHistoryfvbi;
    private LinearLayout llAppMainInfofvbi;
    private RecyclerView rvAppMainInfofvbi;
    private LinearLayout llAppMainPricefvbi;
    private RecyclerView rvAppPricefvbi;

    private int roleId;
    private ArrayList<AppInfoModel> models;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        tvAppMainInfofvbi = (TextView) findViewById(R.id.tvAppMainInfo);

        llAppMainHistoryfvbi = (LinearLayout) findViewById(R.id.llAppMainHistory);
        cvAppMainHistoryfvbi = (CardView) findViewById(R.id.cvAppMainHistory);
        tvAppMainHistoryfvbi = (TextView) findViewById(R.id.tvAppMainHistory);

        llAppMainInfofvbi = (LinearLayout) findViewById(R.id.llAppMainInfo);
        rvAppMainInfofvbi = (RecyclerView) findViewById(R.id.rvAppMainInfo);

        llAppMainPricefvbi = (LinearLayout) findViewById(R.id.llAppMainPrice);
        rvAppPricefvbi = (RecyclerView) findViewById(R.id.rvAppPrice);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.PARAM)) {
            type = bundle.getStringExtra(ISeasonConfig.PARAM);
            Snackbar.make(findViewById(android.R.id.content), type, Snackbar.LENGTH_SHORT).show();
        } else {
            onBackPressed();
            Toast.makeText(getApplicationContext(), "no detect type App Feature", Toast.LENGTH_SHORT).show();
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
            roleId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
        } else {
            onBackPressed();
            Toast.makeText(getApplicationContext(), "no detect role ID", Toast.LENGTH_SHORT).show();
        }
    }

    private void initContent() {
        String s;
        if (type.equalsIgnoreCase("chat")) {
            s = "Lihat riwayat chat motivasi di sini";
        } else {
            s = "Lihat riwayat call motivasi di sini";
        }
        tvAppMainHistoryfvbi.setText(s);

        initRecycleChatReady();
        initRecycleChatPrice();
    }

    private void initListener() {
        cvAppMainHistoryfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AppActivity.this, AppHistoryActivity.class);
                intent.putExtra(ISeasonConfig.PARAM, type);
                startActivity(intent);
                //finish();
            }
        });
    }

    private void initRecycleChatReady() {
        models = new ArrayList<>();
        models.add(new AppInfoModel("Move On", true, 0, true, "28/03/2019 12:00"));
        models.add(new AppInfoModel("Bangkit", false, 3, true, "28/03/2019 12:00"));
        models.add(new AppInfoModel("Berkarakter", true, 0, false, "28/03/2019 12:00"));
        models.add(new AppInfoModel("Move On", false, 6, true, "28/03/2019 12:00"));
        models.add(new AppInfoModel("Gemilang", true, 0, true, "28/03/2019 12:00"));

        List<AppInfoModel> appInfoActive = new ArrayList<>();
        List<AppInfoModel> appInfoDeactive = new ArrayList<>();
        for (int i = 0; i < models.size(); i++) {
            AppInfoModel model = models.get(i);

            if (!model.isExpired()) {
                appInfoActive.add(model);
            } else {
                appInfoDeactive.add(model);
            }
        }

        if (appInfoDeactive.size() == 0) {
            llAppMainHistoryfvbi.setVisibility(View.GONE);
        } else {
            llAppMainHistoryfvbi.setVisibility(View.VISIBLE);
        }

        if (appInfoActive.size() == 0) {
            llAppMainInfofvbi.setVisibility(View.GONE);
            tvAppMainInfofvbi.setVisibility(View.VISIBLE);
        } else {
            llAppMainInfofvbi.setVisibility(View.VISIBLE);
            tvAppMainInfofvbi.setVisibility(View.GONE);

            AppReadyHistoryAdapter adapter = new AppReadyHistoryAdapter(AppActivity.this, appInfoActive, type,
                    new AppReadyHistoryAdapter.AppReadyAdapterListener() {
                        @Override
                        public void onClick(AppInfoModel model) {
                            Intent intent = null;
                            if (type.equalsIgnoreCase("chat")) {
                                intent = new Intent(getBaseContext(), ChatDetailActivity.class);
                            } else if (type.equalsIgnoreCase("call")) {
                                intent = new Intent(getApplicationContext(), CallDetailActivity.class);
                            }
                            intent.putExtra(ISeasonConfig.PARAM, model);
                            startActivity(intent);
                        }
                    });
            rvAppMainInfofvbi.setAdapter(adapter);
            rvAppMainInfofvbi.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    private void initRecycleChatPrice() {
        List<AppInfoModel> models = new ArrayList<>();
        models.add(new AppInfoModel("Move On", "25000", false, 10, 3, true));
        models.add(new AppInfoModel("Bangkit", "35000", false, 30, 3, true));
        models.add(new AppInfoModel("Berkarakter", "100000", false, 75, 5, true));
        models.add(new AppInfoModel("Gemilang", "150000", false, 100, 7, true));

        List<AppInfoModel> models1 = new ArrayList<>();
        for (int i = 0; i < models.size(); i++) {
            AppInfoModel model = models.get(i);

            if (model.isEnable()) {
                models1.add(model);
            }
        }

        if (models1.size() == 0) {
            llAppMainPricefvbi.setVisibility(View.GONE);
        } else {
            AppPriceAdapter adapter = new AppPriceAdapter(AppActivity.this, models, roleId, type,
                    new AppPriceAdapter.AppPriceAdapterListener() {

                        @Override
                        public void onEdit() {

                        }

                        @Override
                        public void onRemove() {

                        }

                        @Override
                        public void onBuy() {

                        }

                        @Override
                        public void onchat() {

                        }
                    });
            rvAppPricefvbi.setAdapter(adapter);
            rvAppPricefvbi.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(AppActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
