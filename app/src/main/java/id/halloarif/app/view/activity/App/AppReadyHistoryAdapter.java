package id.halloarif.app.view.activity.App;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.halloarif.app.R;

public class AppReadyHistoryAdapter extends RecyclerView.Adapter<AppReadyHistoryAdapter.ViewHolder> {
    private Activity activity;
    private List<AppInfoModel> models;
    private String type;
    private final AppReadyAdapterListener listener;

    public AppReadyHistoryAdapter(Activity activity, List<AppInfoModel> models, String type, AppReadyAdapterListener listener) {
        this.activity = activity;
        this.models = models;
        this.type = type;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_client_chatcall_info, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AppInfoModel model = models.get(position);
        holder.tvlistReadyNamefvbi.setText(model.getNama_paket());

        String s1 = null;
        String s3 = null;
        if (type.equalsIgnoreCase("chat")) {
            s1 = model.isUnlimited() ? "Unlimited Chat" : (model.getApp_tersisa() + " Chat Tersisa");
            s3 = "chat";
        } else if (type.equalsIgnoreCase("call")) {
            s1 = model.isUnlimited() ? "Unlimited Call" : (model.getApp_tersisa() + " Panggilan Tersisa");
            s3 = "call";
        }
        holder.tvlistReadyFeaturefvbi.setText(s1);
        holder.tvlistReadyTitlefvbi.setText(s3);

        String s2 = model.isExpired() ? "Masa Aktif Telah Berakhir" : ("Berlaku Hingga " + model.getDate_time_detail_expired());
        holder.tvlistReadyDurasifvbi.setText(s2);

        holder.cvlistReadyBuyfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvlistReadyNamefvbi;
        private final TextView tvlistReadyFeaturefvbi;
        private final TextView tvlistReadyDurasifvbi;
        private final CardView cvlistReadyBuyfvbi;
        private final TextView tvlistReadyTitlefvbi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvlistReadyNamefvbi = (TextView) itemView.findViewById(R.id.tvlistReadyName);
            tvlistReadyFeaturefvbi = (TextView) itemView.findViewById(R.id.tvlistReadyFeature);
            tvlistReadyDurasifvbi = (TextView) itemView.findViewById(R.id.tvlistReadyDurasi);
            cvlistReadyBuyfvbi = (CardView) itemView.findViewById(R.id.cvlistReadyBuy);
            tvlistReadyTitlefvbi = (TextView) itemView.findViewById(R.id.tvlistReadyTitle);
        }
    }

    public interface AppReadyAdapterListener {
        void onClick(AppInfoModel model);
    }

    public void setData(List<AppInfoModel> datas) {
        if (models.size() > 0) {
            models.clear();
            models = datas;
        } else {
            models = datas;
        }
        notifyDataSetChanged();
    }

    public void replaceData(List<AppInfoModel> datas) {
        models.clear();
        models.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<AppInfoModel> datas) {
        models.addAll(datas);
        notifyItemRangeInserted(models.size(), datas.size());
    }

    public void addDataFirst(AppInfoModel data) {
        int position = 0;
        models.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    public void removeDataFirst() {
        int position = 0;
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

}
