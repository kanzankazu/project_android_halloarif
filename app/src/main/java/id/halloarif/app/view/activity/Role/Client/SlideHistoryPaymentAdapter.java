package id.halloarif.app.view.activity.Role.Client;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SlideHistoryPaymentAdapter extends FragmentStatePagerAdapter {
    private final String[] titleTab;

    public SlideHistoryPaymentAdapter(FragmentManager fm, String[] titleTab) {
        super(fm);
        this.titleTab = titleTab;
    }

    @Override
    public Fragment getItem(int position) {

        if (position == 0) {
            return new HistoryPaymentDetail();
        } else if (position == 1) {
            return new HistoryPaymentNew();
        }

        return null;
    }

    @Override
    public int getCount() {
        return titleTab.length;
    }
}
