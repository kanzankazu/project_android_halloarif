package id.halloarif.app.view.activity.Role.Client;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.model.startappquestion.QuestionIsiModel;
import id.halloarif.app.model.startappquestion.QuestionMainModel;
import id.halloarif.app.util.InputValidUtil;
import id.halloarif.app.util.ListArrayUtil;
import id.halloarif.app.util.SystemUtil;
import id.halloarif.app.util.support.NonSwipeableViewPager;

public class ClientQuestionFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int q_no;
    private String q_answer;
    private String q_desc;
    private ArrayList<QuestionIsiModel> q_isi;
    private int q_other;
    private String q_type;
    private boolean q_last;

    private TextView tvQuestionDescfvbi, tvQuestionfvbi;
    private LinearLayout llQuestionButtonfvbi, llQuestionfvbi;
    private RadioGroup rg;
    private EditText input;
    private EditText inputother;
    private Spinner spinner;

    private int currentItem;
    private int size;

    private NonSwipeableViewPager vpClientHomeActivityQuestionfvbi;
    private List<QuestionMainModel> models;
    private boolean lastIndex;

    public ClientQuestionFragment() {
        // Required empty public constructor
    }

    public static ClientQuestionFragment newInstance(String param1, String param2) {
        ClientQuestionFragment fragment = new ClientQuestionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        ClientQuestionFragment fragment = new ClientQuestionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            q_no = getArguments().getInt(ISeasonConfig.Q_NO);
            q_answer = getArguments().getString(ISeasonConfig.Q_ANSWER);
            q_desc = getArguments().getString(ISeasonConfig.Q_DESC);
            q_isi = getArguments().getParcelableArrayList(ISeasonConfig.Q_ISI);
            q_other = getArguments().getInt(ISeasonConfig.Q_OTHER);
            q_type = getArguments().getString(ISeasonConfig.Q_TYPE);
            q_last = getArguments().getBoolean(ISeasonConfig.Q_LAST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.question_layout, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        tvQuestionDescfvbi = (TextView) view.findViewById(R.id.tvQuestionDesc);
        tvQuestionfvbi = (TextView) view.findViewById(R.id.tvQuestion);
        llQuestionfvbi = (LinearLayout) view.findViewById(R.id.llQuestion);
        llQuestionButtonfvbi = (LinearLayout) view.findViewById(R.id.llQuestionButton);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        setFeedbackForm();
    }

    private void initListener() {

    }

    private void setFeedbackForm() {

        boolean isLastPageSwiped = ((ClientQuestionActivity) getActivity()).isLastPageSwiped;
        //Log.d("Lihat", "initContent ClientQuestionFragment isLastPageSwiped: " + isLastPageSwiped);
        StringBuilder builderAnswer = ((ClientQuestionActivity) getActivity()).builderAnswer;
        //Log.d("Lihat", "initContent ClientQuestionFragment builderAnswer.toString().trim() : " + builderAnswer.toString().trim());

        if (!TextUtils.isEmpty(q_desc)) {
            tvQuestionDescfvbi.setText(q_desc);
            tvQuestionDescfvbi.setVisibility(View.VISIBLE);
        } else {
            tvQuestionDescfvbi.setVisibility(View.GONE);
        }

        tvQuestionfvbi.setText(q_no + " . " + q_answer);

        int paddingDp = 10;
        float density = getActivity().getResources().getDisplayMetrics().density;
        int paddingPixel = (int) (paddingDp * density);

        LinearLayout childLL = new LinearLayout(getActivity());
        LinearLayout.LayoutParams paramChildLL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        childLL.setLayoutParams(paramChildLL);
        childLL.setPadding(paddingPixel, paddingPixel, paddingPixel, paddingPixel);
        childLL.setOrientation(LinearLayout.VERTICAL);

        if (q_type.equalsIgnoreCase("checkbox")) {

            LinearLayout row = new LinearLayout(getActivity());
            row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            row.setGravity(Gravity.CENTER);
            row.setOrientation(LinearLayout.VERTICAL);
            childLL.addView(row);

            for (int i = 0; i < q_isi.size(); i++) {
                QuestionIsiModel model = q_isi.get(i);
                CheckBox checkBox = new CheckBox(getActivity());
                checkBox.setId(i);
                checkBox.setTag(model.getJawaban());
                checkBox.setText(model.getJawaban());
                row.addView(checkBox);
            }

        } else if (q_type.equalsIgnoreCase("radio")) {
            rg = new RadioGroup(getActivity()); //create the RadioGroup
            rg.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            rg.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL
            for (int i = 0; i < q_isi.size(); i++) {
                QuestionIsiModel model = q_isi.get(i);

                RadioButton radioButton = new RadioButton(getActivity());
                radioButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                radioButton.setId(i);
                radioButton.setTag(model.getJawaban());
                radioButton.setText(model.getJawaban());
                rg.addView(radioButton);
            }

            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int position) {
                    if (q_other == 1) {
                        if (position == radioGroup.getChildCount() - 1) {
                            inputother.setEnabled(true);
                        } else {
                            inputother.setEnabled(false);
                        }
                    }
                }
            });

            childLL.addView(rg);

        } else if (q_type.equalsIgnoreCase("textarea")) {
            input = new EditText(getActivity());
            input.setTag("jawaban");
            input.setHint("Masukan ");
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setImeOptions(EditorInfo.IME_ACTION_DONE);
            childLL.addView(input);

        } else if (q_type.equalsIgnoreCase("spinner")) {
            spinner = new Spinner(getActivity());

            //Spinner
            List<String> strings = new ArrayList<String>();
            strings.add("Silahkan pilih");
            for (int i = 0; i < models.size(); i++) {
                QuestionIsiModel model = q_isi.get(i);
                strings.add(model.getJawaban());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, strings) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                    //return
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        SystemUtil.changeColText(R.color.gray, tv);
                    } else {
                        SystemUtil.changeColText(R.color.colorPrimary, tv);
                    }
                    return view;
                }
            };
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }

        if (q_other == 1) {
            inputother = new EditText(getActivity());
            inputother.setTag("lainnya");
            inputother.setHint("Masukan jawaban lainnya");
            inputother.setInputType(InputType.TYPE_CLASS_TEXT);
            inputother.setImeOptions(EditorInfo.IME_ACTION_DONE);
            childLL.addView(inputother);
        }

        llQuestionfvbi.addView(childLL);

        Button button = new Button(getActivity());
        button.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        button.setBackgroundResource(R.drawable.background_radius_bottom);
        button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        button.setGravity(Gravity.CENTER);
        button.setTextColor(getResources().getColor(R.color.white));

        if (q_last) {
            button.setText("DONE");
        } else {
            button.setText("NEXT");
        }

        llQuestionButtonfvbi.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vpClientHomeActivityQuestionfvbi = ((ClientQuestionActivity) getActivity()).vpClientHomeActivityQuestionfvbi;
                models = ((ClientQuestionActivity) getActivity()).mainModels;

                StringBuilder stringBuilder = new StringBuilder();

                RadioGroup radioGroup = null;
                for (int i = 0; i < llQuestionfvbi.getChildCount(); i++) {
                    View child = llQuestionfvbi.getChildAt(i);

                    if (child instanceof LinearLayout) {
                        LinearLayout linearLayout = (LinearLayout) child;

                        for (int i1 = 0; i1 < linearLayout.getChildCount(); i1++) {
                            View child1 = linearLayout.getChildAt(i1);

                            if (child1 instanceof RadioGroup) {
                                radioGroup = (RadioGroup) child1;

                                int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();

                                for (int i2 = 0; i2 < radioGroup.getChildCount(); i2++) {
                                    RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i2);

                                    if (checkedRadioButtonId == -1) {
                                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Anda belum memilih", Snackbar.LENGTH_SHORT).show();
                                    } else {
                                        if (radioButton.getId() == checkedRadioButtonId) {
                                            String s = radioButton.getTag().toString().trim();

                                            if (q_other == 1) {
                                                //builderAnswer.append(q_no + "." + s);

                                                /*if (i2 == (radioGroup.getChildCount() - 1)) {
                                                    builderAnswer.append(q_no + "." + s);
                                                } else {
                                                    builderAnswer.append(q_no + "." + s);
                                                    builderAnswer.append(",=,");
                                                    moveNextPager();
                                                }*/
                                            } else {
                                                builderAnswer.append(q_no + "." + s);
                                                builderAnswer.append(",=,");
                                                moveNextPager();
                                            }
                                        }
                                    }
                                }
                            } else if (child1 instanceof LinearLayout) {
                                LinearLayout linearLayout1 = (LinearLayout) child1;

                                StringBuilder builder = new StringBuilder();
                                for (int i3 = 0; i3 < linearLayout1.getChildCount(); i3++) {
                                    CheckBox cb = (CheckBox) linearLayout1.getChildAt(i3);

                                    if (cb.isChecked()) {
                                        builder.append(cb.getTag());
                                        builder.append(",");
                                    }
                                }
                                String s = ListArrayUtil.removeLastSeparatorStringBuilder(builder, ",");

                                if (!TextUtils.isEmpty(builder.toString())) {
                                    if (q_other == 1) {
                                        builderAnswer.append(q_no + "." + s);
                                    } else {
                                        builderAnswer.append(q_no + "." + s);
                                        builderAnswer.append(",=,");
                                        moveNextPager();
                                    }
                                } else {
                                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Anda belum memilih", Snackbar.LENGTH_SHORT).show();
                                }

                            } else if (child1 instanceof Spinner) {
                                Spinner spinner = (Spinner) child1;
                                String s = spinner.getSelectedItem().toString().trim();

                                if (q_other == 1) {
                                    builderAnswer.append(q_no + "." + s);
                                } else {
                                    builderAnswer.append(q_no + "." + s);
                                    builderAnswer.append(",=,");
                                    moveNextPager();
                                }

                            } else if (child1 instanceof EditText) {
                                EditText editText = (EditText) child1;
                                String s = editText.getText().toString().trim();

                                int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();

                                if (q_other == 1) {
                                    if (radioGroup.getCheckedRadioButtonId() == -1) {
                                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Anda belum memilih", Snackbar.LENGTH_SHORT).show();
                                    } else {
                                        if (radioGroup.getCheckedRadioButtonId() == (radioGroup.getChildCount() - 1)) {
                                            if (InputValidUtil.isEmptyField(editText)) {
                                                InputValidUtil.errorET(editText, "Field Masih Kosong");
                                            } else {

                                                for (int i2 = 0; i2 < radioGroup.getChildCount(); i2++) {
                                                    RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i2);
                                                    if (checkedRadioButtonId == -1) {
                                                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Anda belum memilih", Snackbar.LENGTH_SHORT).show();
                                                    } else {
                                                        if (radioButton.getId() == checkedRadioButtonId) {
                                                            String s1 = radioButton.getTag().toString().trim();
                                                            if (q_other == 1) {
                                                                builderAnswer.append(q_no + "." + s1);
                                                            }
                                                        }
                                                    }
                                                }

                                                builderAnswer.append("," + s);
                                                builderAnswer.append(",=,");
                                                moveNextPager();
                                            }
                                        } else {

                                            for (int i2 = 0; i2 < radioGroup.getChildCount(); i2++) {
                                                RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i2);
                                                if (checkedRadioButtonId == -1) {
                                                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Anda belum memilih", Snackbar.LENGTH_SHORT).show();
                                                } else {
                                                    if (radioButton.getId() == checkedRadioButtonId) {
                                                        String s1 = radioButton.getTag().toString().trim();
                                                        builderAnswer.append(q_no + "." + s1);
                                                    }
                                                }
                                            }

                                            builderAnswer.append(",=,");
                                            moveNextPager();
                                        }
                                    }
                                } else {
                                    if (InputValidUtil.isEmptyField(editText)) {
                                        InputValidUtil.errorET(editText, "Field Masih Kosong");
                                    } else {
                                        builderAnswer.append(q_no + "." + s);
                                        builderAnswer.append(",=,");
                                        moveNextPager();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void moveNextPager() {
        currentItem = vpClientHomeActivityQuestionfvbi.getCurrentItem();
        size = models.size();

        if (currentItem == size - 1) {
            ((ClientQuestionActivity) getActivity()).onFinish();
        } else {
            vpClientHomeActivityQuestionfvbi.setCurrentItem(currentItem + 1);
        }
    }
}
