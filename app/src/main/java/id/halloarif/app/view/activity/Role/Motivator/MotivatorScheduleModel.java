package id.halloarif.app.view.activity.Role.Motivator;

public class MotivatorScheduleModel {
    private int jadwal_id;
    private String hari_jadwal;
    private String jadwal_mulai;
    private String jadwal_selesai;
    private int user_id_client;
    private int user_id_motivator1;
    private int user_id_motivator2;
    private int user_id_hrd;
    private int box_id;
    private String jadwal_jenis_layanan;

    MotivatorScheduleModel(int jadwal_id, String hari_jadwal, String jadwal_mulai, String jadwal_selesai, int user_id_client) {
        this.jadwal_id = jadwal_id;
        this.hari_jadwal = hari_jadwal;
        this.jadwal_mulai = jadwal_mulai;
        this.jadwal_selesai = jadwal_selesai;
        this.user_id_client = user_id_client;
    }

    String getHari_jadwal() {
        return hari_jadwal;
    }

    public void setHari_jadwal(String hari_jadwal) {
        this.hari_jadwal = hari_jadwal;
    }

    public int getJadwal_id() {
        return jadwal_id;
    }

    public void setJadwal_id(int jadwal_id) {
        this.jadwal_id = jadwal_id;
    }

    public String getJadwal_mulai() {
        return jadwal_mulai;
    }

    public void setJadwal_mulai(String jadwal_mulai) {
        this.jadwal_mulai = jadwal_mulai;
    }

    public String getJadwal_selesai() {
        return jadwal_selesai;
    }

    public void setJadwal_selesai(String jadwal_selesai) {
        this.jadwal_selesai = jadwal_selesai;
    }

    public int getUser_id_client() {
        return user_id_client;
    }

    public void setUser_id_client(int user_id_client) {
        this.user_id_client = user_id_client;
    }

    public int getUser_id_motivator1() {
        return user_id_motivator1;
    }

    public void setUser_id_motivator1(int user_id_motivator1) {
        this.user_id_motivator1 = user_id_motivator1;
    }

    public int getUser_id_motivator2() {
        return user_id_motivator2;
    }

    public void setUser_id_motivator2(int user_id_motivator2) {
        this.user_id_motivator2 = user_id_motivator2;
    }

    public int getUser_id_hrd() {
        return user_id_hrd;
    }

    public void setUser_id_hrd(int user_id_hrd) {
        this.user_id_hrd = user_id_hrd;
    }

    public int getBox_id() {
        return box_id;
    }

    public void setBox_id(int box_id) {
        this.box_id = box_id;
    }

    public String getJadwal_jenis_layanan() {
        return jadwal_jenis_layanan;
    }

    public void setJadwal_jenis_layanan(String jadwal_jenis_layanan) {
        this.jadwal_jenis_layanan = jadwal_jenis_layanan;
    }
}
