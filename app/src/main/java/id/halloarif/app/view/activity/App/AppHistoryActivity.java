package id.halloarif.app.view.activity.App;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;

public class AppHistoryActivity extends AppCompatActivity {

    private RecyclerView rvAppHistoryfvbi;
    private AppReadyHistoryAdapter rvAppHistoryAdapter;

    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        rvAppHistoryfvbi = (RecyclerView) findViewById(R.id.rvAppHistory);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.PARAM)) {
            type = bundle.getStringExtra(ISeasonConfig.PARAM);
            Snackbar.make(findViewById(android.R.id.content), type, Snackbar.LENGTH_SHORT).show();
        } else {
            onBackPressed();
            Toast.makeText(getApplicationContext(), "no detect type App Feature", Toast.LENGTH_SHORT).show();
        }
    }

    private void initSession() {

    }

    private void initContent() {
        initRecycleChatHistory();
    }

    private void initListener() {

    }

    private void initRecycleChatHistory() {
        List<AppInfoModel> models = new ArrayList<>();
        models.add(new AppInfoModel("Move On", true, 0, true, "28/03/2019 12:00"));
        models.add(new AppInfoModel("Bangkit", false, 3, true, "28/03/2019 12:00"));
        models.add(new AppInfoModel("Berkarakter", true, 0, false, "28/03/2019 12:00"));
        models.add(new AppInfoModel("Move On", false, 6, true, "28/03/2019 12:00"));
        models.add(new AppInfoModel("Gemilang", true, 0, true, "28/03/2019 12:00"));

        List<AppInfoModel> models1 = new ArrayList<>();
        for (int i = 0; i < models.size(); i++) {
            AppInfoModel model = models.get(i);

            if (model.isExpired()) {
                models1.add(model);
            } else {

            }
        }

        rvAppHistoryAdapter = new AppReadyHistoryAdapter(AppHistoryActivity.this, models1, type,
                new AppReadyHistoryAdapter.AppReadyAdapterListener() {
                    @Override
                    public void onClick(AppInfoModel model) {
                        Intent intent = null;
                        if (type.equalsIgnoreCase("chat")) {
                            intent = new Intent(getApplicationContext(), ChatDetailActivity.class);
                        } else if (type.equalsIgnoreCase("call")) {
                            intent = new Intent(getApplication(), CallDetailActivity.class);
                        }
                        startActivity(intent);
                        //finish();
                    }
                });
        rvAppHistoryfvbi.setAdapter(rvAppHistoryAdapter);
        rvAppHistoryfvbi.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
