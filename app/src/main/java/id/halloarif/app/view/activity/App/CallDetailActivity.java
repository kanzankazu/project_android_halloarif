package id.halloarif.app.view.activity.App;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.util.SystemUtil;
import id.halloarif.app.view.activity.Role.Client.ClientHomeFragmentMain;

public class CallDetailActivity extends AppCompatActivity {

    private CardView cvCallDetailIntrofvbi;
    private Spinner spCallDetailMotivatorSelectfvbi;
    private Spinner spCallDetailTimefvbi;
    private LinearLayout llCallDetailTimefvbi;
    private TextView tvCallDetailTimefvbi;
    private CardView cvCallDetailRequestfvbi;
    private CardView cvCallDetailCancelfvbi;
    private CardView cvCallDetailCallfvbi;
    private CardView cvCallDetailCallExecutefvbi;
    private CircleImageView civCallDetailMotivatorPhotofvbi;
    private TextView tvCallDetailMotivatorAliasNamefvbi;
    private TextView tvCallDetailCallPaketNamefvbi;
    private TextView tvCallDetailCallDurasifvbi;
    private TextView tvCallDetailCallProblemfvbi;
    private TextView tvCallDetailExecutefvbi;
    private LinearLayout llCallDetailMotivatorSelectfvbi;
    private LinearLayout llCallDetailUserInfofvbi;
    private ImageView ivCallDetailMotivatorSelectfvbi;
    private ImageView ivCallDetailTimefvbi;
    private Spinner spCallDetailCategoryfvbi;
    private LinearLayout llCallDetailKategoriDetailfvbi;
    private ImageView ivCallDetailCategoryfvbi;
    private TextView tvCallDetailKategoriDetailKeadaanfvbi;
    private TextView tvCallDetailKategoriDetailSubfvbi;

    private boolean isIntro = false;
    private boolean isExpired = false;
    private boolean isCalling = false;
    private int roleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_motivator_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);// place in oncreate activity/fragment

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        /*INTRO*/
        cvCallDetailIntrofvbi = (CardView) findViewById(R.id.cvCallDetailIntro);

        llCallDetailMotivatorSelectfvbi = (LinearLayout) findViewById(R.id.llCallDetailMotivatorSelect);
        spCallDetailMotivatorSelectfvbi = (Spinner) findViewById(R.id.spCallDetailMotivatorSelect);
        ivCallDetailMotivatorSelectfvbi = (ImageView) findViewById(R.id.ivCallDetailMotivatorSelect);

        //llCallDetailUserInfofvbi = (LinearLayout) findViewById(R.id.llCallDetailUserInfo);
        spCallDetailTimefvbi = (Spinner) findViewById(R.id.spCallDetailTime);
        ivCallDetailTimefvbi = (ImageView) findViewById(R.id.ivCallDetailTime);
        llCallDetailTimefvbi = (LinearLayout) findViewById(R.id.llCallDetailTime);
        tvCallDetailTimefvbi = (TextView) findViewById(R.id.tvCallDetailTime);
        spCallDetailCategoryfvbi = (Spinner) findViewById(R.id.spCallDetailCategory);
        llCallDetailKategoriDetailfvbi = (LinearLayout) findViewById(R.id.llCallDetailKategoriDetail);
        ivCallDetailCategoryfvbi = (ImageView) findViewById(R.id.ivCallDetailCategory);
        tvCallDetailKategoriDetailKeadaanfvbi = (TextView) findViewById(R.id.tvCallDetailKategoriDetailKeadaan);
        tvCallDetailKategoriDetailSubfvbi = (TextView) findViewById(R.id.tvCallDetailKategoriDetailSub);
        cvCallDetailRequestfvbi = (CardView) findViewById(R.id.cvCallDetailRequest);
        cvCallDetailCancelfvbi = (CardView) findViewById(R.id.cvCallDetailCancel);

        /*DETAIL*/
        cvCallDetailCallfvbi = (CardView) findViewById(R.id.cvCallDetailCall);
        civCallDetailMotivatorPhotofvbi = (CircleImageView) findViewById(R.id.civCallDetailMotivatorPhoto);
        tvCallDetailMotivatorAliasNamefvbi = (TextView) findViewById(R.id.tvCallDetailMotivatorAliasName);
        tvCallDetailCallPaketNamefvbi = (TextView) findViewById(R.id.tvCallDetailCallPaketName);
        tvCallDetailCallDurasifvbi = (TextView) findViewById(R.id.tvCallDetailCallDurasi);
        tvCallDetailCallProblemfvbi = (TextView) findViewById(R.id.tvCallDetailCallProblem);
        cvCallDetailCallExecutefvbi = (CardView) findViewById(R.id.cvCallDetailCallExecute);
        tvCallDetailExecutefvbi = (TextView) findViewById(R.id.tvCallDetailExecute);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
            roleId = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
        }
    }

    private void initContent() {
        llCallDetailTimefvbi.setVisibility(View.GONE);
        llCallDetailKategoriDetailfvbi.setVisibility(View.GONE);

        if (roleId == ISeasonConfig.ROLE_CLIENT) {
            updateUi();
        } else if (roleId == ISeasonConfig.ROLE_MOTIVATOR || roleId == ISeasonConfig.ROLE_HUMANCAPITAL) {
            isIntro = true;
            isExpired = true;
            updateUi();
        }

        initSpinnerMotivatorSelectCall();
        initSpinnerWhenCall();
        initSpinnerCategory();
    }

    private void initListener() {
        ivCallDetailMotivatorSelectfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spCallDetailMotivatorSelectfvbi.performClick();
            }
        });
        ivCallDetailTimefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spCallDetailTimefvbi.performClick();
            }
        });
        ivCallDetailCategoryfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spCallDetailCategoryfvbi.performClick();
            }
        });

        cvCallDetailRequestfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int iMotivator = spCallDetailMotivatorSelectfvbi.getSelectedItemPosition();
                int iTime = spCallDetailTimefvbi.getSelectedItemPosition();
                int iCategory = spCallDetailCategoryfvbi.getSelectedItemPosition();

                if (iMotivator == 0 || iTime == 0 || iCategory == 0) {
                    Snackbar.make(findViewById(android.R.id.content), "Sahabat belum memilih opsi pilihan di atas.", Snackbar.LENGTH_SHORT).show();
                } else {
                    isIntro = true;
                    updateUi();
                }
            }
        });
        cvCallDetailCancelfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        cvCallDetailCallExecutefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCalling) {

                } else {

                }
            }
        });
    }

    private void initSpinnerMotivatorSelectCall() {
        //Spinner
        List<AppModel> models = new ArrayList<>();
        models.add(new AppModel(0, "Pilih Motivator", "Admin"));
        models.add(new AppModel(1, "GEMILANG001", "Admin"));
        models.add(new AppModel(2, "GEMILANG002", "Eva Kustina"));
        models.add(new AppModel(3, "GEMILANG003", "Topan"));
        models.add(new AppModel(4, "GEMILANG004", "Pinuji Rahayu"));

        CustomSpinAdapter adapter = new CustomSpinAdapter(CallDetailActivity.this, android.R.layout.simple_spinner_item, models) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
//                return
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(R.id.tvlistitemSpinner);
                if (position == 0) {
                    // Set the hint text color gray
                    SystemUtil.changeColText(R.color.gray, tv);
                } else {
                    SystemUtil.changeColText(R.color.colorPrimaryDark, tv);
                }
                return view;
            }
        };
        spCallDetailMotivatorSelectfvbi.setAdapter(adapter);
        spCallDetailMotivatorSelectfvbi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    String sValue = adapter.getItem(position).getValue();
                    Toast.makeText(getApplicationContext(), sValue, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initSpinnerWhenCall() {
        //Spinner
        List<AppModel> models = new ArrayList<>();
        models.add(new AppModel(0, "Pilih Waktu Panggilan", "0"));
        models.add(new AppModel(1, "5 Menit Kedepan", "5"));
        models.add(new AppModel(2, "10 Menit Kedepan", "10"));
        models.add(new AppModel(3, "15 Menit Kedepan", "15"));
        models.add(new AppModel(4, "20 Menit Kedepan", "20"));

        CustomSpinAdapter adapter = new CustomSpinAdapter(CallDetailActivity.this, android.R.layout.simple_spinner_item, models) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
//                return
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(R.id.tvlistitemSpinner);
                if (position == 0) {
                    // Set the hint text color gray
                    SystemUtil.changeColText(R.color.gray, tv);
                } else {
                    SystemUtil.changeColText(R.color.colorPrimaryDark, tv);
                }
                return view;
            }
        };
        spCallDetailTimefvbi.setAdapter(adapter);
        spCallDetailTimefvbi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    String sValue = adapter.getItem(position).getValue();
                    Toast.makeText(getApplicationContext(), sValue, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initSpinnerCategory() {
        List<AppModel> models = new ArrayList<>();
        models.add(new AppModel(0, "Pilih kategori permasalahan sahabat", "Pilih kategori permasalahan sahabat"));
        models.add(new AppModel(1, "Percintaan", "Percintaan"));
        models.add(new AppModel(2, "Keuangan", "Keuangan"));
        models.add(new AppModel(3, "Keluarga", "Keluarga"));
        models.add(new AppModel(4, "Pertemanan", "Pertemanan"));
        models.add(new AppModel(5, "Studi dan Karir", "Studi dan Karir"));
        models.add(new AppModel(6, "Permasalahan Diri", "Permasalahan Diri"));
        CustomSpinAdapter adapter = new CustomSpinAdapter(CallDetailActivity.this, android.R.layout.simple_spinner_item, models) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
                //return
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(R.id.tvlistitemSpinner);
                if (position == 0) {
                    // Set the hint text color gray
                    SystemUtil.changeColText(R.color.gray, tv);
                } else {
                    SystemUtil.changeColText(R.color.colorPrimaryDark, tv);
                }
                return view;
            }
        };
        spCallDetailCategoryfvbi.setAdapter(adapter);
        spCallDetailCategoryfvbi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int iCategory = parent.getSelectedItemPosition();

                if (iCategory != 0) {
                    SessionUtil.setIntPreferences(ISeasonConfig.KEY_QNA_DATA_CATEGORY, iCategory - 1);
                    ClientHomeFragmentMain.moveToClientQuestion(CallDetailActivity.this, false, ClientHomeFragmentMain.REQ_CODE_CALL);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();// place in onOptionItemSelect
        if (id == android.R.id.home) {// place in onOptionItemSelect
            onBackPressed();// place in onOptionItemSelect
        }// place in onOptionItemSelect
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ClientHomeFragmentMain.REQ_CODE_CALL) {
            boolean b = ClientHomeFragmentMain.checkResult(CallDetailActivity.this, true, requestCode, resultCode, data, null, null);
            if (b) {
                String qnaData = SessionUtil.getStringPreferences(ISeasonConfig.KEY_QNA_DATA, null);
                llCallDetailKategoriDetailfvbi.setVisibility(View.VISIBLE);
                String[] qnaDatas = qnaData.split(",=,");
                tvCallDetailKategoriDetailKeadaanfvbi.setText(qnaDatas[0].split("\\.")[1].trim());
                tvCallDetailKategoriDetailSubfvbi.setText(qnaDatas[1].split("\\.")[1].trim());
            } else {
                Toast.makeText(getApplicationContext(), "Gagal", Toast.LENGTH_SHORT).show();
                llCallDetailKategoriDetailfvbi.setVisibility(View.GONE);
            }
        }
    }

    private void updateUi() {

        llCallDetailKategoriDetailfvbi.setVisibility(View.GONE);
        llCallDetailTimefvbi.setVisibility(View.GONE);

        if (isIntro) {
            cvCallDetailIntrofvbi.setVisibility(View.GONE);
            cvCallDetailCallfvbi.setVisibility(View.VISIBLE);
        } else {
            cvCallDetailIntrofvbi.setVisibility(View.VISIBLE);
            cvCallDetailCallfvbi.setVisibility(View.GONE);
        }

        if (isExpired) {
            cvCallDetailCallExecutefvbi.setVisibility(View.GONE);
        } else {
            cvCallDetailCallExecutefvbi.setVisibility(View.VISIBLE);
        }

        if (isCalling) {
            tvCallDetailExecutefvbi.setText("End");
        } else {
            tvCallDetailExecutefvbi.setText("Call");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
