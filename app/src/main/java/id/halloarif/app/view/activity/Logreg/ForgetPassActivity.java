package id.halloarif.app.view.activity.Logreg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import id.halloarif.app.R;
import id.halloarif.app.util.InputValidUtil;

public class ForgetPassActivity extends AppCompatActivity {

    private EditText etForgetPassEmailPhonenmbrfvbi, etForgetPassCodefvbi;
    private CardView bForgetPassExecutefvbi;
    private boolean doubleBackToExitPressedOnce;
    private boolean isEmPhoneDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        etForgetPassEmailPhonenmbrfvbi = (EditText) findViewById(R.id.etForgetPassEmailPhonenmbr);
        etForgetPassCodefvbi = (EditText) findViewById(R.id.etForgetPassCode);
        bForgetPassExecutefvbi = (CardView) findViewById(R.id.bForgetPassExecute);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        updateUI(false);
    }

    private void initListener() {
        etForgetPassEmailPhonenmbrfvbi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_GO) {
                    forgetCodeMainValid();
                }
                return handled;
            }
        });
        etForgetPassCodefvbi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_GO) {
                    forgetCodeMainValid();
                }
                return handled;
            }
        });
        bForgetPassExecutefvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgetCodeMainValid();
            }
        });
    }

    private void updateUI(boolean isEmailDone) {
        isEmPhoneDone = isEmailDone;

        etForgetPassEmailPhonenmbrfvbi.setText("");
        etForgetPassCodefvbi.setText("");

        if (!isEmailDone) {
            etForgetPassEmailPhonenmbrfvbi.setVisibility(View.VISIBLE);
            etForgetPassCodefvbi.setVisibility(View.GONE);
        } else {
            etForgetPassEmailPhonenmbrfvbi.setVisibility(View.GONE);
            etForgetPassCodefvbi.setVisibility(View.VISIBLE);

            etForgetPassCodefvbi.requestFocus();
        }
    }

    /**/
    private void forgetCodeMainValid() {
        if (!isEmPhoneDone) {
            forgetPassValid();
        } else {
            codeValid();
        }
    }

    private void forgetPassValid() {
        String sEmailPhone = etForgetPassEmailPhonenmbrfvbi.getText().toString().trim();

        if (!InputValidUtil.isEmptyField(sEmailPhone)) {
            if (InputValidUtil.isValidateEmail(sEmailPhone)) {
                Toast.makeText(getApplicationContext(), sEmailPhone + " = ini email", Toast.LENGTH_SHORT).show();
                updateUI(true);
            } else if (InputValidUtil.isValidatePhoneNumber(sEmailPhone)) {
                Toast.makeText(getApplicationContext(), sEmailPhone + " = ini no hp", Toast.LENGTH_SHORT).show();
                updateUI(true);
            } else {
                InputValidUtil.errorET(etForgetPassEmailPhonenmbrfvbi, sEmailPhone + " = format Email/No Phone tidak terdeteksi");
            }
        }
    }

    private void codeValid() {
        String sCode = etForgetPassCodefvbi.getText().toString().trim();

        if (!InputValidUtil.isEmptyField(sCode)) {
            Toast.makeText(getApplicationContext(), "Code Ok", Toast.LENGTH_SHORT).show();
            moveToChangePass();
        } else {
            InputValidUtil.errorET(etForgetPassCodefvbi, "Field masih ada yang kosong");
        }
    }
    /**/

    private void moveToChangePass() {
        Intent intent = new Intent(ForgetPassActivity.this, ChangePassActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (isEmPhoneDone) {
            updateUI(false);
        } else {
            finish();
        }
    }
}
