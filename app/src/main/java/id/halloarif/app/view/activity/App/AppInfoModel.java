package id.halloarif.app.view.activity.App;

import android.os.Parcel;
import android.os.Parcelable;

public class AppInfoModel implements Parcelable {

    private int id;
    private String tipe_paket;
    private String nama_paket;
    private String harga_paket;
    private boolean unlimited;
    private boolean enable;
    private boolean expired;
    private int app_tersisa;
    private int date_expired;
    private String date_time_detail_expired;

    private String username;
    private String kategori;
    private String perasaan;
    private String permasalahan;
    private String isi;

    public AppInfoModel(int id,String tipe_paket, String nama_paket, boolean enable, String date_time_detail_expired, String username, String kategori, String perasaan, String permasalahan, String isi) {
        this.id = id;
        this.tipe_paket = tipe_paket;
        this.nama_paket = nama_paket;
        this.enable = enable;
        this.date_time_detail_expired = date_time_detail_expired;
        this.username = username;
        this.kategori = kategori;
        this.perasaan = perasaan;
        this.permasalahan = permasalahan;
        this.isi = isi;
    }

    public AppInfoModel(String nama_paket, boolean unlimited, int app_tersisa, boolean expired, String date_time_detail_expired) {
        this.nama_paket = nama_paket;
        this.unlimited = unlimited;
        this.app_tersisa = app_tersisa;
        this.expired = expired;
        this.date_time_detail_expired = date_time_detail_expired;
    }

    public AppInfoModel(String nama_paket, String harga_paket, boolean unlimited, int app_tersisa, int date_expired, boolean enable) {
        this.nama_paket = nama_paket;
        this.harga_paket = harga_paket;
        this.unlimited = unlimited;
        this.app_tersisa = app_tersisa;
        this.date_expired = date_expired;
        this.enable = enable;
    }

    protected AppInfoModel(Parcel in) {
        id = in.readInt();
        tipe_paket = in.readString();
        nama_paket = in.readString();
        harga_paket = in.readString();
        unlimited = in.readByte() != 0;
        enable = in.readByte() != 0;
        expired = in.readByte() != 0;
        app_tersisa = in.readInt();
        date_expired = in.readInt();
        date_time_detail_expired = in.readString();
        username = in.readString();
        kategori = in.readString();
        perasaan = in.readString();
        permasalahan = in.readString();
        isi = in.readString();
    }

    public static final Creator<AppInfoModel> CREATOR = new Creator<AppInfoModel>() {
        @Override
        public AppInfoModel createFromParcel(Parcel in) {
            return new AppInfoModel(in);
        }

        @Override
        public AppInfoModel[] newArray(int size) {
            return new AppInfoModel[size];
        }
    };

    public String getNama_paket() {
        return nama_paket;
    }

    public void setNama_paket(String nama_paket) {
        this.nama_paket = nama_paket;
    }

    public String getHarga_paket() {
        return harga_paket;
    }

    public void setHarga_paket(String harga_paket) {
        this.harga_paket = harga_paket;
    }

    public boolean isUnlimited() {
        return unlimited;
    }

    public void setUnlimited(boolean unlimited) {
        this.unlimited = unlimited;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getApp_tersisa() {
        return app_tersisa;
    }

    public void setApp_tersisa(int app_tersisa) {
        this.app_tersisa = app_tersisa;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public String getDate_time_detail_expired() {
        return date_time_detail_expired;
    }

    public void setDate_time_detail_expired(String date_time_detail_expired) {
        this.date_time_detail_expired = date_time_detail_expired;
    }

    public int getDate_expired() {
        return date_expired;
    }

    public void setDate_expired(int date_expired) {
        this.date_expired = date_expired;
    }

    public String getTipe_paket() {
        return tipe_paket;
    }

    public void setTipe_paket(String tipe_paket) {
        this.tipe_paket = tipe_paket;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getPerasaan() {
        return perasaan;
    }

    public void setPerasaan(String perasaan) {
        this.perasaan = perasaan;
    }

    public String getPermasalahan() {
        return permasalahan;
    }

    public void setPermasalahan(String permasalahan) {
        this.permasalahan = permasalahan;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AppInfoModel{" +
                "tipe_paket='" + tipe_paket + '\'' +
                ", nama_paket='" + nama_paket + '\'' +
                ", harga_paket='" + harga_paket + '\'' +
                ", unlimited=" + unlimited +
                ", enable=" + enable +
                ", expired=" + expired +
                ", app_tersisa=" + app_tersisa +
                ", date_expired=" + date_expired +
                ", date_time_detail_expired='" + date_time_detail_expired + '\'' +
                ", username='" + username + '\'' +
                ", kategori='" + kategori + '\'' +
                ", perasaan='" + perasaan + '\'' +
                ", permasalahan='" + permasalahan + '\'' +
                ", isi='" + isi + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(tipe_paket);
        parcel.writeString(nama_paket);
        parcel.writeString(harga_paket);
        parcel.writeByte((byte) (unlimited ? 1 : 0));
        parcel.writeByte((byte) (enable ? 1 : 0));
        parcel.writeByte((byte) (expired ? 1 : 0));
        parcel.writeInt(app_tersisa);
        parcel.writeInt(date_expired);
        parcel.writeString(date_time_detail_expired);
        parcel.writeString(username);
        parcel.writeString(kategori);
        parcel.writeString(perasaan);
        parcel.writeString(permasalahan);
        parcel.writeString(isi);
    }
}
