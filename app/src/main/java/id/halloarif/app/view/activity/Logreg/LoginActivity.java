package id.halloarif.app.view.activity.Logreg;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import id.halloarif.app.ISeasonConfig;
import id.halloarif.app.R;
import id.halloarif.app.util.InputValidUtil;
import id.halloarif.app.util.SessionUtil;
import id.halloarif.app.view.activity.Role.Client.DashboardClientActivity;
import id.halloarif.app.view.activity.Role.Finance.DashboardFinanceActivity;
import id.halloarif.app.view.activity.Role.Humancapital.DashboardHCActivity;
import id.halloarif.app.view.activity.Role.Motivator.DashboardMotivatorActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etLogNamefvbi, etLogPassword2fvbi;
    private ImageView ibLogPassword2fvbi;
    private TextView tvLogNowForgotPassfvbi, tvLogNowRegisterfvbi, tvLogNowTermfvbi, tvLogNowAggrementfvbi, tvLogNowActivationfvbi;
    private CardView bLogExecutefvbi;

    private boolean loggedin;
    private int roleid;
    private boolean passwordShown;
    private String sTokenBug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        etLogNamefvbi = (EditText) findViewById(R.id.etLogName);
        etLogPassword2fvbi = (EditText) findViewById(R.id.etLogPassword2);
        ibLogPassword2fvbi = (ImageView) findViewById(R.id.ibLogPassword2);
        bLogExecutefvbi = (CardView) findViewById(R.id.bLogExecute);
        tvLogNowForgotPassfvbi = (TextView) findViewById(R.id.tvLogNowForgotPass);
        tvLogNowRegisterfvbi = (TextView) findViewById(R.id.tvLogNowRegister);
        tvLogNowActivationfvbi = (TextView) findViewById(R.id.tvLogNowActivation);
        tvLogNowTermfvbi = (TextView) findViewById(R.id.tvLogNowTerm);
        tvLogNowAggrementfvbi = (TextView) findViewById(R.id.tvLogNowAggrement);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.HAS_LOGIN)) {
            loggedin = SessionUtil.getBoolPreferences(ISeasonConfig.HAS_LOGIN, false);
            if (loggedin) {
                if (SessionUtil.checkIfExist(ISeasonConfig.KEY_ROLE_ID)) {
                    roleid = SessionUtil.getIntPreferences(ISeasonConfig.KEY_ROLE_ID, 0);
                    Log.d("Lihat", "initSession LoginActivity : " + roleid);
                    moveToDashboard(roleid);
                }
            }
        }
    }

    private void initContent() {
        sTokenBug = FirebaseInstanceId.getInstance().getToken();
    }

    private void initListener() {
        etLogPassword2fvbi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_GO) {
                    loginValid();
                }
                return handled;
            }
        });

        ibLogPassword2fvbi.setOnClickListener(this);
        bLogExecutefvbi.setOnClickListener(this);
        tvLogNowForgotPassfvbi.setOnClickListener(this);
        tvLogNowRegisterfvbi.setOnClickListener(this);
        tvLogNowActivationfvbi.setOnClickListener(this);
        tvLogNowTermfvbi.setOnClickListener(this);
        tvLogNowAggrementfvbi.setOnClickListener(this);
    }

    private void moveToDashboard(int roleid) {
        Intent intent = null;

        SessionUtil.setIntPreferences(ISeasonConfig.KEY_ROLE_ID, roleid);
        SessionUtil.setBoolPreferences(ISeasonConfig.HAS_LOGIN, true);

        switch (roleid) {
            case ISeasonConfig.ROLE_MOTIVATOR:
                intent = new Intent(LoginActivity.this, DashboardMotivatorActivity.class);
                break;
            case ISeasonConfig.ROLE_CLIENT:
                intent = new Intent(LoginActivity.this, DashboardClientActivity.class);
                break;
            case ISeasonConfig.ROLE_FINANCE:
                intent = new Intent(LoginActivity.this, DashboardFinanceActivity.class);
                break;
            case ISeasonConfig.ROLE_HUMANCAPITAL:
                intent = new Intent(LoginActivity.this, DashboardHCActivity.class);
                break;
        }

        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view == ibLogPassword2fvbi) {
            if (passwordShown) {//tidak terlihat
                passwordShown = false;
                etLogPassword2fvbi.setInputType(129);//edittest
                etLogPassword2fvbi.setTypeface(Typeface.SANS_SERIF);
                ibLogPassword2fvbi.setImageResource(R.drawable.ic_visibility_off_black_24dp);
            } else {//terlihat
                passwordShown = true;
                etLogPassword2fvbi.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                ibLogPassword2fvbi.setImageResource(R.drawable.ic_visibility_black_24dp);
            }
        } else if (view == bLogExecutefvbi) {
            loginValid();
        } else if (view == tvLogNowForgotPassfvbi) {
            moveTo(ForgetPassActivity.class);
        } else if (view == tvLogNowRegisterfvbi) {
            moveTo(RegisActivity.class);
        } else if (view == tvLogNowActivationfvbi) {
            moveTo(RegisActivationActivity.class);
        } else if (view == tvLogNowTermfvbi) {
            dialogTermUseAggrement(LoginActivity.this, getString(R.string.term_of_use));
        } else if (view == tvLogNowAggrementfvbi) {
            //dialogTermUseAggrement(LoginActivity.this,getString(R.string.privacy_policy));
        }
    }

    private void loginValid() {
        String sName = etLogNamefvbi.getText().toString().trim();
        String sPass = etLogPassword2fvbi.getText().toString().trim();

        if (!InputValidUtil.isEmptyField("Kolom ini masih kosong", etLogNamefvbi, etLogPassword2fvbi)) {
            if (InputValidUtil.isValidateEmail(sName)) {
                Toast.makeText(getApplicationContext(), sName + " = ini email", Toast.LENGTH_SHORT).show();
                if (sName.equalsIgnoreCase("user@mail.com") && sPass.equalsIgnoreCase("admin")) {
                    moveToDashboard(ISeasonConfig.ROLE_CLIENT);
                } else if (sName.equalsIgnoreCase("mot@mail.com") && sPass.equalsIgnoreCase("admin")) {
                    moveToDashboard(ISeasonConfig.ROLE_MOTIVATOR);
                } else if (sName.equalsIgnoreCase("hc@mail.com") && sPass.equalsIgnoreCase("admin")) {
                    moveToDashboard(ISeasonConfig.ROLE_HUMANCAPITAL);
                } else if (sName.equalsIgnoreCase("fin@mail.com") && sPass.equalsIgnoreCase("admin")) {
                    moveToDashboard(ISeasonConfig.ROLE_FINANCE);
                } else if (sName.equalsIgnoreCase("admin@mail.com") && sPass.equalsIgnoreCase("admin")) {
                    Snackbar.make(findViewById(android.R.id.content), "Under Construction", Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Error email & password", Snackbar.LENGTH_SHORT).show();
                }
            } else if (InputValidUtil.isValidatePhoneNumber(sName)) {
                Toast.makeText(getApplicationContext(), sName + " = ini no hp", Toast.LENGTH_SHORT).show();
                Snackbar.make(findViewById(android.R.id.content), "Under Construction", Snackbar.LENGTH_SHORT).show();
            } else {
                InputValidUtil.errorET(etLogNamefvbi, sName + " = format Email/No Phone tidak terdeteksi");
            }
        }
    }

    private void moveTo(Class<?> aClass) {
        Intent intent = new Intent(LoginActivity.this, aClass);
        startActivity(intent);
        //finish();
    }

    public static void dialogTermUseAggrement(Activity activity, String termUse) {
        Dialog dialogTermUse;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogTermUse = new Dialog(activity, android.R.style.Theme_DeviceDefault_Light_Dialog);
        } else {
            dialogTermUse = new Dialog(activity);
        }
        dialogTermUse.requestWindowFeature(Window.FEATURE_NO_TITLE);//untuk tidak ada title
        dialogTermUse.setContentView(R.layout.poptermuse);
        dialogTermUse.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));//untuk menghilangkan background
        dialogTermUse.setTitle("");
        dialogTermUse.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams layoutparams = new WindowManager.LayoutParams();
        layoutparams.copyFrom(dialogTermUse.getWindow().getAttributes());
        layoutparams.width = WindowManager.LayoutParams.MATCH_PARENT;//ukuran lebar layout
        layoutparams.height = WindowManager.LayoutParams.WRAP_CONTENT;//ukuran tinggi layout

        // set the custom dialogTermUseAggrement components - text, image and button
        HtmlTextView tvPopTermUseTermUse = (HtmlTextView) dialogTermUse.findViewById(R.id.tvPopTermUse);
        Button bPopTermUseCloseTermUse = (Button) dialogTermUse.findViewById(R.id.bPopTermUseClose);

        tvPopTermUseTermUse.setHtml(termUse);
        tvPopTermUseTermUse.setMovementMethod(LinkMovementMethod.getInstance());

        bPopTermUseCloseTermUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTermUse.dismiss();
            }
        });

        dialogTermUse.show();
        dialogTermUse.getWindow().setAttributes(layoutparams);
    }

    /*private void customText(TextView textView) {
        SpannableStringBuilder spanText = new SpannableStringBuilder();

        spanText.append(userName);
        spanText.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                // On Click Action

            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                textPaint.setColor(textPaint.linkColor);    // you can use custom color
                textPaint.setUnderlineText(false);    // this remove the underline
            }
        }, spanText.length() - userName.length(), spanText.length(), 0);

        spanText.append(status);
        spanText.append(songName);
        spanText.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                // On Click Action

            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                textPaint.setColor(textPaint.linkColor);    // you can use custom color
                textPaint.setUnderlineText(false);    // this remove the underline
            }
        }, spanText.length() - songName.length(), spanText.length(), 0);

        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spanText, TextView.BufferType.SPANNABLE);
    }*/
}
