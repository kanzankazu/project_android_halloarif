package id.halloarif.app;

public interface IConfig {

    String API_BASE_URL = "http://halloarif.id/";
    String API_MAIN_URL = API_BASE_URL + "webapi/app/";

    String PACKAGE_NAME = App.getContext().getPackageName();
    String VERSION_NAME = BuildConfig.VERSION_NAME;
    int VERSION_CODE = BuildConfig.VERSION_CODE;

}
